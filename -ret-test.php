<?php


include './inc/config.php';
include './inc/functions.php';


$act = $_REQUEST['act'];
$ps = 'P';
if($act == "cancel") {
  $ps = 'F';
} else if($act=="success") {
  $ps = 'S';
}
$post_values = array(
  "USER"=>PAYPAL_USER,
  "PWD"=>PAYPAL_PWD,
  "SIGNATURE"=>PAYPAL_SIGNATURE,
  "METHOD"=>"GetExpressCheckoutDetails",
  "VERSION"=>"93",
  "TOKEN"=>$_REQUEST['token']);
$post_string = "";
foreach( $post_values as $key => $value) {
  $post_string .= "$key=" . urlencode( $value ) . "&";
}
$post_string = rtrim( $post_string, "& " );
$request = curl_init(PAYPAL_API);
curl_setopt($request, CURLOPT_HEADER, 0);
curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
$post_response = curl_exec($request);
curl_close ($request);

$express_chkout_res = getResToArr($post_response);

if(isset($express_chkout_res['PAYERID'])) {

  $post_values2 = array(
    "USER"=>PAYPAL_USER,
    "PWD"=>PAYPAL_PWD,
    "SIGNATURE"=>PAYPAL_SIGNATURE,
    "METHOD"=>"DoExpressCheckoutPayment",
    "VERSION"=>"93",
    "TOKEN"=>$_REQUEST['token'],
    "PAYERID"=>$express_chkout_res['PAYERID'],
    "PAYMENTREQUEST_0_PAYMENTACTION"=>"SALE",
    "PAYMENTREQUEST_0_AMT"=>$_SESSION['amount'],
    "PAYMENTREQUEST_0_CURRENCYCODE"=>"USD");
$post_string2 = "";
foreach( $post_values2 as $key => $value) {
  $post_string2 .= "$key=" . urlencode( $value ) . "&";
}
$post_string2 = rtrim( $post_string2, "& " );
  $request2 = curl_init(PAYPAL_API);
  curl_setopt($request2, CURLOPT_HEADER, 0);
  curl_setopt($request2, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($request2, CURLOPT_POSTFIELDS, $post_string2);
  curl_setopt($request2, CURLOPT_SSL_VERIFYPEER, FALSE);
  $post_response2 = curl_exec($request2);
  curl_close ($request2);
}

$json = array('GetExpressCheckoutDetails'=> $express_chkout_res);
if(isset($express_chkout_res['PAYERID'])) {
  $json['DoExpressCheckoutPayment'] = getResToArr($post_response2);
}

$sql_qry = "UPDATE `payment` SET `Payment_Status`='".$ps."', Json='".escape_string(json_encode($json), '', 'db')."'  WHERE `Token`='".$_REQUEST['token']."'";
mysql_query($sql_qry);
$pymnt = get_record('payment', 'Type', "`Token`='".$_REQUEST['token']."'");
if($pymnt['Type'] == 'B') {
  header("location: booking-thanks.php?act=$ps");
} else {
  header("location: reg-thanks.php?act=$ps");
}

?>