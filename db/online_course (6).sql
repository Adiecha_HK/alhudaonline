-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 13, 2015 at 06:45 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `online_course`
--

-- --------------------------------------------------------

--
-- Table structure for table `adm_main`
--

CREATE TABLE IF NOT EXISTS `adm_main` (
  `Id` int(2) NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) DEFAULT NULL,
  `Password` varchar(30) DEFAULT NULL,
  `Full_Name` varchar(100) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Last_Loggedin` datetime DEFAULT NULL,
  `Loggedin_IP` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`),
  KEY `Username` (`Username`),
  KEY `Password` (`Password`),
  KEY `Username_2` (`Username`,`Password`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `adm_main`
--

INSERT INTO `adm_main` (`Id`, `Username`, `Password`, `Full_Name`, `Email`, `Last_Loggedin`, `Loggedin_IP`) VALUES
(2, 'test', 'test', 'Admin - Online Course', 'info@codegeeks.in', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Course` varchar(255) DEFAULT NULL,
  `Code` varchar(16) NOT NULL,
  `Brief` varchar(255) DEFAULT NULL,
  `Description` text,
  `Attachment` varchar(255) DEFAULT NULL,
  `Duration` varchar(4) DEFAULT NULL,
  `Duration_Type` enum('H','D','W','M','Y','N') DEFAULT NULL COMMENT 'H:Hours; D:Days; W:Weeks; M:Months; Y:Years; N:Not Defined / Never Ending',
  `Entry_Date` datetime DEFAULT NULL,
  `Last_Modified` datetime DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `mode` enum('A','T') NOT NULL DEFAULT 'A' COMMENT 'A=Audio, T=Tutor & audio',
  `Course_Type` enum('H','TQ','TH') NOT NULL,
  `order_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`Id`, `Course`, `Code`, `Brief`, `Description`, `Attachment`, `Duration`, `Duration_Type`, `Entry_Date`, `Last_Modified`, `amount`, `mode`, `Course_Type`, `order_no`) VALUES
(18, 'Speak Arabic', '', 'A program designed to help you speak Arabic confidently in a short period of time', '', NULL, '12', 'D', '2014-09-26 18:38:08', '2015-01-08 18:53:13', 0, 'A', 'TQ', 2),
(19, 'Modern Standard Arabic - Complete', 'sdf', 'For those seeking a high academic and professional standard of Arabic including literature and media', '', NULL, '12', 'D', '2014-09-26 18:38:42', '2015-01-21 19:18:08', 0, 'A', 'TQ', 0),
(20, 'Qu''ranic Arabic Complete', '', 'For those seeking an excellent academic standard of Qurâ€™anic Arabic and Understanding Islam. Includes Modern Spoken and literary Arabic', '', NULL, '12', 'W', '2014-09-26 18:39:16', NULL, 0, 'A', 'H', NULL),
(21, 'Business Arabic', '', 'A program designed for professionals working in the Middle East or with Arab clients', '', NULL, '12', 'D', '2014-09-26 18:39:46', '2015-01-08 18:52:47', 0, 'T', 'TQ', 1),
(22, 'Tajweed', '', 'For those seeking to perfect the recitation of the Qurâ€™an under a Professional Qurâ€™an instructor', '', NULL, '12', 'D', '2014-09-26 18:40:13', '2015-01-08 18:53:27', 0, 'A', 'TH', 4),
(24, 'Test course 1', '', 'Kashyap''s course for carate', '', NULL, '2', 'M', '2014-11-27 13:38:13', '2014-11-27 15:24:50', 4, 'A', 'TH', NULL),
(25, 'Test course 2', '', 'Karate kashyap', '', NULL, '2', 'W', '2014-11-27 13:44:03', '2015-01-08 18:53:39', 1, 'T', 'TH', 3);

-- --------------------------------------------------------

--
-- Table structure for table `courses_assignments`
--

CREATE TABLE IF NOT EXISTS `courses_assignments` (
  `Id` double NOT NULL AUTO_INCREMENT,
  `Course_Id` int(11) DEFAULT NULL,
  `Level_Id` int(11) DEFAULT NULL,
  `Teacher_Id` int(11) DEFAULT NULL,
  `Entry_Date` datetime DEFAULT NULL,
  `Last_Modified` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`,`Course_Id`,`Level_Id`,`Teacher_Id`),
  KEY `Course_Id` (`Course_Id`,`Level_Id`),
  KEY `Course_Id_2` (`Course_Id`,`Teacher_Id`),
  KEY `Course_Id_3` (`Course_Id`,`Level_Id`,`Teacher_Id`),
  KEY `Level_Id` (`Level_Id`,`Teacher_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `courses_assignments`
--

INSERT INTO `courses_assignments` (`Id`, `Course_Id`, `Level_Id`, `Teacher_Id`, `Entry_Date`, `Last_Modified`) VALUES
(19, 21, NULL, 17, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_validation`
--

CREATE TABLE IF NOT EXISTS `email_validation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Sid` int(11) NOT NULL,
  `Hash` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `email_validation`
--

INSERT INTO `email_validation` (`Id`, `Sid`, `Hash`) VALUES
(1, 21, '06a3b95eb420ff84'),
(2, 22, '07429af8b915883f'),
(3, 23, '48b98febe4376fe4'),
(4, 24, '11ef6f2ba09757b8'),
(5, 25, '12f97b14632ad58f'),
(6, 26, 'a4c0b605ab7884a7'),
(7, 27, 'a92cbda438b518cb'),
(8, 28, '5939706e862b2a6c'),
(9, 29, '148cda8b03a0f527'),
(10, 30, '0afec866c85f87ff'),
(11, 31, 'd998ffbf080bc14a'),
(12, 32, '1a17ceaa62e7c0ee'),
(13, 33, 'efcf81e0a337db08');

-- --------------------------------------------------------

--
-- Table structure for table `error_mysql`
--

CREATE TABLE IF NOT EXISTS `error_mysql` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Error_Value` varchar(255) DEFAULT NULL,
  `Error_Message` text,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`,`Error_Value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jqcalendar`
--

CREATE TABLE IF NOT EXISTS `jqcalendar` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Subject` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `Location` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `Description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `StartTime` datetime DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `IsAllDayEvent` smallint(6) NOT NULL,
  `Color` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `RecurringRule` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `Teacher_Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `jqcalendar`
--

INSERT INTO `jqcalendar` (`Id`, `Subject`, `Location`, `Description`, `StartTime`, `EndTime`, `IsAllDayEvent`, `Color`, `RecurringRule`, `Teacher_Id`) VALUES
(54, 'Availability', NULL, NULL, '2015-02-06 07:30:00', '2015-02-06 12:00:00', 0, NULL, NULL, 17),
(55, 'Availability', NULL, NULL, '2015-02-24 01:00:00', '2015-02-24 08:30:00', 0, NULL, NULL, 17),
(56, 'Availability', NULL, NULL, '2015-02-25 01:00:00', '2015-02-25 08:00:00', 0, NULL, NULL, 17),
(57, 'Availability', NULL, NULL, '2015-02-26 01:00:00', '2015-02-26 08:00:00', 0, NULL, NULL, 17),
(58, 'Availability', NULL, NULL, '2015-02-27 01:00:00', '2015-02-27 08:30:00', 0, NULL, NULL, 17),
(59, 'Availability', NULL, NULL, '2015-02-28 01:00:00', '2015-02-28 08:30:00', 0, NULL, NULL, 17),
(60, 'Availability', NULL, NULL, '2015-03-01 01:00:00', '2015-03-01 08:00:00', 0, NULL, NULL, 17);

-- --------------------------------------------------------

--
-- Table structure for table `lessons`
--

CREATE TABLE IF NOT EXISTS `lessons` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Course_Id` int(11) DEFAULT NULL,
  `Lesson` varchar(255) DEFAULT NULL,
  `Brief` varchar(255) DEFAULT NULL,
  `Description` text,
  `Attachment` varchar(255) DEFAULT NULL,
  `Duration` varchar(4) DEFAULT NULL,
  `Entry_Date` datetime DEFAULT NULL,
  `Last_Modified` datetime DEFAULT NULL,
  `amount` double NOT NULL,
  `audio` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `order_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`,`Course_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `lessons`
--

INSERT INTO `lessons` (`Id`, `Course_Id`, `Lesson`, `Brief`, `Description`, `Attachment`, `Duration`, `Entry_Date`, `Last_Modified`, `amount`, `audio`, `link`, `order_no`) VALUES
(1, 23, 'lskdjf', 'skdjfls', '', NULL, '34', '2014-11-22 09:13:30', NULL, 0, '', NULL, NULL),
(2, 23, 'anther', 'skldflsk', '', NULL, '34', '2014-11-22 09:14:06', NULL, 0, '', NULL, NULL),
(3, 2, 'SLDJF', 'SKLFJDL', '', NULL, '', '2014-11-26 14:54:48', NULL, 0, '', NULL, NULL),
(4, 1, 'Testing', 'skdfjlskdjlfk', '', NULL, '', '2014-11-26 14:58:06', '2014-11-26 15:31:23', 0, '', NULL, NULL),
(6, 1, 'sample1', 'slkdkl', '', NULL, '', '2014-11-26 15:16:07', '2014-11-26 15:41:04', 0, '', NULL, NULL),
(7, 1, 'jdflj', 'dlkjfgl', '', NULL, NULL, '2014-11-26 15:37:41', '2014-11-26 15:39:17', 43.3, '', NULL, NULL),
(8, 7, 'last lesson', 's,djnf', '', NULL, NULL, '2014-11-26 18:58:10', NULL, 98, '', NULL, NULL),
(9, 7, 'lesson x', 'xljdjlf', '', NULL, NULL, '2014-11-26 18:58:30', NULL, 34, '', NULL, NULL),
(10, 6, 'lesson 1', 'sdkjfhs', '', NULL, NULL, '2014-11-26 18:59:16', NULL, 2, '', NULL, NULL),
(11, 6, 'lesson 2', 'sdfkjsj', '', NULL, NULL, '2014-11-26 18:59:32', NULL, 8, '', NULL, NULL),
(12, 5, 'L1', 'skjdfhk', '', NULL, NULL, '2014-11-26 19:00:23', NULL, 6, '', NULL, NULL),
(13, 5, 'L2', 'sdjkfls', '', NULL, NULL, '2014-11-26 19:00:32', NULL, 4, '', NULL, NULL),
(14, 5, 'L3', 'sdhfsk', '', NULL, NULL, '2014-11-26 19:00:43', NULL, 2, '', NULL, NULL),
(15, 12, 'sdfhk', 'KJSDHFK', '', NULL, NULL, '2014-11-27 16:03:21', NULL, 0, '', NULL, NULL),
(16, 12, 'sjdfks', 'skdjfk', '', NULL, NULL, '2014-11-27 16:19:47', NULL, 0, '', NULL, NULL),
(17, 12, 'sjdfl', 'xdjlksj', '', NULL, NULL, '2014-11-27 16:27:58', NULL, 2, '', NULL, NULL),
(18, 5, 'audio', 'ksjdfljs', '', NULL, NULL, '2014-11-28 10:10:25', NULL, 3, '', NULL, NULL),
(19, 21, 'klsdjflsj', 'sdkfjlsdj', NULL, NULL, '9', '2014-12-02 08:12:27', '2015-01-07 00:09:35', 9, '', '', 2),
(20, 21, 'demo lession', 'testing lession description', NULL, NULL, '49', '2014-12-05 09:43:48', '2015-01-21 19:09:45', 10, '', 'sdfsdf', 3),
(21, 22, 'sample lesson1', 'ajfljsdlj', NULL, NULL, '30', '2014-12-05 19:46:30', NULL, 6, '', '', NULL),
(22, 22, 'l2', 'slkdfjl', NULL, NULL, '30', '2014-12-05 19:46:43', NULL, 8, '', '', NULL),
(23, 21, 'Sample', 'skdjldfj', NULL, NULL, '30', '2014-12-07 03:44:57', '2015-01-21 19:08:29', 12, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Sender_Type` enum('T','S','A') NOT NULL COMMENT 'T=Teacher, S=Student, A=Admin',
  `Sender_Id` int(11) NOT NULL,
  `Reciever_Type` enum('T','S','A') NOT NULL COMMENT 'T=Teacher, S=Student, A=Admin',
  `Reciever_Id` int(11) NOT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  `Message` text,
  `Status` enum('U','R') NOT NULL DEFAULT 'U' COMMENT 'U=Unread, R=Read',
  `Entry_Date` datetime NOT NULL,
  `Last_Modified` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`Id`, `Sender_Type`, `Sender_Id`, `Reciever_Type`, `Reciever_Id`, `Title`, `Type`, `Message`, `Status`, `Entry_Date`, `Last_Modified`) VALUES
(1, 'A', 2, 'S', 18, 'S;DK', 0, 'SLKFJLSJ', 'R', '2014-12-03 05:22:04', '2014-12-03 05:22:04'),
(2, 'S', 18, 'A', 2, 'How to do payment', 2, 'Hello, please help me out to do payment for booking a lesson.', 'R', '2014-12-04 01:11:15', '2014-12-04 01:11:15'),
(3, 'A', 2, 'S', 18, 'Re: How to do payment', 2, 'Ok, lets schedule a session to make you learn this thing', 'R', '2014-12-04 01:12:39', '2014-12-04 01:12:39'),
(4, 'S', 37, 'A', 2, 'Enroll for a course', 0, 'Hello sir, please enroll me in another course. thanks', 'R', '2014-12-07 01:59:40', '2014-12-07 01:59:40'),
(5, 'T', 17, 'T', 18, 'sampel', 0, 'dlkfgl', 'U', '2014-12-12 11:27:09', '2014-12-12 11:27:09'),
(6, 'A', 2, 'S', 18, 'Testing', 0, 'Testing', 'R', '2014-12-18 17:16:30', '2014-12-18 17:16:30'),
(7, 'S', 18, 'T', 17, 'Test msg', 0, 'Hello ', 'R', '2014-12-24 15:19:10', '2014-12-24 15:19:10');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Sid` int(11) NOT NULL,
  `TransectionId` varchar(255) NOT NULL,
  `Type` enum('R','B','S') NOT NULL DEFAULT 'B' COMMENT 'R=Registration, B=Booking, S=SubscribeCourse',
  `Discription` varchar(255) NOT NULL,
  `Amount` double NOT NULL,
  `Discount` double NOT NULL,
  `Json` text NOT NULL,
  `Entry_Date` datetime NOT NULL,
  `Schedule_Id` int(11) DEFAULT NULL,
  `Payment_Status` enum('P','S','F') NOT NULL COMMENT 'P=Pending, S=Successfull, F=Fail',
  `Payment_Through` enum('P','A','S') NOT NULL COMMENT 'P=Paypal, A=Authorize, S=Satlement',
  `Token` varchar(255) NOT NULL,
  `Remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`Id`, `Sid`, `TransectionId`, `Type`, `Discription`, `Amount`, `Discount`, `Json`, `Entry_Date`, `Schedule_Id`, `Payment_Status`, `Payment_Through`, `Token`, `Remark`) VALUES
(19, 18, '', 'B', '', 9, 0, '{"response":["3","2","33","Credit card number is required.","","P","0","","Sample Transaction","9.00","CC","auth_capture","","","","","","","","","","","","","","","","","","","","","","","","","","50F4C24A00900A1B7471E0B73C45C8E0","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""]}', '2014-12-19 14:02:54', 45, 'F', 'A', '', 'Credit card number is required.'),
(20, 18, '', 'B', '', 10, 0, '', '2014-12-19 14:07:23', 46, 'P', 'A', '', NULL),
(21, 18, '', 'B', '', 10, 0, '{"response":["1","1","1","This transaction has been approved.","GHDV8W","Y","2225827280","","Sample Transaction","10.00","CC","auth_capture","","","","","","","","","","","","","","","","","","","","","","","","","","9782CA71C473648B72D159E2277D3752","","2","","","","","","","","","","","XXXX1111","Visa","","","","","","","","","","","","","","","",""]}', '2014-12-19 14:19:05', 47, 'S', 'A', '', NULL),
(22, 18, '', 'B', '', 10, 0, '{"response":["1","1","1","This transaction has been approved.","V28IXC","Y","2225828235","","Sample Transaction","10.00","CC","auth_capture","","","","","","","","","","","","","","","","","","","","","","","","","","1C2ECB054D5D9CEC3C696344B33BB81E","","2","","","","","","","","","","","XXXX1111","Visa","","","","","","","","","","","","","","","",""]}', '2014-12-19 14:23:11', 48, 'S', 'A', '', NULL),
(23, 18, '', 'B', '', 9, 0, '{"GetExpressCheckoutDetails":{"ACK":"Failure","L_ERRORCODE0":"81002","L_SHORTMESSAGE0":"Unspecified Method","L_LONGMESSAGE0":"Method Specified is not Supported","L_SEVERITYCODE0":"Error"}}', '2014-12-19 18:09:14', 49, 'S', 'P', 'EC-0TS156157P252741Y', NULL),
(24, 18, '', 'B', '', 12, 0, '{"GetExpressCheckoutDetails":{"ACK":"Failure","L_ERRORCODE0":"81002","L_SHORTMESSAGE0":"Unspecified Method","L_LONGMESSAGE0":"Method Specified is not Supported","L_SEVERITYCODE0":"Error"}}', '2014-12-24 15:16:47', 50, 'S', 'P', 'EC-1U189812NE196581E', NULL),
(25, 18, '', 'B', '', 10, 0, '{"GetExpressCheckoutDetails":{"ACK":"Failure","L_ERRORCODE0":"81002","L_SHORTMESSAGE0":"Unspecified Method","L_LONGMESSAGE0":"Method Specified is not Supported","L_SEVERITYCODE0":"Error"}}', '2014-12-25 11:42:22', 51, 'S', 'P', 'EC-2S089979GC727301W', NULL),
(26, 18, '', 'B', '', 9, 0, '{"GetExpressCheckoutDetails":{"ACK":"Failure","L_ERRORCODE0":"81002","L_SHORTMESSAGE0":"Unspecified Method","L_LONGMESSAGE0":"Method Specified is not Supported","L_SEVERITYCODE0":"Error"}}', '2014-12-25 18:00:18', 52, 'S', 'P', 'EC-5C568469NE635210D', NULL),
(27, 22, '', 'R', '', 60, 0, '', '2015-01-02 11:06:13', NULL, 'P', 'P', 'EC-2JW71812W1203444L', NULL),
(28, 18, '', 'B', '', 9, 0, '{"GetExpressCheckoutDetails":{"ACK":"Failure","L_ERRORCODE0":"81002","L_SHORTMESSAGE0":"Unspecified Method","L_LONGMESSAGE0":"Method Specified is not Supported","L_SEVERITYCODE0":"Error"}}', '2015-01-02 11:56:00', 0, 'F', 'P', 'EC-0YD381426K673904U', NULL),
(29, 23, '', 'R', '', 60, 0, '{"response":["3","2","33","Bill To First Name is required.","","P","0","","Sample Transaction","0.10","CC","auth_capture","","","","","","","","","","","","","","","","","","","","","","","","","","E967EFB28D09D6D773A167D3642D1F8C","","","","","","","","","","","","","XXXX1111","Visa","","","","","","","","","","","","","","","",""]}', '2015-01-02 12:52:15', NULL, 'F', 'A', '', 'Bill To First Name is required.'),
(30, 24, '', 'R', '', 40, 0, '{"response":["1","1","1","This transaction has been approved.","UYN595","Y","2226367350","","Sample Transaction","0.10","CC","auth_capture","","","","","","","","","","","","","","","","","","","","","","","","","","D1956E0EF358718CCBFE65A18C3565D2","","2","","","","","","","","","","","XXXX1111","Visa","","","","","","","","","","","","","","","",""]}', '2015-01-02 12:55:37', NULL, 'S', 'A', '', 'The credit card number is invalid.'),
(31, 25, '', 'R', '', 60, 0, '{"response":["3","1","11","A duplicate transaction has been submitted.","","P","0","","Sample Transaction","0.10","CC","auth_capture","","","","","","","","","","","","","","","","","","","","","","","","","","5C9B22E9C483E052320BF0A7FBBBF954","","","","","","","","","","","","","XXXX1111","Visa","","","","","","","","","","","","","","","",""]}', '2015-01-02 12:58:18', NULL, 'F', 'A', '', 'A duplicate transaction has been submitted.'),
(32, 18, '', 'B', '', 9, 0, '{"GetExpressCheckoutDetails":{"ACK":"Failure","L_ERRORCODE0":"81002","L_SHORTMESSAGE0":"Unspecified Method","L_LONGMESSAGE0":"Method Specified is not Supported","L_SEVERITYCODE0":"Error"}}', '2015-01-02 13:36:26', 0, 'F', 'P', 'EC-8RJ39790R09051323', NULL),
(33, 18, '', 'B', '', 9, 0, '{"GetExpressCheckoutDetails":null}', '2015-01-02 13:39:34', 0, 'S', 'P', 'EC-81J82410MP023961Y', NULL),
(34, 18, '', 'B', '', 9, 0, '{"GetExpressCheckoutDetails":{"TOKEN":"EC-53923871T7644784Y","PHONENUM":"+91 9662025100","BILLINGAGREEMENTACCEPTEDSTATUS":"0","CHECKOUTSTATUS":"PaymentActionNotInitiated","TIMESTAMP":"2015-01-02T08:35:37Z","CORRELATIONID":"44db54e95ed4c","ACK":"Success","VERSION":"93","BUILD":"14443165","EMAIL":"girish.kargathara@gmail.com","PAYERID":"29NFJYJ7L4N38","PAYERSTATUS":"verified","BUSINESS":"Kargathara G C","FIRSTNAME":"Girish","LASTNAME":"kargathara","COUNTRYCODE":"IN","SHIPTONAME":"Kargathara G C","SHIPTOSTREET":"Nagnatha Chowk","SHIPTOSTREET2":"Street No. 1","SHIPTOCITY":"Upleta","SHIPTOSTATE":"Gujarat","SHIPTOZIP":"360490","SHIPTOCOUNTRYCODE":"IN","SHIPTOCOUNTRYNAME":"India","ADDRESSSTATUS":"Unconfirmed","CURRENCYCODE":"USD","AMT":"0.10","ITEMAMT":"0.10","SHIPPINGAMT":"0.00","HANDLINGAMT":"0.00","TAXAMT":"0.00","INSURANCEAMT":"0.00","SHIPDISCAMT":"0.00","L_NAME0":"Alhuda Flexible","L_QTY0":"1","L_TAXAMT0":"0.00","L_AMT0":"0.10","L_ITEMWEIGHTVALUE0":"   0.00000","L_ITEMLENGTHVALUE0":"   0.00000","L_ITEMWIDTHVALUE0":"   0.00000","L_ITEMHEIGHTVALUE0":"   0.00000","PAYMENTREQUEST_0_CURRENCYCODE":"USD","PAYMENTREQUEST_0_AMT":"0.10","PAYMENTREQUEST_0_ITEMAMT":"0.10","PAYMENTREQUEST_0_SHIPPINGAMT":"0.00","PAYMENTREQUEST_0_HANDLINGAMT":"0.00","PAYMENTREQUEST_0_TAXAMT":"0.00","PAYMENTREQUEST_0_INSURANCEAMT":"0.00","PAYMENTREQUEST_0_SHIPDISCAMT":"0.00","PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED":"false","PAYMENTREQUEST_0_SHIPTONAME":"Kargathara G C","PAYMENTREQUEST_0_SHIPTOSTREET":"Nagnatha Chowk","PAYMENTREQUEST_0_SHIPTOSTREET2":"Street No. 1","PAYMENTREQUEST_0_SHIPTOCITY":"Upleta","PAYMENTREQUEST_0_SHIPTOSTATE":"Gujarat","PAYMENTREQUEST_0_SHIPTOZIP":"360490","PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE":"IN","PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME":"India","PAYMENTREQUEST_0_ADDRESSSTATUS":"Unconfirmed","L_PAYMENTREQUEST_0_NAME0":"Alhuda Flexible","L_PAYMENTREQUEST_0_QTY0":"1","L_PAYMENTREQUEST_0_TAXAMT0":"0.00","L_PAYMENTREQUEST_0_AMT0":"0.10","L_PAYMENTREQUEST_0_ITEMWEIGHTVALUE0":"   0.00000","L_PAYMENTREQUEST_0_ITEMLENGTHVALUE0":"   0.00000","L_PAYMENTREQUEST_0_ITEMWIDTHVALUE0":"   0.00000","L_PAYMENTREQUEST_0_ITEMHEIGHTVALUE0":"   0.00000","PAYMENTREQUESTINFO_0_ERRORCODE":"0"},"DoExpressCheckoutPayment":{"TOKEN":"EC-53923871T7644784Y","SUCCESSPAGEREDIRECTREQUESTED":"false","TIMESTAMP":"2015-01-02T08:35:42Z","CORRELATIONID":"f4b675d44f08c","ACK":"Success","VERSION":"93","BUILD":"14443165","INSURANCEOPTIONSELECTED":"false","SHIPPINGOPTIONISDEFAULT":"false","PAYMENTINFO_0_TRANSACTIONID":"2C409195FW0366003","PAYMENTINFO_0_TRANSACTIONTYPE":"expresscheckout","PAYMENTINFO_0_PAYMENTTYPE":"instant","PAYMENTINFO_0_ORDERTIME":"2015-01-02T08:35:40Z","PAYMENTINFO_0_AMT":"0.10","PAYMENTINFO_0_FEEAMT":"0.10","PAYMENTINFO_0_TAXAMT":"0.00","PAYMENTINFO_0_CURRENCYCODE":"USD","PAYMENTINFO_0_PAYMENTSTATUS":"Completed","PAYMENTINFO_0_PENDINGREASON":"None","PAYMENTINFO_0_REASONCODE":"None","PAYMENTINFO_0_PROTECTIONELIGIBILITY":"Eligible","PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE":"ItemNotReceivedEligible,UnauthorizedPaymentEligible","PAYMENTINFO_0_SECUREMERCHANTACCOUNTID":"R5XBDZPA5X8ZJ","PAYMENTINFO_0_ERRORCODE":"0","PAYMENTINFO_0_ACK":"Success"}}', '2015-01-02 13:56:54', 0, 'S', 'P', 'EC-53923871T7644784Y', NULL),
(35, 26, '', 'R', '', 60, 0, '{"response":["3","2","33","Recurring Billing is required.","","P","0","","Sample Transaction","0.10","CC","auth_capture","","Girish","Kargathara","","Rajkot","","GJ","360001","","9662025100","","girish.kargathara@gmail.com","","","","","","","","","","","","","","E967EFB28D09D6D773A167D3642D1F8C","","","","","","","","","","","","","XXXX2548","Visa","","","","","","","","","","","","","","","",""]}', '2015-01-02 16:30:15', NULL, 'F', 'A', '', 'Recurring Billing is required.'),
(36, 27, '', 'R', '', 40, 0, '', '2015-01-02 17:36:24', NULL, 'P', 'P', '', NULL),
(37, 28, '', 'R', '', 60, 0, '', '2015-01-02 17:39:00', NULL, 'P', 'P', '', NULL),
(38, 29, '', 'R', '', 60, 0, '', '2015-01-02 17:41:36', NULL, 'P', 'P', '', NULL),
(39, 30, '', 'R', '', 60, 0, '', '2015-01-02 18:36:32', NULL, 'P', 'P', 'EC-9XJ076421R288942N', NULL),
(40, 31, '', 'R', '', 60, 0, '{"response":["2","2","27","The transaction has been declined because of an AVS mismatch. The address provided does not match billing address of cardholder.","010224","S","6804446311","","Sample Transaction","0.10","CC","auth_capture","","Girish","Kargathara","","Rajkot","","GJ","360001","","9662025100","","girish.kargathara@gmail.com","","","","","","","","","","","","","","CE4FB6690F7C9384C489843C328DF79C","","","","","","","","","","","","","XXXX2548","Visa","","","","","","","","","","","","","","","",""]}', '2015-01-02 19:22:04', NULL, 'F', 'A', '', 'The transaction has been declined because of an AVS mismatch. The address provided does not match billing address of cardholder.'),
(41, 32, '', 'R', '', 60, 0, '{"response":["3","1","11","A duplicate transaction has been submitted.","","P","0","","Sample Transaction","0.10","CC","auth_capture","","Girish","Kargathara","","Rajkot","","GJ","360001","","9662025100","","girish.kargathara@gmail.com","","","","","","","","","","","","","","E967EFB28D09D6D773A167D3642D1F8C","","","","","","","","","","","","","XXXX2548","Visa","","","","","","","","","","","","","","","",""]}', '2015-01-02 20:05:10', NULL, 'F', 'A', '', 'A duplicate transaction has been submitted.'),
(42, 33, '', 'R', '', 40, 0, '{"response":["2","2","27","The transaction has been declined because of an AVS mismatch. The address provided does not match billing address of cardholder.","029479","S","6804525775","","Sample Transaction","0.10","CC","auth_capture","","Girish","Kargathara","","Rajkot","","GJ","360001","","9662025100","","girish.kargathara@gmail.com","","","","","","","","","","","","","","A178EDA6E3C94435C603AD7A03944E8D","","","","","","","","","","","","","XXXX2548","Visa","","","","","","","","","","","","","","","",""]}', '2015-01-02 20:18:46', NULL, 'S', 'A', '', NULL),
(43, 18, '6805133352', 'B', '', 9, 0, '{"response":["2","2","27","The transaction has been declined because of an AVS mismatch. The address provided does not match billing address of cardholder.","015386","S","6805133352","","Sample Transaction","0.10","CC","auth_capture","","Girish","Kargathara","","301 Golden plaza, tagore road","Rajkot","Gujrat","360002","","9662050123","","girish.kargathara@gmail.com","","","","","","","","","","","","","","6FCE01426F983F14D8A699459C0E0649","","","","","","","","","","","","","XXXX2548","Visa","","","","","","","","","","","","","","","",""]}', '2015-01-02 22:44:28', 0, 'S', 'A', '', NULL),
(44, 18, '6805231907', 'B', '', 10, 0, '{"response":["2","2","27","The transaction has been declined because of an AVS mismatch. The address provided does not match billing address of cardholder.","022209","S","6805231907","","Sample Transaction","0.01","CC","auth_capture","","Girish","Kargathara","","301 Golden plaza, tagore road","Rajkot","Gujrat","360002","","9662050123","","girish.kargathara@gmail.com","","","","","","","","","","","","","","E3EC88469C8FD8FD6E33679609AE23A8","","","","","","","","","","","","","XXXX2548","Visa","","","","","","","","","","","","","","","",""]}', '2015-01-02 23:01:15', 2, 'S', 'A', '', NULL),
(45, 18, '', 'B', '', 0, 9, '', '2015-01-08 19:22:44', 3, 'S', 'S', '', NULL),
(46, 18, '', 'B', '', 10, 0, '', '2015-01-13 11:04:05', 4, 'P', 'P', '', NULL),
(47, 18, '', 'S', '', 20, 0, '', '2015-01-18 14:42:39', NULL, 'P', 'P', 'EC-1P093757P2108832B', NULL),
(48, 18, '', 'S', '', 20, 0, '', '2015-01-18 15:28:59', NULL, 'P', 'P', 'EC-92565742WS8771616', NULL),
(49, 18, '', 'S', '', 20, 0, '', '2015-01-18 15:41:21', NULL, 'P', 'P', 'EC-8CY04545K4447033L', NULL),
(50, 18, '', 'B', '', 0, 9, '', '2015-02-21 19:59:44', 5, 'S', 'S', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status_constants`
--

CREATE TABLE IF NOT EXISTS `status_constants` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Value` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Db_Table` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) DEFAULT NULL,
  `Password` varchar(30) DEFAULT NULL,
  `Salutation` varchar(5) DEFAULT NULL,
  `First_Name` varchar(30) DEFAULT NULL,
  `Middle_Name` varchar(30) DEFAULT NULL,
  `Last_Name` varchar(30) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Phone` varchar(16) DEFAULT NULL,
  `City` varchar(64) DEFAULT NULL,
  `Country` varchar(64) DEFAULT NULL,
  `Last_Loggedin` datetime DEFAULT NULL,
  `IP` varchar(255) DEFAULT NULL,
  `Entry_Date` datetime DEFAULT NULL,
  `Last_Modified` datetime DEFAULT NULL,
  `Status` enum('P','A','B') DEFAULT 'A' COMMENT 'A:Active; B:Blocked',
  `Reg_Status` enum('P','S','U') DEFAULT 'P' COMMENT 'P=Panding, S=Success, U=Unsuccess',
  `Credits` double NOT NULL DEFAULT '0',
  `TimeZone` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`Id`, `Username`, `Password`, `Salutation`, `First_Name`, `Middle_Name`, `Last_Name`, `Email`, `Phone`, `City`, `Country`, `Last_Loggedin`, `IP`, `Entry_Date`, `Last_Modified`, `Status`, `Reg_Status`, `Credits`, `TimeZone`) VALUES
(18, 'student', 'student', 'Mr', 'Student', '', '', '', '', '', '', NULL, NULL, '2014-09-26 18:44:19', '2015-02-06 22:14:47', 'A', 'S', 1, 0),
(19, 'hva', 'hva', 'Mr', 'aksjd', NULL, 'lskdjfls', NULL, NULL, '', '', NULL, NULL, '2014-12-10 10:55:44', '2014-12-10 10:55:44', 'P', 'P', 0, 330),
(21, 'abc', 'abc', 'Mr', 'Tom', 'n', 'Crus', NULL, NULL, 'lkjl', '', NULL, NULL, '2014-12-17 04:22:55', '2014-12-17 04:22:55', 'P', 'S', 0, 330),
(22, 'hva', 'hav', 'Mr', 'slkdjf', 'skdjf', 'lldfgjl', NULL, NULL, 'dkfgjlk', 'dklfjl', NULL, NULL, '2015-01-02 11:06:13', '2015-01-02 11:06:13', 'P', 'P', 0, NULL),
(23, 'root', 'root', 'Mr', 'ksdfjks', NULL, 'skdfjl', 'root@gmail.com', NULL, NULL, NULL, NULL, NULL, '2015-01-02 12:52:15', '2015-01-02 12:52:15', 'P', 'U', 0, NULL),
(24, 'a', 'a', 'Mr', 'Student', NULL, NULL, 'adiechahari@gmail.com', NULL, NULL, NULL, NULL, NULL, '2015-01-02 12:55:36', '2015-01-02 12:55:36', 'P', 'S', 0, NULL),
(25, 'b', 'b', 'Mr', 'b', 'b', 'b', 'b@c.com', NULL, NULL, NULL, NULL, NULL, '2015-01-02 12:58:18', '2015-01-02 12:58:18', 'P', 'U', 0, NULL),
(26, 'blank', 'b', 'Mr', 'Harikrushna', 'V.', 'Adiecha', 'b@x.com', '+919033319723', 'Rajkot', 'India', NULL, NULL, '2015-01-02 16:30:14', '2015-01-02 16:30:14', 'P', 'U', 0, NULL),
(27, 'asdas', 'a', 'Mr', 'HK', NULL, 'Gajjar', 'hari.codegeeks@gmail.com', NULL, NULL, NULL, NULL, NULL, '2015-01-02 17:36:23', '2015-01-02 17:36:23', 'P', 'P', 0, NULL),
(28, 'x', 'x', 'Mr', NULL, NULL, NULL, 'x@a.com', NULL, NULL, NULL, NULL, NULL, '2015-01-02 17:39:00', '2015-01-02 17:39:00', 'P', 'P', 0, NULL),
(29, 'x', 'd', 'Mr', NULL, NULL, NULL, 'x@a.com', NULL, NULL, NULL, NULL, NULL, '2015-01-02 17:41:35', '2015-01-02 17:41:35', 'P', 'P', 0, NULL),
(30, 'hk', 'hk', 'Mr', 'Hari', NULL, 'Krushna', 'hk.adiecha@gmail.com', NULL, NULL, NULL, NULL, NULL, '2015-01-02 18:36:31', '2015-01-02 18:36:31', 'P', 'P', 0, NULL),
(31, 'hk1', 'h', 'Mr', 'Harikrushna', 'V.', 'Adiecha', 'hk.adiecha@gmail.com', '+919033319723', 'Rajkot', 'India', NULL, NULL, '2015-01-02 19:22:03', '2015-01-02 19:22:03', 'P', 'U', 0, NULL),
(32, 'gk', 'gk', 'Mr', 'Girish', NULL, 'Gajjar', 'gk@gk.com', NULL, NULL, NULL, NULL, NULL, '2015-01-02 20:05:09', '2015-01-02 20:05:09', 'P', 'U', 0, NULL),
(33, 'D', 'd', 'Mr', 'Harikrushna', 'V.', 'Adiecha', 'b@xs.com', '+919033319723', 'Rajkot', 'India', NULL, NULL, '2015-01-02 20:18:45', '2015-01-02 20:18:45', 'A', 'S', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `students_courses`
--

CREATE TABLE IF NOT EXISTS `students_courses` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Student_Id` int(11) DEFAULT NULL,
  `Course_Id` int(11) DEFAULT NULL,
  `No_Of_Levels` int(4) DEFAULT NULL,
  `Completed_Levels` int(4) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Entry_Date` datetime DEFAULT NULL,
  `Last_Modified` datetime DEFAULT NULL,
  `Feedback` text,
  `Rating` int(1) DEFAULT NULL,
  `Note` text,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`,`Student_Id`,`Course_Id`),
  KEY `Id_2` (`Id`),
  KEY `Student_Id` (`Student_Id`),
  KEY `Course_Id` (`Course_Id`),
  KEY `Student_Id_2` (`Student_Id`,`Course_Id`),
  KEY `Status` (`Status`),
  KEY `Student_Id_3` (`Student_Id`,`Course_Id`,`Status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

--
-- Dumping data for table `students_courses`
--

INSERT INTO `students_courses` (`Id`, `Student_Id`, `Course_Id`, `No_Of_Levels`, `Completed_Levels`, `Status`, `Entry_Date`, `Last_Modified`, `Feedback`, `Rating`, `Note`) VALUES
(31, 38, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 38, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 38, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 39, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 39, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 39, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 40, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 40, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 40, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 41, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 41, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 41, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 21, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 21, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 21, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 22, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 22, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 22, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 19, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 19, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 19, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 21, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 21, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 21, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 22, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 22, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 22, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 23, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 23, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 23, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 24, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 24, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 25, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 25, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 25, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 26, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 26, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 26, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 27, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 27, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 27, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 28, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 28, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 28, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 29, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 29, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 29, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 30, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 30, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 30, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 31, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 31, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 31, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 32, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 32, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 32, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, 33, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 33, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 18, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 18, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 18, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `students_courses_comments`
--

CREATE TABLE IF NOT EXISTS `students_courses_comments` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Student_Id` int(11) DEFAULT NULL,
  `Course_Id` int(11) DEFAULT NULL,
  `Previous_Status` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Comment` text,
  `Entry_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`,`Student_Id`,`Course_Id`,`Status`),
  KEY `Student_Id` (`Student_Id`,`Course_Id`,`Status`),
  KEY `Student_Id_2` (`Student_Id`,`Course_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `students_credits`
--

CREATE TABLE IF NOT EXISTS `students_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule` int(11) NOT NULL,
  `paid_amount` int(11) NOT NULL,
  `satlement_amount` int(11) NOT NULL,
  `status` enum('DELIVERED','REJECTED','CANCELED','PAIED') NOT NULL DEFAULT 'PAIED',
  PRIMARY KEY (`id`),
  KEY `schedule` (`schedule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `students_schedules`
--

CREATE TABLE IF NOT EXISTS `students_schedules` (
  `Id` double NOT NULL AUTO_INCREMENT,
  `Student_Id` int(11) DEFAULT NULL,
  `Course_Id` int(11) DEFAULT NULL,
  `Level_Id` int(11) DEFAULT NULL,
  `Lesson_Id` int(11) NOT NULL,
  `Ref_Schedule_Id` double DEFAULT NULL,
  `Teacher_Id` int(11) DEFAULT NULL,
  `Schedule_On` datetime DEFAULT NULL,
  `Duration` varchar(4) DEFAULT NULL COMMENT 'Duration is on minutes slots, e.g. 15, 30, 60, 120, ...',
  `Teachers_Aval` int(11) DEFAULT NULL,
  `Entry_Date` datetime DEFAULT NULL,
  `Student_Note` text,
  `Teacher_Note` text,
  `Modified_On` datetime DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Schedule_Started` datetime DEFAULT NULL,
  `Schedule_Ended` datetime DEFAULT NULL,
  `Student_Feedback` text,
  `Student_Rating` int(1) DEFAULT NULL,
  `Teacher_Feedback` text,
  `Teacher_Rating` int(1) DEFAULT NULL,
  `Is_Schedule_Expired` enum('Y','N','X') DEFAULT NULL COMMENT 'Y:Yes; N:No; X:Being Processed',
  `Student_Reason` text,
  `Teacher_Reason` text,
  `lesson_order` int(11) DEFAULT NULL,
  `deleted_by_student` int(1) NOT NULL DEFAULT '0',
  `deleted_by_teacher` int(1) NOT NULL DEFAULT '0',
  `deleted_by_admin` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`,`Student_Id`,`Course_Id`,`Level_Id`,`Ref_Schedule_Id`,`Teacher_Id`,`Status`,`Is_Schedule_Expired`),
  KEY `Student_Id` (`Student_Id`,`Course_Id`),
  KEY `Student_Id_2` (`Student_Id`,`Course_Id`,`Teacher_Id`),
  KEY `Student_Id_3` (`Student_Id`,`Course_Id`,`Level_Id`,`Teacher_Id`),
  KEY `Student_Id_4` (`Student_Id`,`Course_Id`,`Level_Id`,`Teacher_Id`,`Status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `students_schedules`
--

INSERT INTO `students_schedules` (`Id`, `Student_Id`, `Course_Id`, `Level_Id`, `Lesson_Id`, `Ref_Schedule_Id`, `Teacher_Id`, `Schedule_On`, `Duration`, `Teachers_Aval`, `Entry_Date`, `Student_Note`, `Teacher_Note`, `Modified_On`, `Status`, `Schedule_Started`, `Schedule_Ended`, `Student_Feedback`, `Student_Rating`, `Teacher_Feedback`, `Teacher_Rating`, `Is_Schedule_Expired`, `Student_Reason`, `Teacher_Reason`, `lesson_order`, `deleted_by_student`, `deleted_by_teacher`, `deleted_by_admin`) VALUES
(1, 18, 21, NULL, 19, NULL, 17, '2014-12-30 09:00:00', '30', NULL, '2014-12-29 12:37:26', NULL, NULL, NULL, 0, NULL, NULL, 'test feedback', 2, NULL, NULL, 'N', NULL, NULL, NULL, 0, 0, 1),
(2, 18, 21, NULL, 20, NULL, 17, '2015-01-03 21:30:00', '30', NULL, '2015-01-02 23:01:15', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, 1, 0, 1),
(3, 18, 21, NULL, 19, NULL, 17, '2015-01-09 07:00:00', '30', NULL, '2015-01-08 19:22:44', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, 1, 1, 1),
(4, 18, 21, NULL, 20, NULL, 17, '2015-01-16 01:00:00', '30', NULL, '2015-01-13 11:04:05', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, 1, 0, 1),
(5, 18, 21, NULL, 19, NULL, 17, '2015-02-23 19:30:00', '30', 415, '2015-02-21 19:59:44', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `students_schedules_comments`
--

CREATE TABLE IF NOT EXISTS `students_schedules_comments` (
  `Id` double NOT NULL AUTO_INCREMENT,
  `Student_Id` int(11) DEFAULT NULL,
  `Course_Id` int(11) DEFAULT NULL,
  `Level_Id` int(11) DEFAULT NULL,
  `Previous_Status` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Comment` text,
  `Entry_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`,`Student_Id`,`Course_Id`,`Level_Id`,`Status`),
  KEY `Student_Id` (`Student_Id`,`Course_Id`),
  KEY `Student_Id_2` (`Student_Id`,`Course_Id`,`Status`),
  KEY `Student_Id_3` (`Student_Id`,`Course_Id`,`Level_Id`),
  KEY `Student_Id_4` (`Student_Id`,`Course_Id`,`Level_Id`,`Status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `student_additional_details`
--

CREATE TABLE IF NOT EXISTS `student_additional_details` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Student_Id` int(11) NOT NULL,
  `Gender` enum('F','M') NOT NULL,
  `Marital_Status` enum('U','M','W','S') NOT NULL COMMENT 'Unmarried, Married, Widow, Saperated',
  `Education` varchar(255) NOT NULL,
  `First_Language` varchar(255) NOT NULL,
  `DOB` date NOT NULL,
  `Address_Line1` varchar(255) NOT NULL,
  `Address_Line2` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `Zip_Code` int(11) NOT NULL,
  `State` varchar(255) NOT NULL,
  `Hear_From` varchar(255) NOT NULL,
  `Did_Before` text NOT NULL,
  `Why` text NOT NULL,
  `Reading_Proficiency` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE IF NOT EXISTS `teachers` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) DEFAULT NULL,
  `Password` varchar(30) DEFAULT NULL,
  `Salutation` varchar(5) DEFAULT NULL,
  `First_Name` varchar(30) DEFAULT NULL,
  `Middle_Name` varchar(30) DEFAULT NULL,
  `Last_Name` varchar(30) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Last_Loggedin` datetime DEFAULT NULL,
  `Phone` varchar(16) DEFAULT NULL,
  `City` varchar(64) DEFAULT NULL,
  `Country` varchar(64) DEFAULT NULL,
  `IP` varchar(255) DEFAULT NULL,
  `Entry_Date` datetime DEFAULT NULL,
  `Last_Modified` datetime DEFAULT NULL,
  `Status` enum('A','B') NOT NULL DEFAULT 'A' COMMENT 'A:Active; B:Blocked',
  `TimeZone` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`,`Username`,`Password`),
  KEY `Username` (`Username`,`Password`),
  KEY `Status` (`Status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`Id`, `Username`, `Password`, `Salutation`, `First_Name`, `Middle_Name`, `Last_Name`, `Email`, `Last_Loggedin`, `Phone`, `City`, `Country`, `IP`, `Entry_Date`, `Last_Modified`, `Status`, `TimeZone`) VALUES
(17, 'teacher', 'teacher', 'Ms', 'Teacher', 'dfgf', 'sdf', 'teacher@gmail.com', '2015-02-26 22:37:50', '5656', 'sdf', 'dfg', '127.0.0.1', '2014-09-26 18:43:33', '2015-01-06 16:26:12', 'A', 330),
(18, 'AdiechaHK', 'hk', 'Mr', 'Harikrushna', 'Vasantbhai', 'Adiecha', 'adiechahari@gmail.com', '2014-12-19 17:58:27', '9033319723', 'Rajkot', 'India', '127.0.0.1', '2014-11-30 20:56:51', '2014-12-15 17:47:57', 'A', 330),
(22, 'ksfjl', NULL, 'Mr', 'sjkdhf', 'kjflj', 'flkj', '', NULL, '980', 'kdljf', 'lkjdlfj', NULL, '2014-12-01 16:15:12', '2014-12-02 00:15:05', 'A', 330);

-- --------------------------------------------------------

--
-- Table structure for table `teachers_availabilities`
--

CREATE TABLE IF NOT EXISTS `teachers_availabilities` (
  `Id` double NOT NULL AUTO_INCREMENT,
  `Summary_Id` int(11) NOT NULL,
  `Teacher_Id` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Hours` varchar(2) NOT NULL,
  `Minutes` varchar(2) NOT NULL,
  `Entry_Date` datetime NOT NULL,
  `Is_Scheduled` enum('Y','N','X') NOT NULL DEFAULT 'N' COMMENT 'Y:Yes; N:No; X:Being Processed',
  PRIMARY KEY (`Id`),
  KEY `Id` (`Id`,`Teacher_Id`),
  KEY `Is_Scheduled` (`Is_Scheduled`),
  KEY `Date` (`Date`),
  KEY `Summary_Id` (`Summary_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=496 ;

--
-- Dumping data for table `teachers_availabilities`
--

INSERT INTO `teachers_availabilities` (`Id`, `Summary_Id`, `Teacher_Id`, `Date`, `Hours`, `Minutes`, `Entry_Date`, `Is_Scheduled`) VALUES
(406, 105, 17, '2015-02-06', '2', '0', '2015-02-05 16:44:59', 'N'),
(407, 105, 17, '2015-02-06', '2', '30', '2015-02-05 16:44:59', 'N'),
(408, 105, 17, '2015-02-06', '3', '0', '2015-02-05 16:44:59', 'N'),
(409, 105, 17, '2015-02-06', '3', '30', '2015-02-05 16:45:00', 'N'),
(410, 105, 17, '2015-02-06', '4', '0', '2015-02-05 16:45:00', 'N'),
(411, 105, 17, '2015-02-06', '4', '30', '2015-02-05 16:45:00', 'N'),
(412, 105, 17, '2015-02-06', '5', '0', '2015-02-05 16:45:00', 'N'),
(413, 105, 17, '2015-02-06', '5', '30', '2015-02-05 16:45:00', 'N'),
(414, 105, 17, '2015-02-06', '6', '0', '2015-02-05 16:45:00', 'N'),
(415, 106, 17, '2015-02-23', '19', '30', '2015-02-21 19:58:34', 'Y'),
(416, 106, 17, '2015-02-23', '20', '0', '2015-02-21 19:58:34', 'N'),
(417, 106, 17, '2015-02-23', '20', '30', '2015-02-21 19:58:34', 'N'),
(418, 106, 17, '2015-02-23', '21', '0', '2015-02-21 19:58:34', 'N'),
(419, 106, 17, '2015-02-23', '21', '30', '2015-02-21 19:58:34', 'N'),
(420, 106, 17, '2015-02-23', '22', '0', '2015-02-21 19:58:34', 'N'),
(421, 106, 17, '2015-02-23', '22', '30', '2015-02-21 19:58:34', 'N'),
(422, 106, 17, '2015-02-23', '23', '0', '2015-02-21 19:58:34', 'N'),
(423, 107, 17, '2015-02-24', '0', '0', '2015-02-21 19:58:34', 'N'),
(424, 107, 17, '2015-02-24', '0', '30', '2015-02-21 19:58:34', 'N'),
(425, 107, 17, '2015-02-24', '1', '0', '2015-02-21 19:58:34', 'N'),
(426, 107, 17, '2015-02-24', '1', '30', '2015-02-21 19:58:34', 'N'),
(427, 107, 17, '2015-02-24', '2', '0', '2015-02-21 19:58:34', 'N'),
(428, 107, 17, '2015-02-24', '2', '30', '2015-02-21 19:58:35', 'N'),
(429, 108, 17, '2015-02-24', '19', '30', '2015-02-21 19:58:36', 'N'),
(430, 108, 17, '2015-02-24', '20', '0', '2015-02-21 19:58:36', 'N'),
(431, 108, 17, '2015-02-24', '20', '30', '2015-02-21 19:58:36', 'N'),
(432, 108, 17, '2015-02-24', '21', '0', '2015-02-21 19:58:36', 'N'),
(433, 108, 17, '2015-02-24', '21', '30', '2015-02-21 19:58:36', 'N'),
(434, 108, 17, '2015-02-24', '22', '0', '2015-02-21 19:58:36', 'N'),
(435, 108, 17, '2015-02-24', '22', '30', '2015-02-21 19:58:36', 'N'),
(436, 108, 17, '2015-02-24', '23', '0', '2015-02-21 19:58:36', 'N'),
(437, 109, 17, '2015-02-25', '0', '0', '2015-02-21 19:58:36', 'N'),
(438, 109, 17, '2015-02-25', '0', '30', '2015-02-21 19:58:36', 'N'),
(439, 109, 17, '2015-02-25', '1', '0', '2015-02-21 19:58:36', 'N'),
(440, 109, 17, '2015-02-25', '1', '30', '2015-02-21 19:58:37', 'N'),
(441, 109, 17, '2015-02-25', '2', '0', '2015-02-21 19:58:37', 'N'),
(442, 110, 17, '2015-02-25', '19', '30', '2015-02-21 19:58:39', 'N'),
(443, 110, 17, '2015-02-25', '20', '0', '2015-02-21 19:58:39', 'N'),
(444, 110, 17, '2015-02-25', '20', '30', '2015-02-21 19:58:39', 'N'),
(445, 110, 17, '2015-02-25', '21', '0', '2015-02-21 19:58:39', 'N'),
(446, 110, 17, '2015-02-25', '21', '30', '2015-02-21 19:58:39', 'N'),
(447, 110, 17, '2015-02-25', '22', '0', '2015-02-21 19:58:39', 'N'),
(448, 110, 17, '2015-02-25', '22', '30', '2015-02-21 19:58:39', 'N'),
(449, 110, 17, '2015-02-25', '23', '0', '2015-02-21 19:58:39', 'N'),
(450, 111, 17, '2015-02-26', '0', '0', '2015-02-21 19:58:39', 'N'),
(451, 111, 17, '2015-02-26', '0', '30', '2015-02-21 19:58:39', 'N'),
(452, 111, 17, '2015-02-26', '1', '0', '2015-02-21 19:58:39', 'N'),
(453, 111, 17, '2015-02-26', '1', '30', '2015-02-21 19:58:40', 'N'),
(454, 111, 17, '2015-02-26', '2', '0', '2015-02-21 19:58:40', 'N'),
(455, 112, 17, '2015-02-26', '19', '30', '2015-02-21 19:58:43', 'N'),
(456, 112, 17, '2015-02-26', '20', '0', '2015-02-21 19:58:43', 'N'),
(457, 112, 17, '2015-02-26', '20', '30', '2015-02-21 19:58:43', 'N'),
(458, 112, 17, '2015-02-26', '21', '0', '2015-02-21 19:58:43', 'N'),
(459, 112, 17, '2015-02-26', '21', '30', '2015-02-21 19:58:43', 'N'),
(460, 112, 17, '2015-02-26', '22', '0', '2015-02-21 19:58:43', 'N'),
(461, 112, 17, '2015-02-26', '22', '30', '2015-02-21 19:58:43', 'N'),
(462, 112, 17, '2015-02-26', '23', '0', '2015-02-21 19:58:43', 'N'),
(463, 113, 17, '2015-02-27', '0', '0', '2015-02-21 19:58:44', 'N'),
(464, 113, 17, '2015-02-27', '0', '30', '2015-02-21 19:58:44', 'N'),
(465, 113, 17, '2015-02-27', '1', '0', '2015-02-21 19:58:44', 'N'),
(466, 113, 17, '2015-02-27', '1', '30', '2015-02-21 19:58:44', 'N'),
(467, 113, 17, '2015-02-27', '2', '0', '2015-02-21 19:58:44', 'N'),
(468, 113, 17, '2015-02-27', '2', '30', '2015-02-21 19:58:44', 'N'),
(469, 114, 17, '2015-02-27', '19', '30', '2015-02-21 19:58:46', 'N'),
(470, 114, 17, '2015-02-27', '20', '0', '2015-02-21 19:58:46', 'N'),
(471, 114, 17, '2015-02-27', '20', '30', '2015-02-21 19:58:46', 'N'),
(472, 114, 17, '2015-02-27', '21', '0', '2015-02-21 19:58:46', 'N'),
(473, 114, 17, '2015-02-27', '21', '30', '2015-02-21 19:58:46', 'N'),
(474, 114, 17, '2015-02-27', '22', '0', '2015-02-21 19:58:46', 'N'),
(475, 114, 17, '2015-02-27', '22', '30', '2015-02-21 19:58:46', 'N'),
(476, 114, 17, '2015-02-27', '23', '0', '2015-02-21 19:58:46', 'N'),
(477, 115, 17, '2015-02-28', '0', '0', '2015-02-21 19:58:46', 'N'),
(478, 115, 17, '2015-02-28', '0', '30', '2015-02-21 19:58:46', 'N'),
(479, 115, 17, '2015-02-28', '1', '0', '2015-02-21 19:58:46', 'N'),
(480, 115, 17, '2015-02-28', '1', '30', '2015-02-21 19:58:46', 'N'),
(481, 115, 17, '2015-02-28', '2', '0', '2015-02-21 19:58:46', 'N'),
(482, 115, 17, '2015-02-28', '2', '30', '2015-02-21 19:58:46', 'N'),
(483, 116, 17, '2015-02-28', '19', '30', '2015-02-21 19:58:48', 'N'),
(484, 116, 17, '2015-02-28', '20', '0', '2015-02-21 19:58:48', 'N'),
(485, 116, 17, '2015-02-28', '20', '30', '2015-02-21 19:58:48', 'N'),
(486, 116, 17, '2015-02-28', '21', '0', '2015-02-21 19:58:48', 'N'),
(487, 116, 17, '2015-02-28', '21', '30', '2015-02-21 19:58:48', 'N'),
(488, 116, 17, '2015-02-28', '22', '0', '2015-02-21 19:58:48', 'N'),
(489, 116, 17, '2015-02-28', '22', '30', '2015-02-21 19:58:48', 'N'),
(490, 116, 17, '2015-02-28', '23', '0', '2015-02-21 19:58:48', 'N'),
(491, 117, 17, '2015-03-01', '0', '0', '2015-02-21 19:58:49', 'N'),
(492, 117, 17, '2015-03-01', '0', '30', '2015-02-21 19:58:49', 'N'),
(493, 117, 17, '2015-03-01', '1', '0', '2015-02-21 19:58:49', 'N'),
(494, 117, 17, '2015-03-01', '1', '30', '2015-02-21 19:58:49', 'N'),
(495, 117, 17, '2015-03-01', '2', '0', '2015-02-21 19:58:49', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_availability_summary`
--

CREATE TABLE IF NOT EXISTS `teacher_availability_summary` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Teacher_Id` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Start_Hr` int(2) NOT NULL,
  `Start_Min` int(2) NOT NULL,
  `End_Hr` int(2) NOT NULL,
  `End_Min` int(2) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

--
-- Dumping data for table `teacher_availability_summary`
--

INSERT INTO `teacher_availability_summary` (`Id`, `Teacher_Id`, `Date`, `Start_Hr`, `Start_Min`, `End_Hr`, `End_Min`) VALUES
(105, 17, '2015-02-06', 2, 0, 6, 30),
(106, 17, '2015-02-23', 19, 30, 23, 30),
(107, 17, '2015-02-24', 0, 0, 3, 0),
(108, 17, '2015-02-24', 19, 30, 23, 30),
(109, 17, '2015-02-25', 0, 0, 2, 30),
(110, 17, '2015-02-25', 19, 30, 23, 30),
(111, 17, '2015-02-26', 0, 0, 2, 30),
(112, 17, '2015-02-26', 19, 30, 23, 30),
(113, 17, '2015-02-27', 0, 0, 3, 0),
(114, 17, '2015-02-27', 19, 30, 23, 30),
(115, 17, '2015-02-28', 0, 0, 3, 0),
(116, 17, '2015-02-28', 19, 30, 23, 30),
(117, 17, '2015-03-01', 0, 0, 2, 30);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `teachers_availabilities`
--
ALTER TABLE `teachers_availabilities`
  ADD CONSTRAINT `teachers_availabilities_ibfk_1` FOREIGN KEY (`Summary_Id`) REFERENCES `teacher_availability_summary` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
