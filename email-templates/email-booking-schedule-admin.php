<?php 
include 'email-top.php'; 

// $_SESSION['payment_id'] = 27;
// $_SESSION['schedule_id'] = 53;

$StudentName = "";
$StudentEmail = "";
$TeacherName = "";
$TeacherEmail = "";
$ScheduleDate = "";
$LessonDetail = "";




$query = "SELECT p.Sid, s.First_Name AS SFNM, s.Last_Name AS SLNM, s.Email AS SEML, 
	t.First_Name AS TFNM, t.Last_Name TLNM, t.Email TEML,
	DATE_ADD(ss.Schedule_On, INTERVAL ".TIMEZONE." MINUTE) AS Schedule_Admin,
	l.Lesson 
	FROM payment p, students_schedules ss 
		LEFT JOIN students s ON s.Id = ss.Student_Id  
		LEFT JOIN teachers t ON t.Id = ss.Teacher_Id 
		LEFT JOIN lessons l ON l.Id = ss.Lesson_Id 
	WHERE p.Id = ".$_SESSION['payment_id']." AND ss.Id = `p`.`Schedule_Id`";
$result = mysql_query($query) or print("Error :: Cannot select booking details.<hr>".mysql_error());
while($arow = mysql_fetch_assoc($result)) {
	if($StudentName == "") {
		$StudentName = $arow["SFNM"]." ".$arow["SLNM"];
		$StudentEmail = $arow["SEML"];
		$TeacherName = $arow["TFNM"]." ".$arow["TLNM"];
		$TeacherEmail = $arow["TEML"];

		$ScheduleDate = $arow["Schedule_Admin"];
		$LessonDetail = $arow["Lesson"];
	}
}

	$DearName = ADM_NAME;
	$email_to = ADM_EMAIL;
	$email_subject = "Alhuda flexible - Lesson booking";
?>

Dear <?=ADM_NAME?>, <br /><br />
A new booking for lesson has been scheduled.<br /><br />
<u>Schedule Details</u><br />
<strong>Lesson</strong><br /><?php echo $LessonDetail; ?><br />
<strong>Teacher</strong><br /><?php echo $TeacherName; ?><br />
<strong>Student</strong><br /><?php echo $StudentName; ?><br />
<strong>Schedule On</strong><br /><?php echo $ScheduleDate; ?><br /><br />

<?php include 'email-bottom.php'; ?>