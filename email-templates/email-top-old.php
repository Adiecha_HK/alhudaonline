<?php include '../inc/config.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title><?php echo TITLE; ?></title>

<style type="text/css">
         #outlook a {padding:0;}
         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
         .ExternalClass {width:100%;} 
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} 
         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
         a img {border:none;}
         .image_fix {display:block;}
         p {margin: 0px 0px !important;}
         table td {border-collapse: collapse;}
         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
         a {color: #33b9ff;text-decoration: none;text-decoration:none!important;}

         table[class=full] { width: 100%; clear: both; }
        
      </style>
</head>
<body>
<?php $class_val=0; ?>

<table width="100%" bgcolor="#cccccc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
   <tbody>
      <tr>
         <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
      </tr>
   </tbody>
</table>
<table width="100%" bgcolor="#cccccc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
   <tbody>
      <tr>
         <td>
            <table width="90%" style="background: none repeat scroll 0 0 rgb(29, 71, 113);" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                  	<td height="20">&nbsp;</td>
                  	</tr>
                  <tr>
                     <td width="100%">
					 
					 	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="20"></td>
								<td><a href="<?php echo URL; ?>" ><img src="./images/logo.png" alt="<?php echo URL; ?>" title="<?php echo URL; ?>"/></a></td>
								<td width="20"></td>
							</tr>
						</table>
	 
					 </td>
                  </tr>
                  <tr>
                  	<td height="20">&nbsp;</td>
                  	</tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<table width="100%" bgcolor="#cccccc" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
   <tbody>
      <tr>
         <td>
            <table width="90%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                     	<tr>
                     		<td width="20" height="20">&nbsp;</td>
                     		<td>&nbsp;</td>
                     		<td width="20" height="20">&nbsp;</td>
                     		</tr>
                     	<tr>
                     		<td>&nbsp;</td>
                     		<td><table cellpadding="0" cellspacing="0" border="0" width="100%" class="devicewidth">
                     			<tbody>
                     				<tr>
                     					<td style="font-family: Helvetica, arial, sans-serif; font-size: 15px; color: #666666; text-align:left; line-height: 20px;">
									Bismillah,<br /><br />