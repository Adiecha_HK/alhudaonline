<?php 
ob_start();
include 'email-top.php'; 

// $_SESSION['payment_id'] = 30;

$DearName = "";
$email_to = "";
$StudentCourses = array();

$query = "SELECT p.Sid, s.First_Name, s.Last_Name, s.Email, c.Course, c.Code 
	FROM payment p 
		LEFT JOIN students s ON s.Id = p.Sid
		LEFT JOIN students_courses sc ON sc.Student_Id = s.Id 
		LEFT JOIN courses c ON c.Id = sc.Course_Id 
	WHERE p.Id = ".$_SESSION['payment_id'];
$result = mysql_query($query) or print("Error :: Cannot select student details.<hr>".mysql_error());
while($arow = mysql_fetch_assoc($result)) {
	if($DearName == "") {
		$DearName = $arow["First_Name"]." ".$arow["Last_Name"];
		$email_to = $arow["Email"];
	}
	if(isset($arow["Course"])) {
		array_push($StudentCourses, $arow["Course"].": ".generate_regCode($arow["Sid"], $arow["Code"]));
	}
}

?>
Dear <?php echo $DearName; ?>,<br /><br />
Assalamualaikum,<br /><br />
Jazakallahukhairun your Registration in the Course is Confirmed.<br /><br />
Your registration number for exams and all Correspondence is XXXXX.<br /><br /> 
We warmly and happily Welcome to Al-Huda Flexible Schedule Course. Mashallah you have taken the Best Decision of your life by making an effort to learn the knowledge of Deen. We pray that Allah swt make this journey easy and beneficial for you.  Ameen<br /><br /> 
<a href="<?=HTTP.$_SERVER['HTTP_HOST'].'/login/'?>">Starting Course</a><br /><br />
<?php echo implode("<br />", $StudentCourses); ?><br /><br />
You are all Set and can start your Course today by Booking Lessons from the link here.<br /><br />
<a href="<?=HTTP.$_SERVER['HTTP_HOST'].'/faq/'?>">FAQs</a><br /><br />
Here is the link for FAQs about Flexible Schedule Courses.<br /><br />
We are looking forward to help and support you in this Journey of Learning. Do not hesitate to contact us with any questions or concerns.<br /><br />
May Allah swt bless Barakah in your knowledge, time and Skills. Ameen<br /><br />
Lots of Duas and Best Wishes,<br /><br />
<?php
/* <a href="<?php echo URL; ?>" target="_blank">Click here</a> to verify your email address.<br /><br /> */ ?>
<?php
$admins = TRUE;
$email_subject = "Welcome to Al-Huda Flexible Schedule Courses BarakallahuFikum";
include 'email-bottom.php'; 
?>