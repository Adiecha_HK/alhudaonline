<?php 
include 'email-top.php'; 

// $_SESSION['payment_id'] = 19;

$DearName = "Admin";
$StudentName = "";
$StudentCourses = array();

$query = "SELECT p.Sid, s.First_Name, s.Last_Name, s.Email, c.Course 
	FROM payment p 
		LEFT JOIN students s ON s.Id = p.Sid
		LEFT JOIN students_courses sc ON sc.Student_Id = s.Id 
		LEFT JOIN courses c ON c.Id = sc.Course_Id 
	WHERE p.Id = ".$_SESSION['payment_id'];
$result = mysql_query($query) or print("Error :: Cannot select student details.<hr>".mysql_error());
while($arow = mysql_fetch_assoc($result)) {
	if($StudentName == "") {
		$StudentName = $arow["First_Name"]." ".$arow["Last_Name"];
	}
	array_push($StudentCourses, $arow["Course"]);
}
?>
Dear <?=$DearName?>, <br /><br />
A new student is registered recently. Below is brief detail of student.<br /><br />
<strong>Name</strong><br /><?php echo $StudentName; ?><br />
<strong>Course(s)</strong><br /><?php echo implode("<br />", $StudentCourses); ?><br /><br />

<?php 
$email_to = ADM_EMAIL;
$email_subject = "New Registration in Alhuda Flexible";

include 'email-bottom.php'; ?>