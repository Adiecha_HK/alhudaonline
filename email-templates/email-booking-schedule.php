<?php 
include 'email-top.php'; 

// $_SESSION['payment_id'] = 27;
// $_SESSION['schedule_id'] = 53;

$StudentName = "";
$StudentEmail = "";
$TeacherName = "";
$TeacherEmail = "";
$ScheduleDate = "";
$LessonDetail = "";




$query = "SELECT p.Sid, s.First_Name AS SFNM, s.Last_Name AS SLNM, s.Email AS SEML, 
	t.First_Name AS TFNM, t.Last_Name TLNM, t.Email TEML,
	DATE_ADD(ss.Schedule_On, INTERVAL s.TimeZone MINUTE) AS Schedule_Student,
	DATE_ADD(ss.Schedule_On, INTERVAL t.TimeZone MINUTE) AS Schedule_Teacher,
	l.Lesson 
	FROM payment p, students_schedules ss 
		LEFT JOIN students s ON s.Id = ss.Student_Id  
		LEFT JOIN teachers t ON t.Id = ss.Teacher_Id 
		LEFT JOIN lessons l ON l.Id = ss.Lesson_Id 
	WHERE p.Id = ".$_SESSION['payment_id']." AND ss.Id = `p`.`Schedule_Id`";
$result = mysql_query($query) or print("Error :: Cannot select booking details.<hr>".mysql_error());
while($arow = mysql_fetch_assoc($result)) {
	if($StudentName == "") {
		$StudentName = $arow["SFNM"]." ".$arow["SLNM"];
		$StudentEmail = $arow["SEML"];
		$TeacherName = $arow["TFNM"]." ".$arow["TLNM"];
		$TeacherEmail = $arow["TEML"];

		$ScheduleDate = $email_for == "S" ? $arow["Schedule_Student"]: ($email_for == "T" ? $arow["Schedule_Teacher"]: $arow["Schedule_Admin"]);
		$LessonDetail = $arow["Lesson"];
	}
}
?>

<?php if($email_for == "S") { // S for student 
	$DearName = $StudentName;
	$email_to = $StudentEmail;
	$email_subject = "Your booking done successfully";
?>

Dear <?php echo $StudentName; ?>, <br /><br />
Your booking for lesson has been successfully scheduled.<br /><br />
<u>Schedule Details</u><br />
<strong>Lesson</strong><br /><?php echo $LessonDetail; ?><br />
<strong>Teacher</strong><br /><?php echo $TeacherName; ?><br />
<strong>Schedule On</strong><br /><?php echo $ScheduleDate; ?><br /><br />

<?php } else  if($email_for == "T") { // T for Teacher 
	$DearName = $TeacherName;
	$email_to = $TeacherEmail;
	$email_subject = "Student booked a lesson for you";
?>

Dear <?php echo $TeacherName; ?>, <br /><br />
A new booking for lesson has been scheduled.<br /><br />
<u>Schedule Details</u><br />
<strong>Lesson</strong><br /><?php echo $LessonDetail; ?><br />
<strong>Student</strong><br /><?php echo $StudentName; ?><br />
<strong>Schedule On</strong><br /><?php echo $ScheduleDate; ?><br /><br />

<?php } else { //  for Admin 
	/*
	$DearName = ADM_NAME;
	$email_to = ADM_EMAIL;
	$email_subject = "Alhuda flexible - Lesson booking";
?>

Dear <?=ADM_NAME?>, <br /><br />
A new booking for lesson has been scheduled.<br /><br />
<u>Schedule Details</u><br />
<strong>Lesson</strong><br /><?php echo $LessonDetail; ?><br />
<strong>Teacher</strong><br /><?php echo $TeacherName; ?><br />
<strong>Student</strong><br /><?php echo $StudentName; ?><br />
<strong>Schedule On</strong><br /><?php echo $ScheduleDate; ?><br /><br />
<?php
*/
 } ?>
<?php include 'email-bottom.php'; ?>