<?php 
include 'email-top.php'; 
$teacher = get_record("teachers", '', 'id=' . $_SESSION['teacher_id']);
$DearName = $teacher['First_Name'] . " " . $teacher['Middle_Name'] . " " . $teacher['Last_Name'];
?>

Dear <?php echo $DearName; ?>,<br /><br />
Assalamualaikum,<br /><br />
May Allah swt make this learning easy and beneficial for you. Ameen<br /><br />
Your registration form has been received. JazakAllahuKhayran.<br /><br /> 
Kindly confirm your Registration, you will receive Confirmation email with other details of the Course.<br /><br /> 

Link of to login: <a href="http://www.alhudaflexible.com/teacher-login">http://www.alhudaflexible.com/teacher-login</a><br/>
Username: <?=$teacher['Username']?><br/>
Password: <?=$teacher['Password']?><br/><br/>


<?php 
$email_to = $teacher['Email'];
$email_subject = "Alhuda Flexible Courses - Teacher Registration";
include 'email-bottom.php'; 
?>