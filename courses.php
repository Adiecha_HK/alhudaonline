<?php 
$page_nav="Courses";
$page_title="Course";
include 'header.php'; 
$sql_qry="SELECT c.Id AS Id, c.Course AS Course, c.Brief AS Brief, l.No_Of_Levels AS No_Of_Levels, (CONCAT(c.Duration,' ',IF(c.Duration_Type='D','Day(s)',IF(c.Duration_Type='W','Week(s)',IF(c.Duration_Type='M','Month(s)','Year(s)'))))) AS Duration FROM courses AS c LEFT JOIN (SELECT Course_Id AS cId, COUNT(Id) AS No_Of_Levels FROM `lessons` GROUP BY Course_Id) AS l ON l.cId=c.Id WHERE Id IN (SELECT Course_id From students_courses WHERE Student_Id='".$_SESSION["student_uniqueid"]."') ORDER BY Id DESC";
/*
echo $sql_qry;
*/
$sql_res=mysql_query($sql_qry) or die(error_mysql("Selecting Courses"));
$sql_nos=mysql_num_rows($sql_res);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL.'dashboard.php'?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Courses</li>
    </ul>
</div>
<!-- List Courses >>> -->
<table class="DataTable table">
  <tr class="DataTableHeading">
    <th width="27%" align="left">Course Name</th>
    <th width="44%" align="left">Details</th>
    <th width="11%" align="center">No. of Lessons</th>
    <th width="11%" align="center">Duration</th>
    <th width="7%" align="center">#</th>
  </tr> 
<?php
if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
?>
  <tr class="DataTableRow">
    <td width="27%" align="left"><?php echo escape_string($sql_row["Course"],"display"); ?></td>
    <td width="44%" align="left"><?php echo escape_string($sql_row["Brief"],"display"); ?></td>
    <td width="11%" align="center"><?php echo escape_string($sql_row["No_Of_Levels"],"display"); ?></td>
    <td width="11%" align="center"><?php echo escape_string($sql_row["Duration"],"display"); ?></td>
    <td width="7%" align="center"><a class="btn btn-info" href="lessons.php?cid=<?php echo $sql_row["Id"]; ?>" >view</a></td>
  </tr>
<?php
}}else{
?>
  <tr>
    <td colspan="7" align="center">There is no Course in database.</td>
  </tr>
<?php 
}
?>
</table>
<?php

  $sql_qry = "SELECT count(`Course_Type`) as cnt FROM `courses` WHERE id in (SELECT `Course_Id` FROM `students_courses` WHERE `Student_Id` = '".$_SESSION["student_uniqueid"]."') GROUP BY `Course_Type`";
  $sql_res = mysql_query($sql_qry);
  $sql_nos = mysql_num_rows($sql_res);
  $show_subscrib_course_btn = false;
  if($sql_nos == 0) {
    $show_subscrib_course_btn = true;
  } else {
    $rec = mysql_fetch_array($sql_res);
    $show_subscrib_course_btn = $rec['cnt'] < 3;
  }
  if($show_subscrib_course_btn) {
    ?>
    <a href="#myModal" role="button" class="btn btn-primary" data-toggle="modal">Subscribe new course</a>

    <!-- Modal -->
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="myModalLabel">Subscribe course</h3>
      </div>
      <form method="POST" action="subscribe-course.php" class="form-horizontal">
        <input type="hidden" name="amount" value="0">
        <div class="modal-body">
          <div class="control-group">
            <label class="control-label" for="course">Course:</label>
            <div class="controls">
              <select type="text" name="course" id="course" placeholder="Select course">
                <option value="0">-- Select Course --</option>
                <?php
                $sql_qry = "SELECT Id, Course, IF(Course_Type='TQ','Tafheem al Quran Courses',IF(Course_Type='TH','Tajweed & Hifdh Courses',IF(Course_Type='H','Audio Hadith & Tazkiyah Courses','N/A'))) AS Course_Type FROM `courses` WHERE `Course_Type` NOT IN (SELECT `Course_Type` as cType FROM `courses` WHERE id in (SELECT `Course_Id` FROM `students_courses` WHERE `Student_Id` = '".$_SESSION["student_uniqueid"]."') GROUP BY `Course_Type`) ORDER BY `Course_Type`, `Course`";
                $sql_res = mysql_query($sql_qry);

                $grp = "";
                while($rec = mysql_fetch_array($sql_res)) {
                  $cond = $grp != $rec['Course_Type'];
                  if($cond) {
                    if($grp != "") echo "</optgroup>";
                    $grp = $rec['Course_Type'];
                    echo "<optgroup label='".$grp."'>";
                  }
                  echo "<option value='".$rec['Id']."'>".$rec['Course']."</option>";
                }
                echo "</optgroup>";
                ?>
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">Amount:</label>
            <div class="controls" style="margin-top:5px">
              <span id="amount">0 $</span>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary">Subscribe</button>
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
      </form>
    </div>
    <script type="text/javascript">
      $("#course").on('change', function() {
        var val = 20;
        if($(this).val() == 0) {
          val = 0;
        }
        $("#amount").text(val+" $");
        $("input[name=amount]").val(val);
      })
    </script>
    <?php
  }
?>

<!-- List Courses <<< -->
</div>
<?php 
include 'footer-tchr.php'; 
?>
