<?php
include '../inc/config.php';
include '../inc/functions.php';
include '../email-templates/email-cancel-schedule-teacher.php';
$next_location = "../" . $_SESSION['final_redirect'];
header('location: ' . $next_location);
echo '<script type="text/javascript">window.location = ' . $next_location . '; </script>';

unset($_SESSION['schedule_id']);
unset($_SESSION['final_redirect']);

exit;

?>