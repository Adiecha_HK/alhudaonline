<?php
include '../inc/config.php';
include '../inc/functions.php';
include '../email-templates/email-add-teacher.php';
$next_location = "../" . $_SESSION['final_redirect'];
header('location: ' . $next_location);
echo '<script type="text/javascript">window.location = ' . $next_location . '; </script>';
unset($_SESSION['final_redirect']);
unset($_SESSION['teacher_id']);
exit;

?>