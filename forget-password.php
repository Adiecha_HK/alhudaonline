<?php
include './inc/config.php';
include './inc/functions.php';

$table = "students";
if(isset($_POST['uType']) && $_POST['uType'] == "t") {
	$table = "teachers";
}
$person = get_record($table, '', "`Email`='" . $_POST['email'] . "'");

if(isset($person)) {

	// SENDING MAIL
	include "./email-templates/email-forget-password.php";

	$_SESSION['thank_you'] = array(
		'title' => "Success",
		'message' => "We mailed you the credentials on " . $person['Email'],
		'links' => array(
			array('href'=>(isset($_POST['uType'])&&trim($_POST['uType']=="t")?"/teacher-login":"/login"), 'text'=>"Login"),
			array('href'=>"/", 'text'=>"Home")));
} else {

	$_SESSION['thank_you'] = array(
		'title' => "Fail",
		'message' => "No such user found",
		'links' => array(
			array('href'=>"/forget-password/".(isset($_POST['uType'])&&trim($_POST['uType'])!=""?"?type=".$_POST['uType']:""), 'text'=>"Try again"),
			array('href'=>"/", 'text'=>"Home")));

}

header('location: /thank-you');
?><script type="text/javascript"> window.location.href = "/thank-you"; </script>

