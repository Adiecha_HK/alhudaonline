<?php
function escape_string($val,$act="",$flg=""){
	if($flg=="db"){
		# use this function to display/insert values coming from sources other than Get/Post/Cookie. i.e. database/file etc.
		# parameter "act" should have a value "display" if it is being used for displaying value. 
		# in case of database update/insert the "act" can be left blank.
		if(!get_magic_quotes_runtime()) $val = addslashes(trim($val));
		if($act == "display") $val = stripslashes($val);
		return $val;	
	}else{
		# use this function to display/insert values coming from Get/Post/Cookie.
		# parameter "act" should have a value "display" if it is being used for displaying value. 
		# in case of database update/insert the "act" can be left blank.
		if(!get_magic_quotes_gpc()) $val = addslashes(trim($val));
		if($act == "display") $val = stripslashes($val);
		return $val;
	}
}
function check_compulsory_postvars(){
	global $compulsory_postvars,$errors; 
	foreach($compulsory_postvars as $key=>$val) if(@$_POST[$val]==""||@$_POST[$val]==NULL) array_push($errors, $key . " cannot be blank");
}
function show_errors($param=''){
	global $errors; 
	$error_file=($param=="adm")?"show_errors_adm.php":""; 
	if(count($errors)>0&&$error_file!="") include(INC.$error_file);
}
function page_nav($params){
	$return="";
	foreach($params as $link=>$val)
		$return.="<a href='".URl.$link."'>".$val."</a> >> ";
	$return=substr($return,0,-4);
	echo $return;
}
function get_records($tbl,$flds="",$whrcls="",$ordcls="",$limit=""){
	$return=array();
	if($flds!="") $sql_qry="SELECT ".$flds." ";
	else $sql_qry="SELECT * ";
	$sql_qry.="FROM ".$tbl." ";
	if($whrcls!="") $sql_qry.="WHERE 1=1 AND ".$whrcls." ";
	if($ordcls!="") $sql_qry.="ORDER BY ".$ordcls." ";	
	if($limit!="") $sql_qry.="LIMIT ".$limit;
  $sql_res=mysql_query($sql_qry) or die(session_err("Database error!", mysql_error()));
  if(mysql_num_rows($sql_res)>0) while($sql_row=mysql_fetch_assoc($sql_res)) $return[]=$sql_row;
  return $return;
}
function get_record($tbl,$flds="",$whrcls="",$ordcls=""){
  $list = get_records($tbl, $flds, $whrcls, $ordcls, 1);
  if(sizeof($list) > 0) return $list[0];
  else return null;
}

function get_count($tbl, $whrcls = "") {
	$return=array();
	$sql_qry = "SELECT count(*) AS count FROM ".$tbl;
	if($whrcls!="") $sql_qry.=" WHERE 1=1 AND ".$whrcls." ";
	$sql_res = mysql_query($sql_qry) or die(session_err("Database error!", "Cannot count " . $tbl . "'s record(s). [" . mysql_error() . "]"));
	if(mysql_num_rows($sql_res)>0) while($sql_row=mysql_fetch_assoc($sql_res)) $return[]=$sql_row;
	return $return[0]['count'];
}

function del_records($tbl, $whrcls = "") {
	$sql_qry = "DELETE FROM " . $tbl;
	if($whrcls!="") $sql_qry.=" WHERE 1=1 AND ".$whrcls." ";
	$sql_res = mysql_query($sql_qry) or die(session_err("Database error!", "Cannot delete " . $tbl . "'s record(s). [" . mysql_error() . "]"));
}

// $curr = array('hr'=>($_REQUEST['start_hr'] < 10 ? "0".$_REQUEST['start_hr']:$_REQUEST['start_hr']), 'min'=>($_REQUEST['start_min']<10?"0".$_REQUEST['start_min']:$_REQUEST['start_min']), 'meridian'=>$_REQUEST['start_meridian']);
// $end = array('hr'=>($_REQUEST['end_hr']<10?"0".$_REQUEST['end_hr']:$_REQUEST['end_hr']), 'min'=>($_REQUEST['end_min']<10?"0".$_REQUEST['end_min']:$_REQUEST['end_min']), 'meridian'=>$_REQUEST['end_meridian']);
// calender_insert_for_date($_SESSION['anybody_id'], $_REQUEST['date'], $curr, $end);


// check out example of formate from this link: http://php.net/manual/en/function.date.php#example-2398
function formated_date($date, $format = "j\-m\-Y") {
	return date($format, strtotime($date));
}

function random_string($len = 16) {
	$scope = "0123456789abcdef";
	$str = "";
	for ($i=0; $i < $len; $i++) { 
		$str.=substr($scope, rand(0, $len - 1), 1);
	}
	return $str;
}

function dashboard_formate($date) {
  $format = "g:i a";
  if(date("Y", time()) != date("Y", strtotime($date))) {
    $format = "j M, Y " . $format;
  } else {
    if(date("d\-m", time()) != date("d\-m", strtotime($date))) {
      $format = "j M " . $format;
    }
  }
  return date($format, strtotime($date));
}

function time_ago_formate($date) {

}

function session_err($title, $msg) {
	$_SESSION['error'] = array('title' => $title, 'text' => $msg);
	return "";
}

function session_msg($title, $msg) {
	$_SESSION['message'] = array('title' => $title, 'text' => $msg);
	return "";
}


function schedule($type = "", $id = "", $lim = '', $time = 'S') {

  // selection changes
  $sel_teacher = "CONCAT(`t`.`Salutation`,' ', `t`.`First_Name`,' ', `t`.`Last_Name`) as Teacher, `ss`.`Student_Rating` as Rating, ";
  $sel_studnet = "CONCAT(`s`.`Salutation`,' ', `s`.`First_Name`,' ', `s`.`Last_Name`) as Student, `ss`.`Teacher_Rating` as Rating, ";
  $sel = $type == 'S' ? $sel_teacher : ( $type == 'T' ? $sel_studnet: $sel_studnet . $sel_teacher);

  // join changes
  $join_t = "LEFT JOIN teachers as t ON `t`.`Id` = `ss`.`Teacher_Id`";
  $join_s = "LEFT JOIN students as s ON `s`.`Id` = `ss`.`Student_Id`";
  $join = $type == 'S' ? $join_t: ($type == 'T' ? $join_s: $join_t . "  " . $join_s);

  // where changes
  $whr_s = " AND `ss`.`Student_Id`='" . $id . "' AND `ss`.`deleted_by_student`='0' ";
  $whr_t = " AND `ss`.`Teacher_Id`='" . $id . "' AND `ss`.`deleted_by_teacher`='0' ";
  $whr_a = " AND `ss`.`deleted_by_admin`='0' ";

  $whr = $type == 'S' ? $whr_s: ($type == 'T' ? $whr_t: $whr_a);
  $whr = ($time == 'S' ? " `ss`.`Schedule_On` > NOW() ": ($time == 'H' ? " `ss`.`Schedule_On` < NOW() ": " 1=1 ")) . $whr;

  $sql_qry="SELECT 
    `ss`.`Id` as Id, 
    `c`.`Course` as Course,
    `l`.`Lesson` as Lesson,
    `ss`.`Course_Id` as C_id,
    " . $sel . "
    `ss`.`Teacher_Id` as T_id,
    `ss`.`Teacher_Reason` as Reason,
    `c`.`mode` AS course_mode,
    DATEDIFF( `ss`.`Schedule_On`, now()) as Days,
    IF(`ss`.`Status` = 0, 'OK', IF(`ss`.`Status` = 1, 'Rejected', IF(`ss`.`Status` = 2, 'Canceled', 'Unexpected'))) as Status,
    `ss`.`Schedule_On` as Schedule, 
    DATE_FORMAT(DATE_ADD(`ss`.`Schedule_On`, INTERVAL ".(get_timezone() - TIMEZONE)." MINUTE), '%d/%m/%Y') AS Schedule_Date, 
    DATE_FORMAT(DATE_ADD(`ss`.`Schedule_On`, INTERVAL ".(get_timezone() - TIMEZONE)." MINUTE), '%H:%i') AS Schedule_Time, 
    CONCAT(`ss`.`Duration`, ' mins') as Duration
  FROM
    students_schedules as ss
  LEFT JOIN
    courses as c
  ON
    `c`.`Id` = `ss`.`Course_Id`
  LEFT JOIN
    lessons as l
  ON
    `l`.`Id` = `ss`.`Lesson_Id`
  " . $join . "
  WHERE " . $whr . "
  ORDER BY Schedule ASC" . ($lim == ''?"":" LIMIT ".$lim);
  
  $req_res = mysql_query($sql_qry) or die(session_err("Database error!", "Can't retrive schedule (" . mysql_error() . ") - " . $sql_qry));
  return $req_res;
}

function teacher_schedule($id) {
  return schedule("T", $id);
}

function student_schedule($id) {
  return schedule("S", $id);
}

function teacher_history($id) {
  return schedule("T", $id, '', "H");
}

function admin_history() {
  return schedule("A", '', '', "H");
}

function student_history($id) {
  return schedule("S", $id, '', "H");
}

function student_history_old($id) {
	$sql_qry="SELECT 
		`ss`.`Id` as Id, 
		`c`.`Course` as Course,
		`ss`.`Course_Id` as C_id,
		CONCAT(`t`.`Salutation`,' ', `t`.`First_Name`,' ', `t`.`Last_Name`) as Teacher,
		`ss`.`Teacher_Id` as T_id,
		`ss`.`Teacher_Reason` as Reason,
		IF(`ss`.`Status` = 0, 'Initial', IF(`ss`.`Status` = 1, 'Accepted', IF(`ss`.`Status` = 2, 'Rejected', 'Unexpected'))) as Status,
		`ss`.`Schedule_On` as Schedule, 
		DATE_FORMAT(ss.Schedule_On, '%d/%m/%Y') AS Schedule_Date, 
		DATE_FORMAT(ss.Schedule_On, '%H:%i') AS Schedule_Time, 
		CONCAT(`ss`.`Duration`, ' mins') as Duration
	FROM
		students_schedules as ss
	LEFT JOIN
		courses as c
	ON
		`c`.`Id` = `ss`.`Course_Id`
	LEFT JOIN
		teachers as t
	ON
		`t`.`Id` = `ss`.`Teacher_Id`
	WHERE 
		`ss`.`Schedule_On` < NOW() AND 
		`ss`.`Student_Id` = " . $id . " 
	ORDER BY Schedule DESC";

	//echo $sql_qry;
	$sql_res = mysql_query($sql_qry) or die(session_err('Database error!', "Can't retrive student history (" . mysql_error() . ")"));
	return $sql_res;	
}

function get_msg_count($type, $id, $status = 'U', $box = 'I') {

  $return=array();
  $qfr = $box == 'I' ? "Reciever_": "Sender_";
  $sql_qry = "SELECT count(*) AS count FROM message WHERE ".$qfr."Type='" . $type . "' AND ".$qfr."Id='" . $id . ($status == 'U' || $status == 'R' ? "' AND Status='" . $status . "';": "';");
  $sql_res = mysql_query($sql_qry) or die(session_err("Database error!", "Cannot count message(s). [" . mysql_error() . "]"));
  if(mysql_num_rows($sql_res)>0) while($sql_row=mysql_fetch_assoc($sql_res)) $return[]=$sql_row;
  return $return[0]['count'];
}

function collect_frm_req($list) {

  $collect = array();
  foreach ($list as $key => $column) {
    $value = $_REQUEST[$column];
    if(trim($value) != "") array_push($collect, " `" . $column . "`='".$value."'");
  }
  return implode(",", $collect);
}

function js2PhpTime($jsdate){
  if(preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches)==1){
    $ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
    //echo $matches[4] ."-". $matches[5] ."-". 0  ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
  }else if(preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches)==1){
    $ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
    //echo 0 ."-". 0 ."-". 0 ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
  } else {
    $ret = strtotime($jsdate);
  }
  return $ret;
}

function php2JsTime($phpDate){
    //echo $phpDate;
    //return "/Date(" . $phpDate*1000 . ")/";
    return date("m/d/Y H:i", $phpDate);
}

function php2MySqlTime($phpDate){
    return date("Y-m-d H:i:s", $phpDate);
}

function mySql2PhpTime($sqlDate){
    $arr = date_parse($sqlDate);
    return mktime($arr["hour"],$arr["minute"],$arr["second"],$arr["month"],$arr["day"],$arr["year"]);

}

function dt_split($str) {
  $arr = explode(" ", $str);
  $sarr = explode(":", $arr[1]);
  $res = array('Date'=>date("Y-m-d", js2PhpTime($str)), 'Hr'=>$sarr[0], 'Min'=>$sarr[1]);
  return $res;
}

function add_teacher_aval($ade, $st, $et, $id) {

  // removing slashes
  $st = stripslashes($st);
  $et = stripslashes($et);

  $sdt = dt_split(saving_date($st));
  if($ade == 'true' && $st == $et) {
    // echo $et;
  } else {
    $edt = dt_split(saving_date($et));
  }

  // echo var_dump($sdt);
  // echo var_dump($edt);

  $start = array('Hr'=>$sdt['Hr'],'Min'=>$sdt['Min']);
  $end = array('Hr'=>$edt['Hr'],'Min'=>$edt['Min']);



  if($sdt['Date'] == $edt['Date']) {

    $date = $sdt['Date'];
    if(isValidTime($start, $end)) {
      calender_insert_for_date($id, $date, $start, $end);
    } else {
      // echo "Please select valid time";
    }

  } else {

    $date = $sdt['Date'];
    $tend = array('Hr'=> 23, 'Min'=> 30);
    if(isValidTime($start, $tend)) {
      calender_insert_for_date($id, $date, $start, $tend);
    } else {
      // echo "Please select valid time";
    }

    $date = $edt['Date'];
    $tstart = array('Hr'=> 0, 'Min'=> 0);
    if(isValidTime($tstart, $end)) {
      calender_insert_for_date($id, $date, $tstart, $end);
    } else {
      // echo "Please select valid time";
    }

  }

}

function remove_qry($id, $date, $sdt, $edt) {
  $sql_qry = "delete from
    `teacher_availability_summary`
  where
    `Teacher_Id` = '".$id."' and
    `Date`       = '".$date."' and
    `Start_Hr`   = '".$sdt['Hr']."' and
    `Start_Min`  = '".$sdt['Min']."' and
    `End_Hr`     = '".$edt['Hr']."' and
    `End_Min`    = '".$edt['Min']."';";
  // echo $sql_qry;
  // exit;
  return $sql_qry;
}

function remove_teacher_aval($id, $tid) {
  $cal = get_record('jqcalendar', 'starttime, endtime', 'id='.$id);
  $sdt = dt_split(saving_date($cal['starttime']));
  $edt = dt_split(saving_date($cal['endtime']));
  if($sdt['Date'] == $edt['Date']) {
    $sql_qry = remove_qry($tid, $sdt['Date'], $sdt, $edt);
    // echo $sql_qry;
    mysql_query($sql_qry) or print("Database Error!, " . mysql_error());
  } else {

    $start = array('Hr'=>$sdt['Hr'],'Min'=>$sdt['Min']);
    $end = array('Hr'=>$edt['Hr'],'Min'=>$edt['Min']);

    $tend = array('Hr'=> 23, 'Min'=> 30);
    if(isValidTime($start, $tend)) {
      $sql_qry = remove_qry($tid, $sdt['Date'], $start, $tend);
      // echo $sql_qry;
      mysql_query($sql_qry) or print("Database Error!, " . mysql_error());
    } else {
      // echo "Please select valid time";
    }

    $tstart = array('Hr'=> 0, 'Min'=> 0);
    if(isValidTime($tstart, $end)) {
      $sql_qry = remove_qry($tid, $edt['Date'], $tstart, $end);
      // echo $sql_qry;
      mysql_query($sql_qry) or print("Database Error!, " . mysql_error());
    } else {
      // echo "Please select valid time";
    }

  }
  // exit;
}


function getAllSlots() {
  $arr = array();
  for ($i = 0; $i < 24; $i++) { 
    for ($j=0; $j < 60; $j+=30) { 
      array_push($arr, array('Hr'=>$i, 'Min'=>$j));
    }
  }
  return $arr;
}

function getListOfSlots($start, $end) {
  $list = getAllSlots();
  $finalList = array();
  $i = array_search($start, $list);
  $l = array_search($end, $list);
  if($i < $l) {
    while ($i < $l) {
      # code...
      array_push($finalList, $list[$i++]);
    }
  }
  return $finalList;
}


function printList($list) {
  foreach ($list as $rec) {
    // echo $rec['Hr'].":".$rec['Min']."<br />";
  }
}

function updateStartTime($time1, $time2) {
  // echo "\nStartTime - (1) ".$time1['Hr'].":".$time1['Min']." or (2) ".$time2['Hr'].":".$time2['Min'];
  $list = getAllSlots();
  $i1 = array_search($time1, $list);
  $i2 = array_search($time2, $list);
  if($i1 < $i2) {
    // echo " -- 1 selected\n";
    return $time1;
  } else {
    // echo " -- 2 selected\n";
    return $time2;
  }
}

function updateEndTime($time1, $time2) {
  $list = getAllSlots();
  $i1 = array_search($time1, $list);
  $i2 = array_search($time2, $list);
  if($i1 > $i2) {
    return $time1;
  } else {
    return $time2;
  }
  
}

function isValidTime($time1, $time2) {
  $list = getAllSlots();
  $i1 = array_search($time1, $list);
  $i2 = array_search($time2, $list);
  return ($i1 < $i2);
}

function replace_old_record($result, $parent, $start, $end) {
  $ret = array('start'=>$start, 'end'=>$end);
  $rec = mysql_fetch_array($result);
  $qry = "SELECT * FROM teacher_availability_summary WHERE Id='".$rec['Summary_Id']."';";
  $result = mysql_query($qry) or die("error||".error_mysql("Selecting summary object."));
  $rec = mysql_fetch_array($result);
  if($parent != $rec['Id']) {
    $ret['start'] = updateStartTime($start, array('Hr'=>$rec['Start_Hr'], 'Min'=>$rec['Start_Min']));
    $ret['end'] = updateEndTime($end, array('Hr'=>$rec['End_Hr'], 'Min'=>$rec['End_Min']));
    $qry = "UPDATE teachers_availabilities SET Summary_Id='".$parent."' WHERE Summary_Id='".$rec['Id']."';";
    mysql_query($qry) or die("error||".error_mysql("Saving detail level teacher availability."));
    $qry = "DELETE FROM teacher_availability_summary WHERE Id='".$rec['Id']."';";
    mysql_query($qry) or die("error||".error_mysql("Saving detail level teacher availability."));
  }
  return $ret;
}


function calender_insert_for_date($teacher, $date, $start, $end) {

  $qry = "INSERT INTO 
    teacher_availability_summary (
        `Teacher_Id`,
        `Date`,
        `Start_Hr`,
        `Start_Min`,
        `End_Hr`,
        `End_Min`)
      VALUES (
        '".$_SESSION['anybody_id']."',
        '".$date."', '".$start['Hr']."',
        '".$start['Min']."',
        '".$end['Hr']."',
        '".$end['Min']."');";
  mysql_query($qry); //or die('');
  $parent = mysql_insert_id();


  $list = getListOfSlots($start, $end);
  foreach ($list as $slot) {
    # code...
    $qry = "SELECT * FROM teachers_availabilities WHERE `Date`='".$date."' AND Teacher_Id='".$_SESSION['anybody_id']."' AND `Hours`='".$slot['Hr']."' AND `Minutes`='".$slot['Min']."';";
    $result = mysql_query($qry) or die("error||".error_mysql("selecting existing record for calende slot."));
    $nos = mysql_num_rows($result);
    $status = "Slot [".$slot['Hr'].":".$slot['Min']."] :: ";
    if($nos == 0) {
      $qry = "INSERT INTO teachers_availabilities (`Summary_Id`, `Date`, Teacher_Id, `Hours`, `Minutes`, `Entry_Date`) VALUES ('".$parent."', '".$date."', '".$_SESSION['anybody_id']."', '".$slot['Hr']."', '".$slot['Min']."', NOW());";
      // echo $qry;
      mysql_query($qry);// or die("error||".error_mysql("Saving detail level teacher availability."));
      $status.="Inserted successfully";
    } else if($nos == 1){
      $tmp = replace_old_record($result, $parent, $start, $end);
      $start = $tmp['start'];
      $end = $tmp['end'];
    } else {
      // echo "Unexpected portion of code.";
    }
    // echo $status.".<br>\n";
  }

  // check for next/previs session joinig
  $allSlots = getAllSlots();
  $i = array_search($start, $allSlots);
  if($i > 0) {
    $slot = $allSlots[$i - 1];
    // echo "HK_DIBUG:: ".var_dump($slot);
    $qry = "SELECT * FROM teachers_availabilities WHERE `Date`='".$date."' AND Teacher_Id='".$teacher."' AND `Hours`='".$slot['Hr']."' AND `Minutes`='".$slot['Min']."';";
    $result = mysql_query($qry);// or die("error||".error_mysql("selecting existing record for calende slot."));
    $nos = mysql_num_rows($result);
    if($nos > 0) {
      $tmp = replace_old_record($result, $parent, $start, $end);
      $start = $tmp['start'];
      $end = $tmp['end'];
    }
  }
  $slot = $end;
  $qry = "SELECT * FROM teachers_availabilities WHERE `Date`='".$date."' AND Teacher_Id='".$teacher."' AND `Hours`='".$slot['Hr']."' AND `Minutes`='".$slot['Min']."';";
  $result = mysql_query($qry);// or die("error||".error_mysql("selecting existing record for calende slot."));
  $nos = mysql_num_rows($result);
  if($nos > 0) {
    $tmp = replace_old_record($result, $parent, $start, $end);
    $start = $tmp['start'];
    $end = $tmp['end'];
  }

  $qry = "UPDATE teacher_availability_summary SET `Start_Hr`='".$start['Hr']."', `Start_Min`='".$start['Min']."', `End_Hr`='".$end['Hr']."', `End_Min`='".$end['Min']."' WHERE Id='".$parent."';";
  mysql_query($qry);// or die("error||".error_mysql("Saving upper level teacher availability."));
}


function cancel_schedule($id, $type = 2) {
  $q1 = "UPDATE students_schedules SET Status=".$type." WHERE Id=".$id;
  mysql_query($q1) or die(session_err('Database error!',"Unable to cancel the schedule (".mysql_error().")"));
  $schedule = get_record('students_schedules', 'Student_Id,Lesson_Id',"Id='".$id."'");
  $lesson = get_record('lessons', 'amount', 'Id='.$schedule['Lesson_Id']);
  $student = get_record('students', 'Credits','Id='.$schedule['Student_Id']);
  $q2 = "UPDATE students SET Credits=".($student['Credits'] + $lesson['amount'])." WHERE Id=".$schedule['Student_Id'];
  mysql_query($q2);
}

function cnt_frm_tmplt($template, $vars) {
  $path = TMPLT . $template;
  $cnt = file_get_contents($path);
  foreach ($vars as $key => $value)
  $cnt = implode(isset($value)?$value:"", explode("{{".$key."}}", $cnt));
  return $cnt;
}

function send_mail($to, $sub, $tmplt, $data = array(), $atl_body = "Unable to load email from Al-huda online course") {
  $cnt = cnt_frm_tmplt($tmplt, $data);
  require 'class.smtp.php';
  require 'PHPMailer.php';
  $mail = new PHPMailer;
  $mail->isSMTP();
  $mail->SMTPDebug = 2;
  $mail->Debugoutput = 'html';
  $mail->Host = "smtp.gmail.com";
  $mail->Port = 587;
  $mail->SMTPSecure = "tls";
  $mail->SMTPAuth = true;
  $mail->Username = SMTP_EMAIL;
  $mail->Password = SMTP_PSWD;
  $mail->setFrom(ADM_EMAIL, ADM_NAME);
  $mail->addReplyTo(ADM_EMAIL, ADM_NAME);
  $mail->addAddress($to);
  $mail->Subject = $sub;
  $mail->msgHTML($cnt);
  $mail->AltBody = $atl_body;
  $mail->send();
}

function smtp_mail($to, $sub, $message) {

require 'class.smtp.php';
require 'PHPMailer.php';

$mail = new PHPMailer;

$mail->isSMTP();

//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = "mail.alhudaflexible.com";
$mail->Port = 25;
$mail->SMTPSecure = "tls";
$mail->SMTPAuth = true;
$mail->Username = "info@alhudaflexible.com";
$mail->Password = "AlHuda@001";
$mail->setFrom('info@alhudaflexible.com', 'Alhuda Flexible');
$mail->addReplyTo('info@alhudaflexible.com', 'Alhuda Flexible');
$mail->addAddress($to);
$mail->Subject = $sub;
$mail->msgHTML($message);
$mail->AltBody = 'Mail from Alhuda, Message could not display as html';

return $mail->send();
}


function get_payments($id = null) {
  $sql_qry = "SELECT 
    CONCAT(`s`.`First_Name`,' ', `s`.`Last_Name`) AS  Student,
    `p`.`Type` AS Type,
    `p`.`Amount` AS Amount,
    `p`.`Discount` AS Discount,
    `p`.`Payment_Status` AS Payment_Status,
    `p`.`Payment_Through` AS Payment_Through,
    `p`.`Entry_Date` AS Entry_Date,
    `c`.`Course` AS Course,
    `c`.`Lesson_Charge` AS Lesson_Charge,
    `c`.`Lesson` AS Lesson
  FROM `payment` AS p
  LEFT JOIN `students` AS s
  ON `s`.`Id`=`p`.`Sid`
  LEFT JOIN (
    SELECT 
      `css`.`Id` AS Id,
      `cc`.`Course` AS Course,
      `cl`.`Lesson` AS Lesson,
      `cl`.`amount` AS Lesson_Charge
    FROM `students_schedules` AS css
    LEFT JOIN `courses` AS cc
    ON `cc`.`Id` = `css`.`Course_Id`
    LEFT JOIN `lessons` AS cl
    ON `cl`.`Id`=`css`.`Lesson_Id`
    ) AS c
  ON `c`.`Id`=`p`.`Schedule_Id`"
  .(isset($id) ?"WHERE `p`.`Sid`='".$_SESSION["student_uniqueid"]."'":"").
  "ORDER BY `p`.`Entry_Date` DESC;";
  $payments = mysql_query($sql_qry) or die(session_err('Database error!', "unable to retrive payments (orders) -- " . mysql_error()));
  return $payments;
}

function getResToArr($post_response) {
  $arr = explode("&", $post_response);
  $json_arr = array();
  foreach ($arr as $str) {
    $t = explode("=", $str);
    $json_arr[$t[0]] = urldecode($t[1]);
  }
  return $json_arr;
}

function retrive_date($date) {
  // $tz = 
  return $date;
}

function saving_date($date) {
  $tz = get_timezone();
  $diff = $tz * 60;
  $t  = strtotime($date);
  $d  = date('Y-m-d H:i:s', $t - $diff);
  return $d;
}

function get_timezone() {
  return intval($_SESSION["timezone"]);
}

function generate_regCode($sid, $code) {
  $sid = "" . $sid;
  for($i = strlen($sid); $i < 5; $i++) $sid = "0" . $sid;
  return "AF/".$sid."/".$code;
}



function collect_session_from_cookies() {

  foreach ($_COOKIE as $key => $value) {
    # code...
    $_SESSION[$key] = $value;
  }
}

function set_cookies_as_session() {

  foreach ($_SESSION as $key => $value) {
    # code...
    setcookie($key, $value, time() + (86400 * 30), "/"); // 86400 = 1 day
  }

}

?>
