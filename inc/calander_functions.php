<?php

function getAllSlots() {
	$arr = array();
	for ($i = 0; $i < 24; $i++) { 
		for ($j=0; $j < 60; $j+=15) { 
			array_push($arr, array('Hr'=>$i, 'Min'=>$j));
		}
	}
	return $arr;
}

 function getListOfSlots($start, $end) {
 	$list = getAllSlots();
 	$finalList = array();
 	$i = array_search($start, $list);
 	$l = array_search($end, $list);
 	if($i < $l) {
 		while ($i < $l) {
 			# code...
 			array_push($finalList, $list[$i++]);
 		}
 	}
 	return $finalList;
 }

function printList($list) {
	foreach ($list as $rec) {
		// echo $rec['Hr'].":".$rec['Min']."<br />";
	}
}

function updateStartTime($time1, $time2) {
	echo "\nStartTime - (1) ".$time1['Hr'].":".$time1['Min']." or (2) ".$time2['Hr'].":".$time2['Min'];
	$list = getAllSlots();
	$i1 = array_search($time1, $list);
	$i2 = array_search($time2, $list);
	if($i1 < $i2) {
		echo " -- 1 selected\n";
		return $time1;
	} else {
		echo " -- 2 selected\n";
		return $time2;
	}
}

function updateEndTime($time1, $time2) {
	$list = getAllSlots();
	$i1 = array_search($time1, $list);
	$i2 = array_search($time2, $list);
	if($i1 > $i2) {
		return $time1;
	} else {
		return $time2;
	}
	
}

function isValidTime($time1, $time2) {
	$list = getAllSlots();
	$i1 = array_search($time1, $list);
	$i2 = array_search($time2, $list);
	return ($i1 < $i2);
}


$a = array('Hr'=>2, 'Min'=>30);
$b = array('Hr'=>5, 'Min'=>0);
$l = getListOfSlots($a, $b);
printList($l);


?>