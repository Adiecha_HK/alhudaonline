<div class="row-fluid">
	<div class="alert offset1 span10">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong>Error!</strong> <?php if(sizeof($errors) > 1) { ?>
		<p><?php foreach($errors as $error){ ?><li><?php echo $error; ?></li><?php } ?></p>
		<?php } else { foreach($errors as $error){ echo $error; } } ?>
	</div>
</div>