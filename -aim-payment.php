<?php
include './inc/config.php';
include './inc/functions.php';

if(isset($_SESSION['payment_id'])) {
	$sql_qry = "UPDATE `payment` SET `Payment_Through`='A' WHERE Id='".$_SESSION['payment_id']."'";
	mysql_query($sql_qry);
}

$_SESSION['amount'] = "0.1";

$post_url = AUTH_URL;

$post_values = array(
	
	"x_login"						=> AUTH_LOGIN,
	"x_tran_key"				=> AUTH_TKEY,
	"x_version"					=> "3.1",
	"x_delim_data"			=> "TRUE",
	"x_delim_char"			=> "|",
	"x_relay_response"	=> "FALSE",
	"x_type"						=> "AUTH_CAPTURE",
	"x_method"					=> "CC",
	"x_card_num"				=> $_POST['CCNumber'],
	"x_exp_date"				=> $_POST['Expiery'],
	"x_amount"					=> $_SESSION['amount'],
	"x_description"			=> "Sample Transaction",

	"x_first_name"		=> "Girish",
	"x_last_name"		=> "Kargathara",
	"x_address"			=> "Rajkot",
	"x_phone"					=> "9662025100",
	"x_state"				=> "GJ",
	"x_zip"					=> "360001"
);

$post_string = "";
foreach( $post_values as $key => $value )
	{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
$post_string = rtrim( $post_string, "& " );

$request = curl_init($post_url);
	curl_setopt($request, CURLOPT_HEADER, 0);
	curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
	curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
	$post_response = curl_exec($request);
curl_close ($request);

$_SESSION['payment'] = array('delimiter' => $post_values["x_delim_char"], "responce" => $post_response, "Payment_Through"=>'A');

$response_json = explode($post_values["x_delim_char"], $post_response);

if($response_json[0]==1) {

	if(isset($_SESSION['payment_id'])) {
		$sql_qry = "UPDATE `payment` SET `Payment_Status`='S', `Json`='".escape_string(json_encode(array('response' => $response_json)), '', 'db')."' WHERE `Id`=".$_SESSION['payment_id'].";";
		mysql_query($sql_qry) or die(session_err('Database error:', mysql_error()));    
		$pymnt = get_record('`payment`', '`Type`', '`Id`='.$_SESSION['payment_id']);
		if($pymnt['Type'] == 'R') {
			$sql_qry = "UPDATE `students` SET `Reg_Status`='S' WHERE `Id`='".$_SESSION['uid']."';";
			mysql_query($sql_qry) or die(session_err('Database error:', mysql_error()));
		}
	}
} else {
	echo "<br/> Res: Fail";
	if(isset($_SESSION['payment_id'])) {
		$sql_qry = "UPDATE `payment` SET `Payment_Status`='F', `Json`='".escape_string(json_encode(array('response' => $response_json)), '', 'db')."', `Remark`='".escape_string($response_json[3])."' WHERE `Id`=".$_SESSION['payment_id'].";";
		mysql_query($sql_qry) or die(session_err('Database error:', mysql_error()));    
		$pymnt = get_record('`payment`', '`Type`', '`Id`='.$_SESSION['payment_id']);
		if($pymnt['Type'] == 'R') {
			$sql_qry = "UPDATE `students` SET `Reg_Status`='U' WHERE `Id`='".$_SESSION['uid']."';";
			mysql_query($sql_qry) or die(session_err('Database error:', mysql_error()));
		}
	}
}

header("location: ".$_SESSION['redirect_to']);
?>

<script type="text/javascript">window.location = "<?=$_SESSION['redirect_to']?>"</script>

<?php exit; ?>