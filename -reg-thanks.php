<?php
include './inc/config.php';
include './inc/functions.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>index.css" />
    <link href='http://fonts.googleapis.com/css?family=Dosis:200,400' rel='stylesheet' type='text/css'>
    <!-- <META http-equiv="refresh" content="5;URL=<?=URL?>">  -->
    <style type="text/css">
      body {
        background-image: url('<?=IMG."heroshot.jpg"?>');
      }
      .cg-form-container legend h3{
        color: white;
        margin-left: 20px;
      }

      .cg-form-container a.link {
        color: cyan;
      }
      .cg-form-container {
        background-color: rgba(0,0,0,0.9);
        border: 10px solid rgba(255,255,255,0.5);
        border-radius: 15px;
        margin-top: 20%;
      }

      .cg-form-container input {
        max-width: 90%;
      }

    </style>
    <script language="javascript" src="<?php echo JS; ?>html5.js"></script>
    <script language="javascript" src="<?php echo JS; ?>jquery-1.8.0.min.js"></script>
    <script language="javascript" src="<?php echo JS; ?>bootstrap.js"></script>
    <script language="javascript" src="<?php echo JS; ?>index.js"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Registration</title>
  </head>

  <body class="homepage">


    <div class="container">
      <div class="reg-form-holder">
        <div class="row-fluid">
          <div class="span10 offset1">
            <br/>
            <br/>
            <?php if(isset($_SESSION['error'])) { ?>
              <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4><?=(isset($_SESSION['error']['title'])?$_SESSION['error']['title']:"Error!")?></h4>
                <?=$_SESSION['error']['text']?>
              </div>
            <?php unset($_SESSION['error']); } ?>
            <?php 
            if(isset($_SESSION['payment']['Payment_Through']) && $_SESSION['payment']['Payment_Through'] == 'A') {

              $responce = explode($_SESSION['payment']['delimiter'], $_SESSION['payment']['responce']);
              if($responce[0] == 1) { 
                mysql_query("UPDATE payment SET
                  `Transection`='".$responce[6]."',
                  `Payment_Status`='S' WHERE `Id`=".$_SESSION['payment_id']);
                $sql_qry = "UPDATE students SET Reg_Status='S' WHERE Id='".$_SESSION['uid']."';";
                mysql_query($sql_qry) or die(session_err('Database error:', mysql_error()));    
              ?>
              <h1>Success!</h1>
              <p><?=$responce[3]?></p>
              <a href="login.php" class="btn">Go to Login</a>
              <?php } else { 
                mysql_query("UPDATE payment SET
                  `Payment_Status`='F' WHERE `Id`=".$_SESSION['payment_id']);
                $sql_qry = "UPDATE students SET Reg_Status='U' WHERE Id='".$_SESSION['uid']."';";
                mysql_query($sql_qry) or die(session_err('Database error:', mysql_error()));    
                ?>
              <h1>Error!</h1>
              <p><?=$responce[3]?></p>
              <a href="register-payment.php" class="btn">Re-fill Details</a>
              <?php }
              unset($_SESSION['payment']);
            } else {
              if($_REQUEST['act'] == 'P') {
                echo "It's still pending";
              } else if($_REQUEST['act'] == 'S') {
                echo "Payment done successfully";
              } else if($_REQUEST['act'] == 'F') {
                echo "Payment Failed";
              }
            } ?>
            <br/>
            <br/>

            <a href="<?=URL?>">click here</a> to go to home page.
          </div>
        </div>
      </div>
    </div>
  </body>
</html>