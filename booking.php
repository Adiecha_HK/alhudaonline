<?php 

$page_nav="Book Lession";
$page_title="Book Lession";
$page_script=array("jquery-ui.min.js", "book_lession.js");
$page_style = array("jquery-ui-hk.css");//, "jquery-ui.theme.min.css");
include 'header.php'; 

$date = $_POST['date'];
$hr = $_POST['hr'];
$min = $_POST['min'];
$course = $_POST['course'];
$lesson = $_POST['lesson'];
$teacher = $_POST['teacher'];

$sid = $_SESSION['student_uniqueid'];
$student = get_record('students', '', 'Id='.$sid);

// echo "<pre>";
// echo "REQUEST::\n";
// echo var_dump($_REQUEST);
// echo "SESSION::\n";
// echo var_dump($_SESSION);
// echo "SERVER::\n";
// echo var_dump($_SERVER);
// echo "GET::\n";
// echo var_dump($_GET);
// echo "POST::\n";
// echo var_dump($_POST);
// exit;


if(isset($lesson) && isset($course)) {

	$_SESSION['date'] = $date;
	$_SESSION['hr'] = $hr;
	$_SESSION['min'] = $min;
	$_SESSION['course'] = $course;
	$_SESSION['lesson'] = $lesson;
	$_SESSION['teacher'] = $teacher;

	if(trim($lesson) == '') {
		session_err('Validation error!', "Lesson can't be null.");
		header("location: ".$_SERVER['PHP_SELF']);
		?><script type="text/javascript">window.location = "<?=$_SERVER['PHP_SELF']?>"</script><?php
		exit;
	}
	$crs = get_record('courses', 'mode', 'Id='.$course);
	if($crs['mode'] == 'T' && trim($teacher) == '') {
		session_err('Validation error!', "Teacher can't be null.");
		header("location: ".$_SERVER['PHP_SELF']);
		?><script type="text/javascript">window.location = "<?=$_SERVER['PHP_SELF']?>"</script><?php
		exit;
	}
	$gmt_date = saving_date($date . ' ' . $hr . ':' . $min.':0');
	$sql_qry = "INSERT INTO students_schedules SET `Student_Id`='".$sid."', `Course_Id`='".$course."', `Lesson_Id`='".$lesson."', ".($crs['mode'] == 'T' ? "`Teacher_Id`='".$teacher."',":"")." `Schedule_On`='".$gmt_date."', `Teachers_Aval`='".$_REQUEST['aval_id']."', `Duration`='30', `Entry_Date`=NOW(), `Status`=0, `Is_Schedule_Expired`='N'";
	mysql_query($sql_qry) or die(session_err('Database error!', "Unable to save student schedule."));
	$schedule_id = mysql_insert_id();
	if(isset($_REQUEST['aval_id'])) {
		$sql_qry = "UPDATE `teachers_availabilities` SET `Is_Scheduled`='Y' WHERE Id='".$_REQUEST['aval_id']."'";
		mysql_query($sql_qry) or die(session_err('Database error!', "Unable to lock teachers availability. (" . mysql_error() . ")"));
	}
	$_SESSION['schedule_id'] = $schedule_id;
	if(!isset($_SESSION['submition_message'])) {
		$_SESSION['submition_message'] = "Added successfully";
	}
	$amount = $_POST['amount'];
	$sql_qry = "INSERT INTO payment SET `Sid`='".$sid."', `Type`='B', `Amount`='".$amount."', `Entry_Date`=NOW(), Schedule_Id='".$schedule_id."', Payment_Status='P', `Discount`='".$_REQUEST['discount']."'";
	mysql_query($sql_qry) or die(session_err('Database error!', "While inserting record in payment."));
	$payment_id = mysql_insert_id();
	if($amount > 0) {

		$_SESSION['payment_id'] = $payment_id;
		$_SESSION['uid'] = $sid;
		$_SESSION['amount'] = $amount;
		$_SESSION['Discription'] = "Lesson Booked with amount of " . $amount . " USD";
		$_SESSION['redirect_to'] = "booking-thanks.php";

		$sql_qry = "UPDATE students SET Credits='".$_POST['Credit']."' WHERE Id='".$sid."'";
		mysql_query($sql_qry) or die(session_err('Database error!', "Unable deduct credit."));
		// header('location: book_lession.php');
		// echo "<pre>".var_dump($_REQUEST);
		// echo "<pre>".var_dump($_SESSION);
		header('location: payment-form.php');
		?><script type="text/javascript"> window.location = "payment-form.php"</script><?php
		exit;
	} else {
		$sql_qry = "UPDATE students SET Credits='".$_POST['Credit']."' WHERE Id='".$sid."'";
		mysql_query($sql_qry) or die(session_err('Database error!', "Unable deduct credit."));
		$sql_qry = "UPDATE payment SET `Payment_Status`='S', `Payment_Through`='S' WHERE `Id`='".$payment_id."'";
		mysql_query($sql_qry) or die(session_err('Database error!', "While inserting record in payment."));

		session_msg('Successful!', "Lesson booked successfully.");
		header("location: schedules.php");
		?><script type="text/javascript">window.location = "schedules.php"</script><?php
		exit;
	}
	exit;
} else {

	$date = null;
	if(isset($_SESSION['date'])) {
		$date = $_SESSION['date'];
		unset($_SESSION['date']);
	}
	$hr = null;
	if(isset($_SESSION['hr'])) {
		$hr = $_SESSION['hr'];
		unset($_SESSION['hr']);
	}
	$min = null;
	if(isset($_SESSION['min'])) {
		$min = $_SESSION['min'];
		unset($_SESSION['min']);
	}
	$course = null;
	if(isset($_SESSION['course'])) {
		$course = $_SESSION['course'];
		unset($_SESSION['course']);
	}
	$lesson = null;
	if(isset($_SESSION['lesson'])) {
		$lesson = $_SESSION['lesson'];
		unset($_SESSION['lesson']);
	}
	$teacher = null;
	if(isset($_SESSION['teacher'])) {
		$teacher = $_SESSION['teacher'];
		unset($_SESSION['teacher']);
	}


}
?>
<?="<span id='ajax-url' url='".PROJECT."ajx/booking-ajax.php'></span>"?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL.'dashboard.php'?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Book lesson</li>
    </ul>
</div>
<?php if($student['Credits'] > 0) { ?> 
<div class="row-fluid">
	<div class="span12">
		<div class="pull-right" id="credit_txt">
			Credit: <strong><?=$student['Credits']?></strong>
		</div>
			<br/>
		<div class="pull-right">
			<label><input id="useCr" type="checkbox" style="margin-top: -2px" /> Use Credits</label>
		</div>
	</div>
</div>
<br/>
<?php } ?>
<center>
	<?php if (isset($_SESSION['submition_message'])) { 
		echo "<div><p>" . $_SESSION['submition_message'] . "</p></div>";
		unset($_SESSION['submition_message']);
	} ?>
	<div class="Dashboard row hide" style="background-color: rgba(0,0,0,0.3); border-radius: 15px; margin-top: 20px;margin-bottom: 20px;">
		<div class="DashboardLeft span6">
			<div class="DashboardHeading"><strong>Teachers for <span id="selectedSubject"></span> on <span class="selectedDate"></span></strong></div>
			<div class="Clear"></div>
			<div class="DashboardContent" id="subjectTeacherList">
			</div>
		</div>
		<div class="DashboardRight span6">
			<div class="DashboardHeading"><strong><span id="selectedTeacher"></span>'s availability for <span class="selectedDate"></span></strong></div>
			<div class="Clear"></div>
			<div class="DashboardContent" id="availabilityList">
			</div>
		</div>
	</div>
	<form method="POST" id="booking-form">
		<table class="DataTable table">
			<thead>
				<tr class="DataTableHeading">
					<th>Course</th>
					<th>Lesson</th>
					<th class="tutor-fields teacher-col">Teacher</th>
					<th class="tutor-fields">Date</th>
					<th class="tutor-fields">Time</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<select  id="courses" class="span2" name="course">
							<option value="0">-- Select Courses --</option>
							<?php
								$sql_qry="SELECT Id, Course FROM courses where Id in (SELECT Course_Id FROM students_courses WHERE Student_Id = '".$_SESSION['student_uniqueid']."')";
								$sql_res=mysql_query($sql_qry) or die(error_mysql("Cannot match username"));
								$cid = NULL;
								if(mysql_num_rows($sql_res)>0){
									while($sql_row=mysql_fetch_array($sql_res)){
										if(!isset($cid)) $cid = $sql_row['Id'];
										echo "<option value='".$sql_row['Id']."' " . (isset($course) && $sql_row['Id'] == $course ? "selected": "") . ">".$sql_row['Course']."</option>";
									}
								}
								?>
							?>		
						</select>
					</td>
					<td>
						<select id="lessons" class="span2" name="lesson" disabled="disabled">
							<option value="0">-- Select Lesson --</option>
						</select>
					</td>
					<td class="tutor-fields teacher-col">
						<select id="teachers" class="span2" name="teacher" disabled="disabled">
							<option value="0">-- Select Teacher --</option>
						</select>
					</td>
					<td class="tutor-fields"><input disabled="disabled" id="date" class="span2" type="text" name="date" <?=isset($date)?"value='$date'":"value='-- Select Date --'"?> /></td>
					<td class="tutor-fields">
						<select class="span1" name="hr" disabled="disabled">
						<option>-- Hour --</option>
						</select>
						:
						<select  class="span1" name="min" disabled="disabled">
							<option>-- Min --</option>
						</select>
					</td>
					<td>
					<input type="hidden" name="aval_id" id="aval_id" value="" />
					<input type="hidden" name="amount" id="amount" value="<?=$amount?>" />
					<input type="hidden" name="Credit" id="credit" value="<?=$student['Credits']?>" />
					<input type="hidden" name="discount" id="discount" value="0">
					<span id="amount_txt"><?=isset($amount) ? $amount: "0"?></span> USD</td>
				</tr>
				<?php if($student['Credits'] > 0) { ?> 
				<tr>
					<td colspan="1"></td>
					<td class="tutor-fields" colspan="3"></td>
					<td>Used Credits</td>
					<td id="credit_used_txt">0</td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="1"></td>
					<td class="tutor-fields" colspan="3"></td>
					<th>Final payable amount</th>
					<th id="payble_amount">0</th>
				</tr>
			</tbody>
		</table>
		<button class="btn btn-success pull-right" type="submit">Schedule</button>
		<img src="<?=IMG.'payment.png'?>" /><br /><br /><br />

	</form>
</center>
<?php 
include 'footer.php'; 
?>


<!-- 
Hetalbhai (Mintu didi)
business 9327726407
private  9033336052
 -->