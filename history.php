<?php 
$page_nav="History";
$page_title="History";
include 'header.php'; 

$sql_res=student_history($_SESSION['student_uniqueid']);
$sql_nos=mysql_num_rows($sql_res);
$feedbacks = array("Poor", "Average", "Good", "Great", "Excelent");

if($_SERVER['REQUEST_METHOD'] == "POST") {
  $action = $_REQUEST['action'];
  if($action == "delete") {
    $sql_qry = "UPDATE students_schedules SET deleted_by_student='1' WHERE Id in ('".implode("','",$_REQUEST['history'])."')";
    mysql_query($sql_qry) or die(session_err("Database error!", "Unable to delete (" . mysql_error() . ")"));
  } else {
    $sql_qry = "UPDATE students_schedules SET Student_Rating='".$_REQUEST['Rate']."', Student_Feedback='".escape_string($_REQUEST['Feedback'], "", "db")."' WHERE Id='".$_REQUEST['Id']."'";
    mysql_query($sql_qry) or die(session_err("Database error!", "Unable to rate (" . mysql_error() . ")"));
    if(!isset($_SESSION['error'])) session_msg("Thank you!", "Thanks for giving feedback.");
  }
  header("location: ".$_SERVER['PHP_SELF']);
  echo '<script type="text/javascript"> window.location = "'.$_SERVER['PHP_SELF'].'"; </script>';
}

?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL.'dashboard.php'?>">Home</a> <span class="divider">/</span></li>
      <li class="active">History</li>
    </ul>
</div>
<form method="POST">
<input name="action" value="delete" type="hidden" />
<table class="DataTable table">
  <tr class="DataTableHeading">
    <th width="5%" align="left"><?php if($sql_nos>0) { ?><input type="checkbox" id="toggleAll"><?php } ?></th>
    <th width="6%" align="left">Date</th>
    <th width="5%" align="center">Time</th>
    <th width="14%" align="left">Course</th>
    <th width="14%" align="left">Lesson</th>
    <th width="14%" align="left">Teacher</th>
    <th width="14%" align="center">Duration</th>
    <th width="14%" align="center">Status</th>
    <th width="14%" align="center">Feedback</th>
  </tr> 
<?php
$iDate="";
if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
  if($sql_row["course_mode"] == 'A') {
?>
    <tr class="DataTableRow">
      <td align="left"><input type="checkbox" class="historyRecord" name="history[]" value="<?=$sql_row['Id']?>" /></td>
      <td align="left"><?php echo "- N/A -"; ?></td>
      <td align="center"><?php echo "- N/A -"; ?></td>
      <td align="left"><?php echo $sql_row["Course"]; ?></td>
      <td align="left"></td>
      <td align="left"><?php echo $sql_row["Teacher"]; ?></td>
      <td align="center"><?php echo $sql_row["Duration"]; ?></td>
      <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
      <td align="center">
        <?php if(isset($sql_row["Rating"]) && $sql_row["Rating"] > 0) { ?>
          <?=$feedbacks[$sql_row['Rating'] - 1]?>
        <?php } else { ?>
          <a class="btn btn-info" href="#myModal" role="button" data-toggle="modal" data-id="<?=$sql_row['Id']?>">Feedback</a>&nbsp;
        <?php } ?>
      </td>
    </tr>
<?php
  } else {
    if($iDate!=$sql_row["Schedule_Date"])
  {
?>
  <tr class="DataTableRow">
    <td align="left"></td>
    <td align="left"><?php echo $sql_row["Schedule_Date"]; ?></td>
    <td align="center"></td>
    <td align="left"></td>
    <td align="left"></td>
    <td align="left"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
  </tr>
<?php
    $iDate=$sql_row["Schedule_Date"];   
  }
?>
  <tr class="DataTableRow">
    <td align="left"><input type="checkbox" class="historyRecord" name="history[]" value="<?=$sql_row['Id']?>" /></td>
    <td align="left"></td>
    <td align="center"><?php echo $sql_row["Schedule_Time"]; ?></td>
    <td align="left"><?php echo $sql_row["Course"]; ?></td>
    <td align="left"></td>
    <td align="left"><?php echo $sql_row["Teacher"]; ?></td>
    <td align="center"><?php echo $sql_row["Duration"]; ?></td>
    <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
    <td align="center">
      <?php if(isset($sql_row["Rating"]) && $sql_row["Rating"] > 0) { ?>
        <?=$feedbacks[$sql_row['Rating'] - 1]?>
      <?php } else { ?>
        <a class="btn btn-info" href="#myModal" role="button" data-toggle="modal" data-id="<?=$sql_row['Id']?>">Feedback</a>&nbsp;
      <?php } ?>
    </td>
  </tr>
<?php
}}}else{
?>
  <tr>
    <td colspan="9" align="center" style="text-align: center">There is no Scheduled lession found.</td>
  </tr>
<?php 
}
?>
</table>

<?php if($sql_nos>0) { ?><input class="btn btn-danger" type="submit" value="Delete selected records" /><?php } ?>

</form>
<form method="POST">
  <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="myModalLabel">Feedback</h3>
    </div>
    <input name="Id" value="0" type="hidden" />    
    <input name="Rate" value="0" type="hidden" />    
    <div class="modal-body">
      <label>Rate (<span class="rate-txt">None</span>)</label>
      <div id="rate"></div><br/>
      <label for="feedback">Feedback</label>
      <textarea name="Feedback"></textarea>
    </div>
    <div class="modal-footer">
      <button class="btn btn-primary" type="submit">Save</button>
      <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
  </div>
</form>
<script type="text/javascript">
  $("#toggleAll").checkAll(".historyRecord");

  $("#rate").pulgRating({
    'text': "#myModal .rate-txt",
    'filled': "icon-star",
    'empty': "icon-star-empty",
    'html': "<i class='icon-star-empty'></i>",
    'target-input': "input[name=Rate]",
    'rating': [ "Poor", "Average", "Good", "Great", "Excelent" ]
  });
  $("[data-id]").on('click', function() {
    $("[name=Id]", "#myModal").val($(this).data("id"));
  });
</script>
<?php 
include 'footer.php'; 
?>















