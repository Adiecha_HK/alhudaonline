<?php 
include './inc/config.php';
include './inc/functions.php';
if(!isset($_SESSION["anybody_loggedin"]) || $_SESSION["anybody_loggedin"]!="student"){ 
	header("Location: login.php"); exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title><?php echo escape_string($page_title,"display"); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>select2.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>select2-bootstrap.css" />
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>jquery-ui-hk.css" /> -->
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>index.css" />
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>style.css" /> -->
    <script language="javascript" src="<?php echo JS; ?>html5.js"></script>
    <script language="javascript" src="<?php echo JS; ?>jquery-1.8.0.min.js"></script>
    <script language="javascript" src="<?php echo JS; ?>bootstrap.min.js"></script>
    <script language="javascript" src="<?php echo JS; ?>main.js"></script>
    <script type="text/javascript" src="<?=JS.'select2.min.js'?>"></script>
    <?php if(isset($page_script)) { foreach ($page_script as $script) { ?>
    <script language="javascript" src="<?php echo JS.$script; ?>"></script>
    <?php } } ?>
    <?php if(isset($page_style)) { foreach ($page_style as $styles) { ?>
        <link rel="stylesheet" href="<?=CSS.$styles?>">
    <?php } } ?>
  </head>
  <body>
  <div id="before-header">
    <div class="container">
      <div class="row">
        <div class="span12 clearfix">
          <div class="header-left pull-left max-width">
            <div class="site-logo">
              <a href="http://www.alhudaflexible.com">
                <img src="http://www.alhudaflexible.com/wp-content/uploads/2014/12/Alhuda-Online-Logo1.png" alt="alhuda Online">
              </a>
            </div>
          </div><!-- /.header-left -->
          <div class="header-right pull-right max-width">
            <div id="menu-top" class="pull-left">
              <ul id="nav-menu" class="breadcrumb nav-menu menu">
                <li id="menu-item-597" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-550 current_page_item menu-item-597"><a href="http://www.alhudaflexible.com/">Home</a></li>
                <li id="menu-item-535" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-535"><a href="#">|</a></li>
                <li id="menu-item-599" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-599"><a href="http://www.alhudaflexible.com/news/">News &amp; Event</a></li>
                <li id="menu-item-536" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-536"><a href="#">|</a></li>
                <li id="menu-item-627" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-627"><a href="http://www.alhudaflexible.com/faq/">FAQs</a></li>
                <li id="menu-item-545" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-545"><a href="#">|</a></li>
                <li id="menu-item-598" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-598"><a href="http://www.alhudaflexible.com/contact/">Contact</a></li>
              </ul>
            </div>                                
            <div id="header-phone" class="hidden-sm hidden-xs">
              <strong>+1 (817) 285 9450 </strong>Admissions department
            </div>
          </div> <!-- /.header-right -->
        </div>
      </div>
    </div>
  </div>
  <div id="menu-wrapper" class="main-nav-outer-wrapper">
    <div class="container">
      <div class="navbar">
        <?php $page = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], "/")+1);?>
        <!-- <a class="brand" href="#">Online Course </a> -->
        <ul class="nav">
          <li <?=($page=="dashboard.php"?"class='active'":"")?>><a href="dashboard.php">Home</a></li>
          <li <?=($page=="courses.php"?"class='active'":"")?>><a href="courses.php">Courses</a></li>
          <li <?=($page=="schedules.php"?"class='active'":"")?>><a href="schedules.php">Schedule</a></li>
          <li <?=($page=="history.php"?"class='active'":"")?>><a href="history.php">History</a></li>
          <li <?=($page=="booking.php"?"class='active'":"")?>><a href="booking.php">Book Lesson</a></li>
          <li <?=($page=="messages.php"?"class='active'":"")?>><a href="messages.php">Messages
            <?php $msgs = get_msg_count('S', $_SESSION["student_uniqueid"]); if($msgs > 0) { ?>
            <span class="badge badge-inverse"><?=$msgs?></span>
            <?php } ?>
          </a></li>
          <li <?=($page=="payment.php"?"class='active'":"")?>><a href="payment.php">Payments</a></li>
        </ul>
        <ul class="nav pull-right">
          <li <?=($page=="settings.php"?"class='active'":"")?>><a href="settings.php">Settings</a></li>
          <li><a href="logout.php">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div id="page-container" class="container">
  <br/>
  <?php if(isset($_SESSION['error'])) { ?>
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4><?=(isset($_SESSION['error']['title'])?$_SESSION['error']['title']:"Error!")?></h4>
        <?=$_SESSION['error']['text']?>
      </div>
  <?php unset($_SESSION['error']); } ?>
  <?php if(isset($_SESSION['message'])) { ?>
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4><?=(isset($_SESSION['message']['title'])?$_SESSION['message']['title']:"Message!")?></h4>
        <?=$_SESSION['message']['text']?>
      </div>
  <?php unset($_SESSION['message']); } ?>