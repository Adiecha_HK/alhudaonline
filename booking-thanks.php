<?php
$page_nav="Payment";
$page_title="Payment";
include 'header.php'; 

  unset($_SESSION['date']);
  unset($_SESSION['hr']);
  unset($_SESSION['min']);
  unset($_SESSION['course']);
  unset($_SESSION['lesson']);
  unset($_SESSION['teacher']);

?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL.'dashboard.php'?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Payment done</li>
    </ul>
</div>

<?php 
if(isset($_SESSION['payment']['Payment_Through']) && $_SESSION['payment']['Payment_Through'] == 'A') {

  // $responce = explode($_SESSION['payment']['delimiter'], $_SESSION['payment']['responce']);
  if($_SESSION['payment']['status'] == 1 || $_SESSION['payment']['status'] == 2) { ?>
    <h1>Success!</h1>
    <p><?=$_SESSION['payment']['message']?></p>
  <?php } else { ?>
    <h1>Error!</h1>
    <p><?=$_SESSION['payment']['message']?></p>
  <?php }
   // unset($_SESSION['payment']);

} else {
  if($_REQUEST['act'] == 'P') { ?>
    <h1>Pending!</h1>
    <p>It's still pending</p><?php
  } else if($_REQUEST['act'] == 'S') { ?>
    <h1>Success!</h1>
    <p>Payment done successfully.</p><?php
  } else if($_REQUEST['act'] == 'F') { ?>
    <h1><?=isset($_SESSION['payment']['title']) ? $_SESSION['payment']['title']: "Error"?></h1>
    <p><?=isset($_SESSION['payment']['message']) ? $_SESSION['payment']['message']: "Please check with site admin, payment not done due to some technical error"?></p><?php
  }
}
?>
<?php 
include 'footer.php'; 
?>