<?php
include './inc/config.php';
include './inc/functions.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap-responsive.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>index.css" />
	<link href='http://fonts.googleapis.com/css?family=Dosis:200,400' rel='stylesheet' type='text/css'>
	<style type="text/css">
		body {
			background-image: url('<?=IMG."heroshot.jpg"?>');
		}
		.cg-form-container legend h3{
			color: white;
			margin-left: 20px;
		}

		.cg-form-container a.link {
			color: cyan;
		}
		.cg-form-container {
			background-color: rgba(0,0,0,0.9);
			border: 10px solid rgba(255,255,255,0.5);
			border-radius: 15px;
			margin-top: 20%;
		}

		.cg-form-container input {
			max-width: 90%;
		}

	</style>
	<script language="javascript" src="<?php echo JS; ?>html5.js"></script>
	<script language="javascript" src="<?php echo JS; ?>jquery-1.8.0.min.js"></script>
	<script language="javascript" src="<?php echo JS; ?>bootstrap.js"></script>
	<script language="javascript" src="<?php echo JS; ?>index.js"></script>

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Registration</title>
</head>

<body class="homepage">


<div class="container">
	<div class="reg-form-holder">
		<div class="row-fluid">
			<div class="span10 offset1">
				<br/>
				<br/>
				<?php if(isset($_SESSION['error'])) { ?>
			    <div class="alert alert-error">
				    <button type="button" class="close" data-dismiss="alert">&times;</button>
				    <h4><?=(isset($_SESSION['error']['title'])?$_SESSION['error']['title']:"Error!")?></h4>
				    <?=$_SESSION['error']['text']?>
			    </div>
				<?php unset($_SESSION['error']); } ?>
				<form method="POST" class="form-horizontal" action="aim-payment.php" autocomplete="off">
				  <legend>Payment details</legend>
					<div class="row-fluid">
					  <div class="span3">
					    <label class="radio">
					      <input id="authorize" name="pay_type" type="radio" checked="checked"> Authorize.net
					    </label>
					  </div>
					  <div class="span3">
					    <label class="radio">
					      <input id="paypal" name="pay_type" type="radio"> PayPal
					    </label>
					  </div>
					</div>
					<br/>
					<br/>
					<div id="auth-div">
						
					  <div class="control-group">
					    <label class="control-label" for="CCNumber">CC Number</label>
					    <div class="controls">
					      <input type="text" name="CCNumber" id="CCNumber" placeholder="Credit Card Number">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="Expiery">Expiery date</label>
					    <div class="controls">
					      <input type="text" name="Expiery" id="Expiery" placeholder="MMYY">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="CVV">CVV</label>
					    <div class="controls">
					      <input type="text" name="CVV" id="CVV" placeholder="CVV">
					    </div>
					  </div>
					  <div class="control-group">
					    <div class="controls">
					      <button type="submit" class="btn">Pay</button>
					    </div>
					  </div>
					</div>
					<div id="paypal-div">
						<br/>
						Procced to pay through paypal
						<a href="paypal-init.php" class="btn">Pay</a>
					</div>
				</form>
				<br/>
				<br/>
			</div>
		</div>
	</div>
</div>
</body>
<script type="text/javascript">
  $("#paypal-div").hide();
  $("#authorize").on('change', function(e) {
    $("#auth-div").show(500);  
    $("#paypal-div").hide(500);
  });
  $("#paypal").on('change', function(e) {
    $("#paypal-div").show(500);
    $("#auth-div").hide(500);  
  });


</script>
</html>