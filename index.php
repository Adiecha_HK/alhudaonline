<?php include './inc/config.php'; 

$sql_qry="SELECT Id,Course,Brief FROM courses ORDER BY Id DESC";
/*
echo $sql_qry;
*/
$sql_res=mysql_query($sql_qry) or die(session_err("Database error!", mysql_error()));
$sql_nos=mysql_num_rows($sql_res);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Course</title>
<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>index.css" />
<link href='http://fonts.googleapis.com/css?family=Dosis:200,400' rel='stylesheet' type='text/css'>
<style type="text/css">
  body {
    background-image: url('<?=IMG."heroshot.jpg"?>');
  }
</style>
<script language="javascript" src="<?php echo JS; ?>html5.js"></script>
<script language="javascript" src="<?php echo JS; ?>jquery-1.8.0.min.js"></script>
<script language="javascript" src="<?php echo JS; ?>bootstrap.js"></script>
<script language="javascript" src="<?php echo JS; ?>index.js"></script>
</head>

<body class="homepage">
<div class="navbar cg-navbar-main-page navbar-fixed-top">
  <div class="navbar-inner cg-navbar-main-page">
    <div class="container">
 
      <!-- Be sure to leave the brand out there if you want it shown -->
      <a class="brand" href="#">Online Course</a>
 
      <!-- .nav, .navbar-search, .navbar-form, etc -->
      <ul class="nav pull-right">
        <!-- <li><a href="#">Link</a></li> -->
        <!-- <li class="divider-vertical"></li> -->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sign In <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo URL; ?>login.php">As Student</a></li>
            <li><a href="<?php echo URL; ?>teachers/login.php">As Teacher</a></li>
            <li><a href="<?php echo URL_ADM; ?>login.php">As Admin</a></li>
            <li class="divider"></li>
            <li><a href="register.php">Register</a></li>
          </ul>
        </li>
      </ul>
 
    </div>
  </div>
</div>
<div class="container">
  <div class="landing-page-msg">
    <h2>Courses designed for School, College & University Students, Working Men & Women, Anyone who needs a more flexible schedule<br/>
  Once you register we will tailor a course according to YOUR weekly schedule!</h2>
    <hr />
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="span8">
    <?php if($sql_nos > 0) { ?>
      <div id="myCarousel" class="carousel slide">
        <!-- Carousel items -->
        <div class="carousel-inner">

          <?php $actv_cls = 'active ';
          while($sql_row=mysql_fetch_array($sql_res)) { ?>
          <div class="<?=$actv_cls?>item">
            <div class="in-block">
              <h2><?=$sql_row['Course']?></h2>
              <p><?=$sql_row['Brief']?></p>
            </div>
          </div>
          <?php $actv_cls = ''; } ?>
        </div>
        <!-- Carousel nav -->

        <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>

      </div>
    <?php } else { ?>
      <h3>Courses will be available here sortly.</h3>
    <?php } ?>
    </div>
    <div class="span4 benifits">
      <h2>Benifits</h2>
      <ul>
        <li>Course begins when you are ready to begin! </li>
        <li>Access audio lectures, take tests, and assignments at your convenience!</li>
        <li>Studies Incharges will be assigned for Quran Course students!</li>
        <li>Once a Month Meetings on Conference Call for questions and concerns!</li>
        <li>Get Weekly Reminders to Stay focused and on Track!</li>
      </ul>
    </div>
  </div>
</div>

<div class="hide">
  
<center><h1>Welcome to Online Course</h1></center>
<br /><br />
<center>
  <a href="<?php echo URL; ?>login.php">Proceed as a student</a> &nbsp;
  <a href="<?php echo URL; ?>teachers/login.php">Proceed as a teacher</a> &nbsp;
  <a href="<?php echo URL_ADM; ?>login.php">Proceed as an administrator</a> &nbsp;
</center>
</div>
</body>
</html>
<?php mysql_close($db_con); ?>