<?PHP
include './inc/config.php';
include './inc/functions.php';

$post_values = array(
  "USER"=>PAYPAL_USER,
  "PWD"=>PAYPAL_PWD,
  "SIGNATURE"=>PAYPAL_SIGNATURE,
  "METHOD"=>"SetExpressCheckout",
  "VERSION"=>"93",
  "PAYMENTREQUEST_0_PAYMENTACTION"=>"SALE",
  // "PAYMENTREQUEST_0_AMT"=>$_SESSION['amount'],
  "PAYMENTREQUEST_0_AMT"=>"0.01",
  "PAYMENTREQUEST_0_CURRENCYCODE"=>"USD",
  "RETURNURL"=>URL."ret-test.php?act=success",
  "CANCELURL"=>URL."ret-test.php?act=cancel"
);

$post_string = "";
foreach( $post_values as $key => $value) {
  $post_string .= "$key=" . urlencode( $value ) . "&";
}
$post_string = rtrim( $post_string, "& " );

$request = curl_init(PAYPAL_API);
curl_setopt($request, CURLOPT_HEADER, 0);
curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
$post_response = curl_exec($request);
curl_close ($request);

$arr = explode("&", $post_response);
$res = array();
foreach ($arr as $i => $nvp) {
  # code...
  $nvpa = explode("=", $nvp);
  $key = $nvpa[0];
  $val = $nvpa[1];
  $res[$key] = urldecode($val);
}

if($res['ACK'] == "Success") {
  $_SESSION['PAYPAL_TOKEN'] = $res['TOKEN'];
  header("location: ".PAYPAL_URL.$res['TOKEN']);
} else {
  echo var_dump($res);
}

?>