<?php
$page_nav="Payment";
$page_title="Payment";
include 'header.php'; 
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL.'dashboard.php'?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Payment done</li>
    </ul>
</div>

<?php 
if(isset($_SESSION['payment']['Payment_Through']) && $_SESSION['payment']['Payment_Through'] == 'A') {

  $responce = explode($_SESSION['payment']['delimiter'], $_SESSION['payment']['responce']);
  if($responce[0] == 1) { ?>
    <h1>Success!</h1>
    <p><?=$responce[3]?></p>
  <?php } else { ?>
    <h1>Error!</h1>
    <p><?=$responce[3]?></p>
  <?php } unset($_SESSION['payment']);

} else {
  if($_REQUEST['act'] == 'P') {
    echo "It's still panding";
  } else if($_REQUEST['act'] == 'S') {
    echo "Payment done successfully";
  } else if($_REQUEST['act'] == 'F') {
    echo "Payment Failed";
  } else {
    echo "Invalid state of payment, please contact site admin or technical person";
  }
}
?>
<?php 
include 'footer.php'; 
?>