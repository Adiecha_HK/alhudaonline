<?php
$page_nav="Payment";
$page_title="Payment";
include 'header.php'; 
$_SESSION['Payer'] = get_record('students', '', 'Id='.$_SESSION["student_uniqueid"]);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL.'dashboard.php'?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Payment</li>
    </ul>
</div>
<div class="row-fluid">
  <div class="span3">
    <label class="radio">
      <input id="authorize" name="pay_type" type="radio" checked="checked"> Authorize.net
    </label>
  </div>
  <div class="span3">
    <label class="radio">
      <input id="paypal" name="pay_type" type="radio"> PayPal
    </label>
  </div>
</div>
<div class="row-fluid">
  <div class="span6">
    <div id="auth-div">
      <form method="POST" class="form-horizontal" action="aim-payment.php" autocomplete="off">
        <legend>Card details</legend>
                
            <div class="control-group">
              <label class="control-label" for="CCNumber">CC Number</label>
              <div class="controls">
                <input type="text" name="CCNumber" id="CCNumber" placeholder="Credit Card Number">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="Expiery">Expiery date</label>
              <div class="controls">
                <input type="text" name="Expiery" id="Expiery" placeholder="MMYY">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="CVV">CVV</label>
              <div class="controls">
                <input type="password" name="CVV" id="CVV" placeholder="CVV">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="Address">Billing Address</label>
              <div class="controls">
                <textarea name="Address" id="Address" placeholder="Enter Address"></textarea>
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Pay</button>
              </div>
            </div>
      </form>
    </div>
    <div id="paypal-div">
      <div>
        Click on pay with paypal.
      </div><br/>
      <a href="paypal-init.php" class="btn btn-success">Procced to pay with paypal</a>
    </div>
  </div>
  <div class="span6">
    <legend>Order details</legend>
    <?php if(isset($_SESSION['purches_type']) && $_SESSION['purches_type'] == "course_subscribe") { ?>
    <table class="table">
      <tr>
        <th>Course</th>
        <td><?php
          $course = get_record('courses', 'Course', 'Id='.$_SESSION['course']);
          echo $course['Course'];
        ?></td>
      </tr>
      <tr>
        <th>Amount</th>
        <td><?=$_SESSION['amount']?>&nbsp;USD</td>
      </tr>
    </table>
    <?php } else { ?>
    <table class="table">
      <tr>
        <th>Course</th>
        <td><?php
          $course = get_record('courses', 'Course', 'Id='.$_SESSION['course']);
          echo $course['Course'];
        ?></td>
      </tr>
      <tr>
        <th>Lesson</th>
        <td><?php 
          $lesson = get_record('lessons', 'Lesson', 'Id='.$_SESSION['lesson']);
          echo $lesson['Lesson'];
        ?></td>
      </tr>
      <?php if (isset($_SESSION['teacher'])) { ?>
      <tr>
        <th>Teacher</th>
        <td><?php
          $teacher = get_record('teachers', 'CONCAT(`First_Name`,\' \',`Last_Name`) AS FullName', 'Id='.$_SESSION['teacher']);
          echo $teacher['FullName'];
        ?></td>
      </tr>
      <?php } ?>
      <?php if(isset($_SESSION['date'])) { ?>
      <tr>
        <th>Date & time</th>
        <td><?=$_SESSION['date']." (".$_SESSION['hr'].":".$_SESSION['min'].")"?></td>
      </tr>
      <?php } ?>
      <tr>
        <th>Amount</th>
        <td><?=$_SESSION['amount']?>&nbsp;USD</td>
      </tr>
    </table>
    <?php } ?>
  </div>
</div>
<script type="text/javascript">
  $("#paypal-div").hide();
  $("#authorize").on('change', function(e) {
    $("#auth-div").show(500);  
    $("#paypal-div").hide(500);
  });
  $("#paypal").on('change', function(e) {
    $("#paypal-div").show(500);
    $("#auth-div").hide(500);  
  });
</script>
<?php include 'footer.php'; ?>