<?php
include './inc/config.php';
include './inc/functions.php';

if (isset($_SESSION["anybody_loggedin"]) && $_SESSION["anybody_loggedin"]=="student") {
	header("Location: ".URL."dashboard.php");
	?><script type="text/javascript">window.location = "<?=URL.'dashboard.php'?>"</script><?php
	exit;
}


if($_SERVER['REQUEST_METHOD'] == "POST") {
	$exist = trim($_REQUEST['Username']) != "" ? get_count('students', "Username='".$_REQUEST['Username']."' && Reg_Status='S'"): null;
	if(!isset($exist) || $exist > 0) {

		session_err('Invalid!', "Username is already exist, please try another username.");
		header("location: " . $_SERVER['PHP_SELF']);
		?><script type="text/javascript">window.location = "<?=$_SERVER['PHP_SELF']?>"</script><?php
		exit;

	} else {

		$students_fields = array('Username', 'Password', 'Salutation', 'First_Name', 'Middle_Name', 'Last_Name', 'Email', 'Phone', 'City', 'Country');
		$sql_qry = "INSERT INTO students SET " . collect_frm_req($students_fields) . ", Entry_Date=NOW(), Last_Modified=NOW(), Status='P', Reg_Status='P';";
		mysql_query($sql_qry) or die(session_err('Database error:', "Unsuccesful Registration - " . mysql_error()));		
		if(!isset($_SESSION['error'])) {
		  $amount = $_REQUEST['amount'];
			$uid = mysql_insert_id();
			mysql_query("INSERT INTO student_additional_details SET ".collect_frm_req(array('Gender', 'Marital_Status', 'Education', 'First_Language', 'DOB','Address1', 'Address2', 'City', 'Zip_Code', 'State', 'Hear_From', 'Did_Before', 'Why', 'Reading_Proficiency')) . ", `Student_Id`='".$uid.'"');
		  foreach ($_REQUEST['courses'] as $cid) {
		    mysql_query("INSERT INTO students_courses SET Course_Id='" . $cid . "', Student_Id = '" . $uid . "'");
		  }
		  $data = get_record('students', '', 'Id='.$uid);
		  $token = random_string();
		  $data['KEY'] = URL."confirm.php?v=".$token;
		  mysql_query("INSERT INTO `email_validation` SET `Sid`='".$uid."', `Hash`='".$token."'");
		  if(isset($_REQUEST['Email'])) {
		  	# send_mail($_REQUEST['Email'], "Welcome to Al-huda online course", "welcome_mail.html", $data);
		  }
			$_SESSION['uid'] = $uid;
			$_SESSION['Payer'] = get_record('students', '', 'Id='.$uid);
			$_SESSION['Discription'] = "Registration with amount of " . $amount . " USD";
			// $_SESSION['Discription'] = "User registered with Registration# ". $uid . " with amount of " . $amount . " USD";
			$_SESSION['amount'] = $amount;
			// $_SESSION['redirect_to'] = "reg-thanks.php";
			$_SESSION['redirect_to'] = "/registration-thanks/";
			session_msg('Successful', "Registration successful.");
			$sql_qry = "INSERT INTO `payment` SET `Sid`='".$uid."', `Type`='R', `Amount`='".$amount."', `Entry_Date`=NOW(), `Payment_Status`='P';";
			mysql_query($sql_qry);
			$_SESSION['payment_id'] = mysql_insert_id();

			// code to send a mail
			$DearName = $_REQUEST['First_Name'] . " " . $_REQUEST['Last_Name'];
			$email_to = $_REQUEST['Email'];
			include "email-templates/email-registration.php";
			// echo "<a href='" . URL . "register-payment.php'>Click to proceed.</a>";
			// exit;

			// header("location: " . URL . "register-payment.php");
			?><script type="text/javascript">// window.location = "<?=URL.'register-payment.php'?>"</script><?php

			header("location: /register-payment");
			?><script type="text/javascript">window.location = "/register-payment"</script><?php


		} else {
			header("location: " . $_SERVER['PHP_SELF']);
			?><script type="text/javascript">window.location = "<?=$_SERVER['PHP_SELF']?>"</script><?php
			exit;
		}
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap-responsive.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>index.css" />
	<link href='http://fonts.googleapis.com/css?family=Dosis:200,400' rel='stylesheet' type='text/css'>
	<style type="text/css">
		body {
			background-image: url('<?=IMG."heroshot.jpg"?>');
		}
		.cg-form-container legend h3{
			color: white;
			margin-left: 20px;
		}

		.cg-form-container a.link {
			color: cyan;
		}
		.cg-form-container {
			background-color: rgba(0,0,0,0.9);
			border: 10px solid rgba(255,255,255,0.5);
			border-radius: 15px;
			margin-top: 20%;
		}

		.cg-form-container input {
			max-width: 90%;
		}

		form h4 {
			font-size: 20.5px;
			text-decoration: underline;
		}

		input, textarea, select {
			width: 400px;
		}

		textarea {
			height: 100px;
			resize: none;
		}

		span.v-icon {
			position: relative;
			left: -25px;
			top: -5px;
		}

	</style>
	<script language="javascript" src="<?php echo JS; ?>html5.js"></script>
	<script language="javascript" src="<?php echo JS; ?>jquery-1.8.0.min.js"></script>
	<script language="javascript" src="<?php echo JS; ?>bootstrap.js"></script>
	<script language="javascript" src="<?php echo JS; ?>index.js"></script>

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Registration</title>
</head>

<body class="homepage">

<div url="<?=AJAX.'validation-ajax.php'?>" id="ajax-url"></div>

<div class="container">
	<div class="reg-form-holder">
		<div class="row-fluid">
			<div class="span10 offset1">
			<br/>
			<br/>
			<?php if(isset($_SESSION['error'])) { ?>
		    <div class="alert alert-error">
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
			    <h4><?=(isset($_SESSION['error']['title'])?$_SESSION['error']['title']:"Error!")?></h4>
			    <?=$_SESSION['error']['text']?>
		    </div>
			<?php unset($_SESSION['error']); } ?>
				<form method="POST" class="form">
				  <fieldset>
				    <legend>Al Huda Online Courses - Application for Admission Form</legend>
				    <div class="row-fluid">
				    	<div class="offset1 span10">
					    		
				    		<h4>Personal Information</h4>
				    		<br/>
							  <div class="control-group hide">
							    <label class="control-label" for="title">Salutation</label>
							    <div class="controls">
							    	<select name="Salutation">
							    		<option value="Mr">Mr</option>
							    		<option value="Mrs">Mrs</option>
							    		<option value="Miss">Miss</option>
							    		<option value="Ms">Ms</option>
							    		<option value="Dr">Dr</option>
							    		<option value="Er">Er</option>
							    	</select>
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="firstname">First name</label>
							    <div class="controls">
							    	<input name="First_Name" type="text" id="firstname">
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="lastname">Last name</label>
							    <div class="controls">
							    	<input name="Last_Name" type="text" id="lastname">
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="middlename">Father/Guardian/Husband's Name</label>
							    <div class="controls">
							    	<input name="Middle_Name" type="text" id="middlename">
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="Gender">Gender</label>
							    <div class="controls">
							    	<select name="Gender" type="text" id="Gender">
							    		<option value="M">Male</option>
							    		<option value="F">Female</option>
							    	</select>
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="Marital_Status">Marital Status</label>
							    <div class="controls">
							    	<select name="Marital_Status" type="text" id="Marital_Status">
							    		<option value='U'>Unmarried</option>
							    		<option value='M'>Married	</option>
							    		<option value='W'>Widow</option>
							    		<option value='S'>Seperated</option>
							    	</select>
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="Education">Education</label>
							    <div class="controls">
							    	<input name="Education" type="text" id="Education">
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="First_Language">First Language</label>
							    <div class="controls">
							    	<input name="First_Language" type="text" id="First_Language">
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="country">Country</label>
							    <div class="controls">
							    	<input name="Country" type="text" id="country">
							    </div>
							  </div>
							  <!--
							  <div class="control-group">
							    <label class="control-label" for="Profession">Profession</label>
							    <div class="controls">
							    	<input name="Profession" type="text" id="Profession">
							    </div>
							  </div>
							  -->
							  <div class="control-group">
							    <label class="control-label" for="DOB">Date of birth</label>
							    <div class="controls">
							    	<input name="DOB" type="date" id="DOB">
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="phone">Phone</label>
							    <div class="controls">
							    	<input name="Phone" type="text" id="phone">
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="address">Address</label>
							    <div class="controls">
							    	<input name="Address1" type="text" id="address" placeholder="Address line 1"><br/>
							    	<input name="Address2" type="text"  placeholder="Address line 2"><br/>
							    	<input name="City" type="text"  placeholder="Enter your City"><br/>
							    	<input name="Zip_Code" type="text"  placeholder="Enter your Zip Code"><br/>
							    	<input name="State" type="text"  placeholder="Enter your State"><br/>
							    	<input name="Country" type="text"  placeholder="Enter your Country"><br/>

							    </div>
							  </div>
							  <br/>
							  <br/>
							  <h4>Other information</h4>
							  <br/>
							  <div class="control-group">
							    <label class="control-label" for="Hear_Abt_Us">How did you hear about this course?</label>
							    <div class="controls">
							    	<input name="Hear_From" type="text" id="Hear_Abt_Us">
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="Related_Course_Details">Have you done any course related to the Qur'aan before? Please mention in detail.</label>
							    <div class="controls">
							    	<textarea name="Did_Before" type="text" id="Related_Course_Details"></textarea>
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="Why">Why do you want to do this course?</label>
							    <div class="controls">
							    	<textarea name="Why" type="text" id="Why"></textarea>
							    </div>
							  </div>

							  <div class="control-group">
							    <label class="control-label" for="Reading_Proficiency">Proficiency in reading the Quran</label>
							    <div class="controls">
								    <label class="radio"><input type="radio" name="Reading_Proficiency" value="Can read the Quran fluently" />&nbsp;Can read the Quran fluently</label>
								    <label class="radio"><input type="radio" name="Reading_Proficiency" value="Can read the Quran slowly with mistakes" />&nbsp;Can read the Quran slowly with mistakes</label>
								    <label class="radio"><input type="radio" name="Reading_Proficiency" value="Cannot read the Quran (or can read minimally)" />&nbsp;Cannot read the Quran (or can read minimally)</label>
								    <label class="radio"><input type="radio" name="Reading_Proficiency" value="Can read arabic quickly" />&nbsp;Can read arabic quickly</label>
							    </div>
							  </div>
							  <br/>
							  <br/>
				    		<h4>Course Details</h4>
							  <br/>
							  <div class="control-group">
							    <label class="control-label" for="courses1">Tafheem al Quran Courses</label>
							    <div class="controls">
							    	<select name="courses[]" class="courses" id="courses1" validate="amt">
							    		<option value="0"> -- None -- </option>
							    		<?php
							    		$tq_list = get_records('courses', 'Id, Course', 'Course_Type=\'TQ\'');
							    		foreach ($tq_list as $key => $value) { ?>
							    		<option value="<?=$value['Id']?>"><?=$value['Course']?></option>
							    		<?php } ?>
							    	</select>
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="courses2">Tajweed &amp; Hifdh Courses</label>
							    <div class="controls">
							    	<select name="courses[]" class="courses" id="courses2" validate="amt">
							    		<option value="0"> -- None -- </option>
							    		<?php
							    		$tq_list = get_records('courses', 'Id, Course', 'Course_Type=\'TH\'');
							    		foreach ($tq_list as $key => $value) { ?>
							    		<option value="<?=$value['Id']?>"><?=$value['Course']?></option>
							    		<?php } ?>
							    	</select>
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="courses3">Audio Hadith &amp; Tazkiyah Courses</label>
							    <div class="controls">
							    	<select name="courses[]" class="courses" id="courses3" validate="amt">
							    		<option value="0"> -- None -- </option>
							    		<?php
							    		$tq_list = get_records('courses', 'Id, Course', 'Course_Type=\'H\'');
							    		foreach ($tq_list as $key => $value) { ?>
							    		<option value="<?=$value['Id']?>"><?=$value['Course']?></option>
							    		<?php } ?>
							    	</select>
							    </div>
							    <br/>
							    <br/>
				    	</div>
				    		<h4>Account Details</h4>
				    		<br/>
							  <div class="control-group">
							    <label class="control-label" for="username">Username *</label>
							    <div class="controls">
							    	<input name="Username" type="text" id="username" required validate="server" act="validate_username" msg="Invalid Username"	/>
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="password">Password *</label>
							    <div class="controls">
							    	<input name="Password" type="password" id="password" required>
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="repassword">Re-enter password *</label>
							    <div class="controls">
							    	<input name="rePassword" type="password" id="repassword" required validate="password" msg="Password doesn't match.">
							    </div>
							  </div>
							  <div class="control-group">
							    <label class="control-label" for="email">Email *</label>
							    <div class="controls">
							    	<input name="Email" type="text" id="email" validate="email" required msg="Please insert valid email address">
							    	<span class="help-block">Please make double sure you enter the correct email address.</span>
							    </div>
							  </div>
							  <br/>
							  <br/>
							  </div>
				    </div>
					  <br/>
					  <br/>
				    <div class="row-fluid">
				    	<div class="offset1 span10">
				    		<h4>Agreement</h4>
							  <br/>
							  <div class="control-group">
						    	<label class="control-label checkbox"><input name="Req_Fullfill" type="checkbox" />
									I understand that to qualify for the certificate I must fulfill all course requirements
									</label>
								</div>
							  <br/>
							  <div class="control-group">
						    	<label class="control-label checkbox"><input name="true_info" type="checkbox" />
									I make Allah my witness that all information provided is true and take pledge that InshaAllah I will complete all my required work for Certification, before the completion of the Course.
									</label>
								</div>
				    	</div>
				    </div>
					  <br/>
					  <br/>
				    <div class="row-fluid">
				    	<div class="offset1 span5">
							  <input type="hidden" name="amount" validation="non_zero_num" value="0" id="amount" target=".courses" msg="Please select course" />
							  <h5>Amount to pay <span id="amount_txt">0</span>&nbsp;USD</h5>
				    	</div>
				    	<div class="span5">
				    		<button type="submit" class="btn" >Proceed to pay</button>				    		
				    	</div>				    	
				    </div>
				  </fieldset>
				</form>
				<br/>
				<br/>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
	var validate = {errors:[]};

	var makeErr = function(input, msg) {
		var i = validate.errors.indexOf(msg);
		if(i == -1) {
			validate.errors.push(msg);
			$(input).attr('title', msg);
			$(input).parents(".control-group").addClass("error");
		}
	}

	var removeErr = function(input, msg) {
		var i = validate.errors.indexOf(msg);
		if(i != -1) {
			validate.errors.splice(i, 1);
			$(input).removeAttr('title');
			$(input).parents(".control-group").removeClass("error");
		}
	}

	$(document).ready(function() {

		$(".courses").on('change', function() {
			var amount = 0;
			$(".courses").each(function() {
				if($(this).val() > 0) amount+=20;
			}).promise().done(function() {
				// alert('hello');
				$("#amount").val(amount);
				$("#amount_txt").text(amount);
			});
		});

		$(".courses:first").change();

		$("form").on('submit', function() {
			if($("#amount").val() == 0) makeErr(".courses", "Please select course.");
			else removeErr(".courses", "Please select course.");
			
			if($("[name=Req_Fullfill]").attr('checked') == undefined) makeErr("[name=Req_Fullfill]", "Please check this to agree that you are fullfill all requirement.");
			else removeErr("[name=Req_Fullfill]", "Please check this to agree that you are fullfill all requirement.");
			if($("[name=true_info]").attr('checked') == undefined) makeErr("[name=true_info]", "Please check this to agree all the information you provided is correct");
			else removeErr("[name=true_info]", "Please check this to agree all the information you provided is correct");

			if(validate.errors.length > 0) {
				alert("* " + validate.errors.join("\n* "));
				return false;
			}
			return true;
		});

		$(":input[validate]").on('blur', function() {
			var input = $(this);
			var valid = $(input).attr('validate');
			var msg = $(input).attr('msg');
			switch(valid) {
				case 'server':
					var url = $("#ajax-url").attr('url');
					var data = {'action': $(input).attr("act"), 'data': $(input).val()};
					$.post(url, data, function(data) {
						if(!data.status) makeErr(input, msg);
						else removeErr(input, msg);
					});
					break;
				case 'amt':
					if($("#amount").val() == 0) makeErr(".courses", "Please select course.");
					else removeErr(".courses", "Please select course.");
					break;
				case 'password':
					if($("#password").val() == $(input).val()) removeErr(input, msg);
					else makeErr(input, msg);
					break;
				case 'email':
          var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
          email = $(input).val();
          if (reg.test(email) == false) makeErr(input, msg);
          else {
						removeErr(input, msg);
						var url = $("#ajax-url").attr('url');
						var data = {'action': "validate_email", 'data': email};
						$.post(url, data, function(data) {
							if(!data.status) makeErr(input, "Email address already exist.");
							else removeErr(input, "Email address already exist.");
						});
          }
          break;
        case '':
			}
		});
	});

</script>
</body>
</html>
