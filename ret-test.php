<?php


include './inc/config.php';
include './inc/functions.php';

$act = $_REQUEST['act'];
$ps = 'P';
if($act == "cancel") {
  $ps = 'F';
} else if($act=="success") {
  $ps = 'S';
}
$post_values = array(
  "USER"=>PAYPAL_USER,
  "PWD"=>PAYPAL_PWD,
  "SIGNATURE"=>PAYPAL_SIGNATURE,
  "METHOD"=>"GetExpressCheckoutDetails",
  "VERSION"=>"93",
  "TOKEN"=>$_REQUEST['token']);
$post_string = "";
foreach( $post_values as $key => $value) {
  $post_string .= "$key=" . urlencode( $value ) . "&";
}
$post_string = rtrim( $post_string, "& " );
$request = curl_init(PAYPAL_API);
curl_setopt($request, CURLOPT_HEADER, 0);
curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
$post_response = curl_exec($request);
curl_close ($request);

$express_chkout_res = getResToArr($post_response);

if(isset($express_chkout_res['PAYERID'])) {

  $post_values2 = array(
    "USER"=>PAYPAL_USER,
    "PWD"=>PAYPAL_PWD,
    "SIGNATURE"=>PAYPAL_SIGNATURE,
    "METHOD"=>"DoExpressCheckoutPayment",
    "VERSION"=>"93",
    "TOKEN"=>$_REQUEST['token'],
    "PAYERID"=>$express_chkout_res['PAYERID'],
    "PAYMENTREQUEST_0_PAYMENTACTION"=>"SALE",
    "PAYMENTREQUEST_0_AMT"=>$_SESSION['amount'],
    "PAYMENTREQUEST_0_CURRENCYCODE"=>"USD");
$post_string2 = "";
foreach( $post_values2 as $key => $value) {
  $post_string2 .= "$key=" . urlencode( $value ) . "&";
}
$post_string2 = rtrim( $post_string2, "& " );
  $request2 = curl_init(PAYPAL_API);
  curl_setopt($request2, CURLOPT_HEADER, 0);
  curl_setopt($request2, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($request2, CURLOPT_POSTFIELDS, $post_string2);
  curl_setopt($request2, CURLOPT_SSL_VERIFYPEER, FALSE);
  $post_response2 = curl_exec($request2);
  curl_close ($request2);
}

if(strtolower($express_chkout_res['ACK']) == "success") {
  $ps = 'S';
} else {
  $ps = 'F';
  $_SESSION['payment'] = array('status' => FALSE, 'title'=> $post_response2_json['L_SHORTMESSAGE0'], 'message'=>$post_response2_json['L_LONGMESSAGE0']);
}
$json = array('GetExpressCheckoutDetails'=> $express_chkout_res);
if(isset($express_chkout_res['PAYERID'])) {

  $post_response2_json = getResToArr($post_response2);
  $json['DoExpressCheckoutPayment'] = $post_response2_json;
  if(strtolower($post_response2_json['ACK']) == "success") {
    $ps = 'S';
    $sql_qry = "UPDATE `payment` SET `Payment_Status`='".$ps."', `TransectionId`='".$post_response2_json["PAYMENTINFO_0_TRANSACTIONID"]."', Json='".escape_string(json_encode($json), '', 'db')."'  WHERE `Token`='".$_REQUEST['token']."'";
    mysql_query($sql_qry) or die("SQL Error!: <br/><br/>".$sql_qry."<br/><br/>".mysql_error());
  } else {
    $ps = 'F';
    $_SESSION['payment'] = array('status' => FALSE, 'title'=> $post_response2_json['L_SHORTMESSAGE0'], 'message'=>$post_response2_json['L_LONGMESSAGE0']);
  }

} else {
  $ps = 'F';
  $_SESSION['payment'] = array('status' => FALSE, 'title'=> $post_response2_json['L_SHORTMESSAGE0'], 'message'=>$post_response2_json['L_LONGMESSAGE0']);
}
$pymnt = get_record('`payment`', '`Type`', '`Id`='.$_SESSION['payment_id']);
if($pymnt['Type'] == 'B') {

  if($ps == 'S') {
    // $email_for = 'S'; include 'email-templates/email-booking-schedule.php';
    // $email_for = 'T'; include 'email-templates/email-booking-schedule.php';
    // $email_for = 'A'; include 'email-templates/email-booking-schedule.php';
    
    $_SESSION['final_redirect'] = "booking-thanks.php?act=" . $ps;
    header("location: ".URL."email-queues/bookingq-student.php");
    echo "<script type='text/javascript'>window.location = '".URL."email-queues/bookingq-student.php'</script>";
    exit;
  } else {
    // Release teacher avalability slot
    $pymnt_tmp = get_record('`payment`', '`Schedule_Id`', '`Id`='.$_SESSION['payment_id']);
    $sql_qry = "UPDATE `teachers_availabilities` SET
        `Is_Scheduled`='N' 
      WHERE
        `Id` in (SELECT `Teachers_Aval` FROM `students_schedules` WHERE `Id` = '".$pymnt_tmp['Schedule_Id']."')";
    mysql_query($sql_qry);

  }


  header("location: booking-thanks.php?act=$ps");
  echo "<script type='text/javascript'>window.location = 'booking-thanks.php?act=".$ps."'</script>";
  exit;
} else if($pymnt['Type'] == 'S') {

  if($ps == 'S') {
    mysql_query("INSERT INTO students_courses SET Course_Id='" . $_SESSION['course'] . "', Student_Id = '" . $_SESSION['uid'] . "'");
  }


  header("location: booking-thanks.php?act=$ps");
  echo "<script type='text/javascript'>window.location = 'booking-thanks.php?act=".$ps."'</script>";
  exit;
} else {
  if($ps == 'S') {

    $sql_qry = "UPDATE students SET Reg_Status='S', Status='A' WHERE Id='".$_SESSION['uid']."';";
    mysql_query($sql_qry) or die("SQL Error!: <br/><br/>".$sql_qry."<br/><br/>".mysql_error());

    // include 'email-templates/email-registration-confirmation.php';
    // include 'email-templates/email-registration-notification.php';

    $_SESSION['final_redirect'] = "/registration-thanks/?act=" . $ps;
    // $_SESSION['final_redirect'] = "reg-thanks.php?act=" . $ps;
    header("location: ".URL."email-queues/regq-student.php");
    echo "<script type='text/javascript'>window.location = '".URL."email-queues/regq-student.php'</script>";
    exit;
  }

  header("location: /registration-thanks/?act=$ps");
  echo "<script type='text/javascript'>window.location = '/registration-thanks/?act=$ps'</script>";
  // header("location: reg-thanks.php?act=$ps");
  // echo "<script type='text/javascript'>window.location = 'reg-thanks.php?act=$ps'</script>";
  exit;
}

?>