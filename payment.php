<?php
$page_nav="Payment";
$page_title="Payment";
include 'header.php'; 
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL.'dashboard.php'?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Payment</li>
    </ul>
</div>
<?php
$payments = get_payments($_SESSION["student_uniqueid"]) or die(session_err('Database error!', "unable to retrive payments (orders) -- " . mysql_error()));
$sql_nos = mysql_num_rows($payments);
?>
<table class="table">
  <tr>
    <th>Type</th>
    <th>Detail</th>
    <th>Through</th>
    <th>Status</th>
    <th title="Lesson charge - discount = actual payble amount">Charge</th>
    <th>Date</th>
  </tr>
  <?php
    if($sql_nos>0){ while($payment=mysql_fetch_array($payments)){
    ?>
    <tr>
      <td><?=$payment['Type'] == 'B'?"Booking":"Registration"?></td>
      <td><?=$payment['Type'] == 'B'?$payment['Lesson']." of ".$payment['Course']:"Registration"?></td>
      <td>
        <?=$payment['Payment_Through']=='A'?"Authorized.Net":($payment['Payment_Through']=='P'?"Paypal":"Setalement")?>
      </td>
      <td>
        <?=$payment['Payment_Status']=='S'?"Successful":($payment['Payment_Status']=='F'?"Failed":"Panding")?>
      </td>
      <td>
        <?=$payment['Lesson_Charge']?>
        -
        <?=$payment['Discount']?>
        =
        <?=$payment['Amount']?>
      </td>
      <td><?=dashboard_formate($payment['Entry_Date'])?></td>
    </tr>
    <?php
  }}
  ?>
</table>

<?php include 'footer.php'; ?>