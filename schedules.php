<?php 
$page_nav="Schedules";
$page_title="Schedules";
$page_script=array('teacher-schedule.js');
include 'header.php'; 

$act = $_REQUEST['act'];
if($act == "cancel") {

  cancel_schedule($_REQUEST['id']);
  $_SESSION['schedule_id'] = $_REQUEST['id'];
  $_SESSION['final_redirect'] = 'schedules.php';
  header("location: ./email-queues/cancelq-student.php");
  ?><script type="text/javascript"> window.location = "./email-queues/cancelq-student.php";</script><?php
  exit;
}

/*
echo $sql_qry;
*/
$sql_res=student_schedule($_SESSION['student_uniqueid']);
$sql_nos=mysql_num_rows($sql_res);


?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL.'dashboard.php'?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Schedule</li>
    </ul>
</div>
<!-- List Courses >>> -->
<table class="DataTable table">
  <tr class="DataTableHeading">
    <th width="6%" align="left">Date</th>
    <th width="5%" align="center">Time</th>
    <th width="12%" align="left">Course</th>
    <th width="11%" align="left">Lesson</th>
    <th width="12%" align="left">Teacher</th>
    <th width="11%" align="center">Duration</th>
    <th width="11%" align="center">Status</th>
    <th width="12%" align="center">Remark</th>
    <th width="20%" align="center">More</th>
  </tr> 
<?php
$iDate="";
if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
  if($iDate!=$sql_row["Schedule_Date"])
  {
?>
  <tr class="DataTableRow">
    <td align="left"><?php echo $sql_row["Schedule_Date"]; ?></td>
    <td align="center"></td>
    <td align="left"></td>
    <td align="left"></td>
    <td align="left"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
  </tr>
<?php
    $iDate=$sql_row["Schedule_Date"];   
  }
?>
  <tr class="DataTableRow">
    <td align="left"></td>
    <td align="center"><?php echo $sql_row["Schedule_Time"]; ?></td>
    <td align="left"><?php echo $sql_row["Course"]; ?></td>
    <td align="left"><?php echo $sql_row["Lesson"]; ?></td>
    <td align="left"><?php echo $sql_row["Teacher"]; ?></td>
    <td align="center"><?php echo $sql_row["Duration"]; ?></td>
    <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
    <td align="center"></td>
    <td align="center">
      <?php if($sql_row['Status'] == "OK" && $sql_row['Days'] > 1) { ?>
        <a class="btn btn-info cancel-btn" href="?act=cancel&id=<?=$sql_row['Id']?>">Cancel</a>&nbsp;
      <?php } ?>
    </td>
  </tr>
<?php
}}else{
?>
  <tr>
    <td colspan="9" align="center">There is no Scheduled lession found.</td>
  </tr>
<?php 
}
?>
</table>
<!-- List Courses <<< -->
<script type="text/javascript">
  $(".cancel-btn").on('click', function() {
    var cond = confirm("Sure, you want to cancel schedule?");
    return cond;
  })
</script>
<?php 
include 'footer.php'; 
?>
