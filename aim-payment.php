<?php
include './inc/config.php';
include './inc/functions.php';

if(isset($_SESSION['payment_id'])) {
	$sql_qry = "UPDATE `payment` SET `Payment_Through`='A' WHERE Id='".$_SESSION['payment_id']."'";
	mysql_query($sql_qry);
}

// $_SESSION['amount'] = "0.01";

$post_url = AUTH_URL;

$post_values = array(
	
	"x_login"						=> AUTH_LOGIN,
	"x_tran_key"				=> AUTH_TKEY,
	"x_version"					=> "3.1",
	"x_delim_data"			=> "TRUE",
	"x_delim_char"			=> "|",
	"x_relay_response"	=> "FALSE",
	"x_type"						=> "AUTH_CAPTURE",
	"x_method"					=> "CC",
	"x_card_num"				=> $_POST['CCNumber'],
	"x_exp_date"				=> $_POST['Expiery'],
	"x_amount"					=> $_SESSION['amount'],
	"x_description"			=> isset($_SESSION['Discription'])?$_SESSION['Discription']: "Alhuda flexible purches",
	"x_recurring_billing" => "NO",

	"x_first_name"			=> $_SESSION['Payer']['First_Name'],
	"x_last_name"				=> $_SESSION['Payer']['Last_Name'],
	"x_address"					=> $_POST['Address'],
	"x_phone"						=> $_SESSION['Payer']['Phone'],
	"x_city"						=> $_SESSION['Payer']['City'],
	"x_email"						=> $_SESSION['Payer']['Email']
);

$post_string = "";
foreach( $post_values as $key => $value )
	{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
$post_string = rtrim( $post_string, "& " );

$request = curl_init($post_url);
	curl_setopt($request, CURLOPT_HEADER, 0);
	curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
	curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
	$post_response = curl_exec($request);
curl_close ($request);


$response_json = explode($post_values["x_delim_char"], $post_response);

$_SESSION['payment'] = array('status' => $response_json[0], "message" => $response_json[3], "Payment_Through"=>'A');

if($response_json[0]=='1') {

	if(isset($_SESSION['payment_id'])) {
		$sql_qry = "UPDATE `payment` SET `Payment_Status`='S', `TransectionId`='".$response_json[6]."', `Json`='".escape_string(json_encode(array('response' => $response_json)), '', 'db')."' WHERE `Id`=".$_SESSION['payment_id'].";";
		mysql_query($sql_qry) or die(session_err('Database error:', mysql_error()));    
		$pymnt = get_record('`payment`', '`Type`', '`Id`='.$_SESSION['payment_id']);
		if($pymnt['Type'] == 'R') {
			$sql_qry = "UPDATE `students` SET `Reg_Status`='S', `Status`='A' WHERE `Id`='".$_SESSION['uid']."';";
			mysql_query($sql_qry) or die(session_err('Database error:', mysql_error()));

	    // include 'email-templates/email-registration-confirmation.php';
	    // include 'email-templates/email-registration-notification.php';

			$_SESSION['final_redirect'] = $_SESSION['redirect_to'];

			header("location: ./email-queues/regq-student.php");
			?>
			<script type="text/javascript">window.location = "./email-queues/regq-student.php"</script>
			<?php exit; 


		} else {

			$_SESSION['final_redirect'] = $_SESSION['redirect_to'];

			header("location: ./email-queues/bookingq-student.php");
			?>
			<script type="text/javascript">window.location = "./email-queues/bookingq-student.php"</script>
			<?php exit; 
			// $email_for = 'S'; include 'email-templates/email-booking-schedule.php';
			// $email_for = 'T'; include 'email-templates/email-booking-schedule.php';
			// $email_for = 'A'; include 'email-templates/email-booking-schedule.php';

		}
	}
} else {
	if(isset($_SESSION['payment_id'])) {
		$sql_qry = "UPDATE `payment` SET `Payment_Status`='F', `Json`='".escape_string(json_encode(array('response' => $response_json)), '', 'db')."', `Remark`='".escape_string($response_json[3])."' WHERE `Id`=".$_SESSION['payment_id'].";";
		mysql_query($sql_qry) or die(session_err('Database error:', mysql_error()));    
		$pymnt = get_record('`payment`', '`Type`', '`Id`='.$_SESSION['payment_id']);
		if($pymnt['Type'] == 'R') {
			$sql_qry = "UPDATE `students` SET `Reg_Status`='U' WHERE `Id`='".$_SESSION['uid']."';";
			mysql_query($sql_qry) or die(session_err('Database error:', mysql_error()));
		} else {
	    $pymnt_tmp = get_record('`payment`', '`Schedule_Id`', '`Id`='.$_SESSION['payment_id']);
	    $sql_qry = "UPDATE `teachers_availabilities` SET
	        `Is_Scheduled`='N' 
	      WHERE
	        `Id` in (SELECT `Teachers_Aval` FROM `students_schedules` WHERE `Id` = '".$pymnt_tmp['Schedule_Id']."')";
	    mysql_query($sql_qry);
		}
	}
}

header("location: ".$_SESSION['redirect_to']);
?>
<script type="text/javascript">window.location = "<?=$_SESSION['redirect_to']?>"</script>
<?php exit; ?>