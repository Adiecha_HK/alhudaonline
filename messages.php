<?php
$page_nav="Messages";
$page_title="Messages";
include 'header.php';
$id = $_SESSION['student_uniqueid'];
$type = 'S';
if($_SERVER['REQUEST_METHOD'] == "POST") {
  $pre_qry = "INSERT INTO message SET Sender_Type='".$type."', Sender_Id='".$id."'";
  $pst_qry = ", Title='".$_REQUEST['title']."', Type='".$_REQUEST['type']."', Message='".$_REQUEST['message']."',Last_Modified=NOW() ".", Entry_Date=NOW()";

  foreach ($_REQUEST['to'] as $toAll) {
    # code...
    $arr = explode("_", $toAll);
    $sql_qry = $pre_qry . ", Reciever_Type='" . $arr[0] . "', Reciever_Id='" . $arr[1] ."'" . $pst_qry;
    mysql_query($sql_qry) or die(session_err("Database error!", "Not sent ! " . " (" . mysql_error() .")"));
  }
  if(!isset($_SESSION['error'])) session_msg("Successful", "Message sent successfully");

  header("location: " . $_SERVER['PHP_SELF']);
  exit;
}

?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL.'dashboard.php'?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Messages</li>
    </ul>
</div>
<div class="row-fluid">
  <a href="#myModal" role="button" class="btn" data-toggle="modal">Compose Message</a>
</div>
<br/>
<div class="row-fluid">
  <ul class="nav nav-tabs" id="myTab">
    <li class="active"><a href="#inbox" data-toggle="tab">Inbox
        <?php $unread = get_msg_count($type, $id); $total = get_msg_count($type, $id, 'A'); ?>
        <?=($unread == 0 ? "(".$total.")":"(".$unread."/".$total.")")?>
    </a></li>
    <li><a href="#send" data-toggle="tab">Outbox (<?=get_msg_count($type, $id, 'A','O')?>)</a></li>
  </ul>

  <div class="tab-content">
    <div class="tab-pane active" id="inbox">
      <div class="offset3 span6">
        <?php
        $sql_qry = "SELECT Id,Title,Entry_Date,Type,Status FROM message WHERE Reciever_Id = '".$id."' AND Reciever_Type='".$type."' ORDER BY Status ASC, Entry_Date DESC"; 
        $sql_res=mysql_query($sql_qry) or die(error_mysql("Selecting Teachers"));
        $sql_nos=mysql_num_rows($sql_res);
        if($sql_nos > 0) { 
        ?>
        <ul class="nav nav-pills nav-stacked">
          <?php $types = array('Information', 'Re-Schedule', 'Query', 'Paymet'); while($sql_row = mysql_fetch_array($sql_res)) { ?>
          <li>
            <a href="message.php?box=I&id=<?=$sql_row['Id']?>">
              <?=$sql_row['Status'] == 'U' ? "&nbsp;<i class='icon-envelope'></i>&nbsp" : "&nbsp;<i class='icon-ok'></i>&nbsp;" ?>
              <?=($sql_row['Status'] == 'U'?"<strong>":"").$sql_row['Title'].($sql_row['Status'] == 'U'?"</strong>":"")?>&nbsp;<span class="label"><?=$types[$sql_row['Type']]?></span><span class="pull-right"><?=$sql_row['Entry_Date']?></span>
            </a>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </div>
    </div>
    <div class="tab-pane" id="send">
      <div class="offset3 span6">
        <?php
        $sql_qry = "SELECT Id,Title,Entry_Date,Type,Status FROM message WHERE Sender_Id = '".$id."' AND Sender_Type='".$type."' ORDER BY Status ASC, Entry_Date DESC"; 
        $sql_res=mysql_query($sql_qry) or die(error_mysql("Selecting Teachers"));
        $sql_nos=mysql_num_rows($sql_res);
        if($sql_nos > 0) { 
        ?>
        <ul class="nav nav-pills nav-stacked">
          <?php $types = array('Information', 'Re-Schedule', 'Query', 'Paymet'); while($sql_row = mysql_fetch_array($sql_res)) { ?>
          <li><a href="message.php?box=O&id=<?=$sql_row['Id']?>"><?=$sql_row['Title']?>&nbsp;<span class="label"><?=$types[$sql_row['Type']]?></span><span class="pull-right"><?=$sql_row['Entry_Date']?></span></a></li>
          <?php } ?>
        </ul>
        <?php } ?>
      </div>

    </div>
  </div>

</div>


<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="myModalLabel">Create your message</h3>
  </div>
  <form method="POST" class="form-horizontal">
  <div class="modal-body">
    <div class="control-group">
      <label class="control-label" for="to">To:</label>
      <div class="controls">
        <select name="to[]" class="select2" multiple="multiple" style="width: 220px">
          <?php $admins = get_records('adm_main','Id, Full_Name');
          foreach ($admins as $row) { ?>
            <option value="A_<?=$row['Id']?>"><?=$row['Full_Name']?> (Admin)</option>
          <?php } ?>
          <?php $teachers = get_records('teachers',"Id, CONCAT(Salutation,' ',First_Name,' ',Last_Name) AS Full_Name");
          foreach ($teachers as $row) { ?>
            <option value="T_<?=$row['Id']?>"><?=$row['Full_Name']?> (Teacher)</option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="type">Type:</label>
      <div class="controls">
        <select name="type" id="type">
          <?php foreach (array('Information', 'Re-Schedule', 'Query', 'Payment') as $key => $value) { ?>
            <option value="<?=$key?>"><?=$value?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="title">Title:</label>
      <div class="controls">
        <input type="text" name="title" id="title" placeholder="Enter title">
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="message">Message:</label>
      <div class="controls">
        <textarea name="message" id="message" placeholder="Write your message here" style="resize: none" rows="5"></textarea>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" type="submit">Send</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
  </form>
</div>


<?php include 'footer.php'; ?>
