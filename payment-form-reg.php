<?php
session_start();
$page_nav="Payment";
$page_title="Payment";
include 'header.php'; 
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL.'dashboard.php'?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Payment</li>
    </ul>
</div>
<form method="POST" class="form-horizontal" action="aim-payment.php">
  <legend>Payment details</legend>
  <div class="control-group">
    <label class="control-label" for="CCNumber">CC Number</label>
    <div class="controls">
      <input type="text" name="CCNumber" id="CCNumber" placeholder="Enter CC Number here">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="Expiery">Expiery date</label>
    <div class="controls">
      <input type="text" name="Expiery" id="Expiery" placeholder="Enter Expiery date here">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="CVV">CVV</label>
    <div class="controls">
      <input type="password" name="CVV" id="CVV" placeholder="Enter CVV here">
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <button type="submit" class="btn">Pay</button>
    </div>
  </div>
</form>
<?php include 'footer.php'; ?>