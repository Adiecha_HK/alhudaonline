<?php 
$page_nav="Courses";
$page_title="Course";
include 'header-adm.php'; 
$cid=isset($_REQUEST["cid"])?intval($_REQUEST["cid"]):0;

if($_SERVER['REQUEST_METHOD'] == "POST") {
  $myids=(isset($_REQUEST["Lessons"]))?implode(", ", $_REQUEST["Lessons"]):"";
  if($myids!=""){
    del_records('lessons', "Id IN (".$myids.")");
    if(!isset($_SESSION['error'])) session_msg("Successful", "Lesson(s) deleted successfully.");
  }
  header("location: " . $_SERVER['PHP_SELF']);
}


$sql_qry="SELECT Id,Lesson,Brief,Duration,order_no FROM lessons WHERE Course_Id = " . $cid . " ORDER BY order_no";
$sql_res=mysql_query($sql_qry) or die(error_mysql("Selecting Courses"));
$sql_nos=mysql_num_rows($sql_res);
$course = get_record('courses', 'Course', 'Id='.$cid);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'courses.php'?>">Courses</a> <span class="divider">/</span></li>
      <li class="active">Lessons of <strong><?=$course['Course']?></strong></li>
    </ul>
</div>
<!-- Breadcrum ends -->

<!-- List Courses >>> -->
<form method="POST">
<div id="listing_lesson" style="display:block;">
  <div class="row-fluid">
    <div class="span12">
      <table width="100%">  
        <tr>
          <td colspan="5" align="left" width="100%">
            <a href="lesson-form.php?cid=<?=$cid?>" class="AnchorButton btn">&nbsp;&nbsp;Add New Lesson&nbsp;&nbsp;</a>
            <?php if($sql_nos>0){ ?>
              <!-- <a class="AnchorButton btn btn-danger delete-action-js">&nbsp;&nbsp;Delete Selected Course(s)&nbsp;&nbsp;</a> -->
              <input type="submit" class="AnchorButton btn btn-danger" value="&nbsp;&nbsp;Delete Selected Lesson(s)&nbsp;&nbsp;" />
            <?php } ?>
          </td>
        </tr>
      </table>
    </div>
  </div><br/>
  <div class="row-fluid">
    <div class="span12">
      <table width="100%" class="DataTable table" >
        <tr class="DataTableHeading">
          <th width="5%" align="center"><input type="checkbox" id="toggleAll" /></th>
          <th width="5%" align="left">Order#</th>
          <th width="15%" align="left">Lesson</th>
          <th width="40%" align="left">Details</th>
          <th width="15%" align="center">Duration</th>
          <th width="10%" align="center">#</th>
          <th width="10%" align="center">#</th>
        </tr> 
      <?php
      if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
      ?>
        <tr class="DataTableRow">
          <td width="5%" align="center"><input class="lessonCheck" type="checkbox" name="Lessons[]" value="<?php echo $sql_row["Id"]; ?>" /></td>
          <td width="5%" align="left"><?php echo escape_string($sql_row["order_no"],"display"); ?></td>
          <td width="15%" align="left"><?php echo escape_string($sql_row["Lesson"],"display"); ?></td>
          <td width="40%" align="left"><?php echo escape_string($sql_row["Brief"],"display"); ?></td>
          <td width="15%" align="center"><?php echo escape_string($sql_row["Duration"],"display"); ?></td>
          <td width="10%" align="center"><a class="btn btn-info" href="lesson.php?cid=<?=$cid?>&lid=<?=$sql_row["Id"]?>" >View</a></td>
          <td width="10%" align="center"><a class="btn btn-info" href="lesson-form.php?cid=<?=$cid?>&lid=<?=$sql_row["Id"]?>">Edit</a></td>
        </tr>
      <?php
      }}else{
      ?>
        <tr>
          <td colspan="7" align="center">There is no lessons in database.</td>
        </tr>
      <?php 
      }
      ?>
      </table>
    </div>
  </div><br/>
  <div class="row-fluid">
    <div class="span12">
      <table width="100%">  
        <tr>
          <td colspan="5" align="left" width="100%">
            <a href="lesson-form.php?cid=<?=$cid?>" class="AnchorButton btn">&nbsp;&nbsp;Add New Lesson&nbsp;&nbsp;</a>
            <?php if($sql_nos>0){ ?>
              <input type="submit" class="AnchorButton btn btn-danger" value="&nbsp;&nbsp;Delete Selected Course(s)&nbsp;&nbsp;" />
            <?php } ?>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#toggleAll").checkAll(".lessonCheck");
  $("form").submit(function() {
    return confirm("Are you sure you want to remove "+ $(".lessonCheck:checked").size() + " lesson(s)?");
  })
</script>
<?php
include 'footer-adm.php'; 
?>