<?php 
$page_nav="Courses";
$page_title="Course - " . (isset($_REQUEST['cid']) ? "Edit": "Add");
include 'header-adm.php'; 
if($_SERVER['REQUEST_METHOD'] == "POST") {
  $myid = (isset($_REQUEST['cid']) ? $_REQUEST['cid']: 0);
  $sql_qry=($myid>0)?"UPDATE ":"INSERT INTO ";
  $sql_qry.="courses SET Course = '".escape_string($_REQUEST["coursename"])."',`Code` = '".escape_string($_REQUEST["courseCode"])."', Brief='".escape_string($_REQUEST["coursebrief"])."', Description='".escape_string($_REQUEST["coursedescription"])."', Duration='".$_REQUEST["courseduration"]."', Duration_Type='".strtoupper($_REQUEST["coursedurationtype"])."', amount='".$_REQUEST["courseamount"]."', mode='".strtoupper($_REQUEST["coursemode"])."', Course_Type='".strtoupper($_REQUEST["coursegroup"])."', order_no='".strtoupper($_REQUEST["courseorderno"])."'";
  $sql_qry.=($myid>0)?",Last_Modified=NOW() ":",Entry_Date=NOW() ";
  $sql_qry.=($myid>0)?"WHERE Id=".$myid:"";
  mysql_query($sql_qry) or die("error||".error_mysql("Saving a Course data."));
  echo "Added course successsfully.<br/><br/><br/><a href='courses.php'>Back</a>";
  header('location: courses.php');
  exit;
} else {
  $course = (isset($_REQUEST['cid']) ? get_record('courses', '', 'Id = '.$_REQUEST['cid']): null);
}
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'courses.php'?>">Courses</a> <span class="divider">/</span></li>
      <li class="active"><?=(isset($_REQUEST['cid']) ? "Edit ": "Add ")?>Course</li>
    </ul>
</div>
<!-- Breadcrum ends -->
<!-- Course form starts -->
<div>
  <form method="POST" class="form-horizontal">
     <fieldset>
      <legend><?=$page_title?></legend>
      <div class="control-group">
        <label class="control-label" for="courseorderno">Order No.</label>
        <div class="controls">
          <input name="courseorderno" type="text" id="courseorderno" placeholder="Enter course order number" value="<?=isset($lesson)?$lesson['order_no']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="courseName">Course Name</label>
        <div class="controls">
          <input name="coursename" type="text" id="courseName" placeholder="Enter course name" value="<?=isset($course)?$course['Course']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="courseCode">Course Code</label>
        <div class="controls">
          <input name="courseCode" type="text" id="courseCode" placeholder="Enter course code" value="<?=isset($course)?$course['Code']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="Details">Details</label>
        <div class="controls">
          <textarea name="coursebrief" id="Details" placeholder="Enter course details"><?=isset($course)?$course['Brief']:''?></textarea>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="cost">Cost per lesson</label>
        <div class="controls">
          <div class="input-prepend">
            <span class="add-on">$</span>
            <input class="span2" name="courseamount" type="text" id="cost" placeholder="Enter cost per lesson" value="<?=isset($course)?$course['amount']:''?>" data-validation="number">
          </div>
          <span class="help-inline"></span>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="duration">Duration</label>
        <div class="controls">
          <input name="courseduration" type="text" id="duration" placeholder="Duration (only numeric value)" value="<?=isset($course)?$course['Duration']:''?>" data-validation="number">
          <span class="help-inline"></span>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="durationType">Duration type</label>
        <div class="controls">
          <select name="coursedurationtype" id="durationType">
            <option value="D" <?=isset($course) && $course['Duration_Type'] == 'D'?'selected':''?>>Day(s)</option>
            <option value="W" <?=isset($course) && $course['Duration_Type'] == 'W'?'selected':''?>>Week(s)</option>
            <option value="M" <?=isset($course) && $course['Duration_Type'] == 'M'?'selected':''?>>Month(s)</option>
            <option value="Y" <?=isset($course) && $course['Duration_Type'] == 'Y'?'selected':''?>>Year(s)</option>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="mode">Course mode</label>
        <div class="controls">
          <select name="coursemode" id="mode">
            <option value="T" <?=isset($course) && $course['mode'] == 'T'?'selected':''?>>Tutor &amp; Audio</option>
            <option value="A" <?=isset($course) && $course['mode'] == 'A'?'selected':''?>>Audio</option>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="group">Course group</label>
        <div class="controls">
          <select name="coursegroup" id="group">
            <option value="TQ" <?=isset($course) && $course['Course_Type'] == 'TQ'?'selected':''?>>Tafheem al Quran Courses</option>
            <option value="TH" <?=isset($course) && $course['Course_Type'] == 'TH'?'selected':''?>>Tajweed &amp; Hifdh Courses</option>
            <option value="H" <?=isset($course) && $course['Course_Type'] == 'H'?'selected':''?>>Audio Hadith &amp; Tazkiyah Courses</option>
          </select>
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <input type="submit" class="btn btn-success" value="Save" />
          <input type="reset" class="btn btn-info" value="Reset" />
          <a href="courses.php" class="btn">Back</a>
        </div>
      </div>
    </fieldset>
  </form>
</div>
<!-- Course from ends -->
<script type="text/javascript" src="<?=JS.'/validate-form.js'?>"></script>
<?php 
include 'footer-adm.php'; 
?>