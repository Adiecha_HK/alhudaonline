<?php 
$page_nav="Courses";
$page_title="Course";
include 'header-adm.php'; 
if($_SERVER['REQUEST_METHOD'] == "POST") {
  $myids=(isset($_REQUEST["Courses"]))?implode(", ", $_REQUEST["Courses"]):"";
  if($myids!=""){
    del_records('lessons', "Course_Id IN (".$myids.")");
    del_records('courses', "Id IN (".$myids.")");
    if(!isset($_SESSION['error'])) session_msg("Successful", "Course(s) deleted successfully.");
  }
  header("location: " . $_SERVER['PHP_SELF']);
}

$sql_qry="SELECT
  `c`.`Id` as Id,
  `c`.`order_no` as order_no,
  `c`.`Course` as Course,
  `c`.`Brief` as Brief,
  `l`.`No_Of_Levels` as No_Of_Levels,
  (CONCAT(`c`.`Duration`,' ',IF(`c`.`Duration_Type`='D','Day(s)',IF(`c`.`Duration_Type`='W','Week(s)',IF(`c`.`Duration_Type`='M','Month(s)','Year(s)'))))) AS Duration,
  `s`.`Students` AS Students
FROM `courses` AS c LEFT JOIN
  (SELECT
    `Course_Id` AS cId,
    COUNT(`Id`) AS No_Of_Levels
FROM `lessons`
GROUP BY Course_Id) AS l
ON `l`.`cId`=`c`.`Id`
LEFT JOIN
  (SELECT
    `Course_Id` AS cId,
    COUNT(`Student_Id`) AS Students
  FROM `students_courses`
  GROUP BY Course_Id) AS s
ON `s`.`cId`=`c`.`Id`
ORDER BY `c`.`order_no`";
$sql_res=mysql_query($sql_qry) or die(error_mysql("Selecting Courses"));
$sql_nos=mysql_num_rows($sql_res);


?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Courses</li>
    </ul>
</div>
<!-- List Courses >>> -->
<form method="POST">
<div id="listing_course" style="display:block;">
  <div class="row-fluid">
    <div class="span12">
      <table width="100%">  
        <tr>
          <td colspan="5" align="left" width="100%">
            <a href="course-form.php" class="AnchorButton btn">&nbsp;&nbsp;Add New Course&nbsp;&nbsp;</a>
            <?php if($sql_nos>0){ ?>
              <!-- <a class="AnchorButton btn btn-danger delete-action-js">&nbsp;&nbsp;Delete Selected Course(s)&nbsp;&nbsp;</a> -->
              <input type="submit" class="AnchorButton btn btn-danger" value="&nbsp;&nbsp;Delete Selected Course(s)&nbsp;&nbsp;" />
            <?php } ?>
          </td>
        </tr>
      </table>
    </div>
  </div><br/>
  <div class="row-fluid">
    <div class="span12">
      <table width="100%" class="DataTable table" >
        <tr class="DataTableHeading">
          <th width="3%" align="center"><input type="checkbox" id="toggleAll" /></th>
          <th width="3%" align="left">Order#</th>
          <th width="17%" align="left">Course Name</th>
          <th width="40%" align="left">Details</th>
          <th width="10%" align="center">No. of Lessons</th>
          <th width="10%" align="center">Duration</th>
          <th width="5%" align="center">Registered Students</th>
          <th width="6%" align="center">#</th>
          <th width="6%" align="center">#</th>
        </tr> 
      <?php
      if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
      ?>
        <tr class="DataTableRow">
          <td width="3%" align="center"><input class="courseCheck" type="checkbox" name="Courses[]" value="<?php echo $sql_row["Id"]; ?>" /></td>
          <td width="3%" align="left"><?php echo escape_string($sql_row["order_no"],"display"); ?></td>
          <td width="17%" align="left"><?php echo escape_string($sql_row["Course"],"display"); ?></td>
          <td width="40%" align="left"><?php echo escape_string($sql_row["Brief"],"display"); ?></td>
          <td width="10%" align="center"><?php echo escape_string($sql_row["No_Of_Levels"],"display"); ?></td>
          <td width="10%" align="center"><?php echo escape_string($sql_row["Duration"],"display"); ?></td>
          <td width="5%" align="center"><?php echo escape_string($sql_row["Students"],"display"); ?></td>
          <td width="6%" align="center"><a class="btn btn-info" href="lessons.php?cid=<?php echo $sql_row["Id"]; ?>" >view</a></td>
          <td width="6%" align="center"><a class="btn btn-info" href="course-form.php?cid=<?php echo $sql_row["Id"]; ?>">edit</a></td>
        </tr>
      <?php
      }}else{
      ?>
        <tr>
          <td colspan="7" align="center">There is no Course in database.</td>
        </tr>
      <?php 
      }
      ?>
      </table>
    </div>
  </div><br/>
  <div class="row-fluid">
    <div class="span12">
      <table width="100%">  
        <tr>
          <td colspan="5" align="left" width="100%">
            <a href="course-form.php" class="AnchorButton btn">&nbsp;&nbsp;Add New Course&nbsp;&nbsp;</a>
            <?php if($sql_nos>0){ ?>
              <input type="submit" class="AnchorButton btn btn-danger" value="&nbsp;&nbsp;Delete Selected Course(s)&nbsp;&nbsp;" />
            <?php } ?>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#toggleAll").checkAll(".courseCheck");
  $("form").submit(function() {
    return confirm("Are you sure you want to remove "+ $(".courseCheck:checked").size() + " course(s)?");
  })
</script>
<!-- List Courses <<< -->
<?php 
include 'footer-adm.php'; 
?>