<?php 
$page_nav="Student";
$page_title="Student - " . (isset($_REQUEST['sid']) ? "Edit": "Add");
include 'header-adm.php'; 
if($_SERVER['REQUEST_METHOD'] == "POST") {
  $myid=(isset($_REQUEST["sid"])&&intval($_REQUEST["sid"])>0)?intval($_REQUEST["sid"]):0;
  $sql_qry=($myid>0)?"UPDATE ":"INSERT INTO "; /// 
  $sql_qry.="students SET  Salutation = '".escape_string($_REQUEST["salutation"])."', First_Name = '".escape_string($_REQUEST["firstname"])."', Middle_Name = '".escape_string($_REQUEST["middlename"])."', Last_Name = '".escape_string($_REQUEST["lastname"])."', Username = '".escape_string($_REQUEST["username"])."', Email = '".escape_string($_REQUEST["email"])."', Phone = '".escape_string($_REQUEST["phone"])."', City = '".escape_string($_REQUEST["city"])."', Country = '".escape_string($_REQUEST["country"])."'";
  $sql_qry.=($myid>0)?",Last_Modified=NOW() ":", Entry_Date=NOW() ";
  $sql_qry.=($myid>0)?"WHERE Id=".$myid:"";
  $tempact=($myid>0)?"updated":"added";
  mysql_query($sql_qry) or die(session_err("Database error!", "Studnet not " . $tempact . " (" . mysql_error() .")"));
  $myid = $myid>0?$myid:mysql_insert_id();
  mysql_query("DELETE FROM students_courses WHERE Student_Id = " . $myid);
  foreach ($_REQUEST['courses'] as $cid) {
    # code...
    mysql_query("INSERT INTO students_courses SET Course_Id='" . $cid . "', Student_Id = '" . $myid . "'");
  }
  if(!isset($_SESSION['error'])) session_msg("Successful", "Studnet ".$tempact." successfully");
  header('location: students.php');
} else {
  $student = (isset($_REQUEST['sid']) ? get_record('students', '', 'Id = '.$_REQUEST['sid']): null);
}
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'students.php'?>">Students</a> <span class="divider">/</span></li>
      <li class="active"><?=(isset($_REQUEST['sid']) ? "Edit ": "Add ")?>Student</li>
    </ul>
</div>
<!-- Breadcrum ends -->
<!-- Course form starts -->
<div>
  <form method="POST" class="form-horizontal">
     <fieldset>
      <legend><?=$page_title?></legend>
      <div class="control-group">
        <label class="control-label" for="salutation">Salutation</label>
        <div class="controls">
          <select name="salutation" id="salutation">
            <option value="Mr" <?=isset($student) && $student['Salutation'] == 'Mr'?'selected':''?>>Mr.</option>
            <option value="Ms" <?=isset($student) && $student['Salutation'] == 'Ms'?'selected':''?>>Ms.</option>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="firstname">First Name</label>
        <div class="controls">
          <input name="firstname" type="text" id="firstname" placeholder="Enter firstname" value="<?=isset($student)?$student['First_Name']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="middlename">Middle Name</label>
        <div class="controls">
          <input name="middlename" type="text" id="middlename" placeholder="Enter middlename" value="<?=isset($student)?$student['Middle_Name']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="lastname">Last Name</label>
        <div class="controls">
          <input name="lastname" type="text" id="lastname" placeholder="Enter lastname" value="<?=isset($student)?$student['Last_Name']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="username">Username</label>
        <div class="controls">
          <input name="username" type="text" id="username" placeholder="Enter username" value="<?=isset($student)?$student['Username']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="email">Email</label>
        <div class="controls">
          <input name="email" type="text" id="email" placeholder="Enter email id" value="<?=isset($student)?$student['Email']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="phone">Phone</label>
        <div class="controls">
          <input name="phone" type="text" id="phone" placeholder="Enter phone number" value="<?=isset($student)?$student['Phone']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="city">City</label>
        <div class="controls">
          <input name="city" type="text" id="city" placeholder="Enter city" value="<?=isset($student)?$student['City']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="country">Country</label>
        <div class="controls">
          <input name="country" type="text" id="country" placeholder="Enter country" value="<?=isset($student)?$student['Country']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="country">Associated Courses</label>
        <div class="controls">
          <select name="courses[]" class="select2" multiple="mutliple" style="width:220px">
            <?php $course_list = get_records('courses', 'Id, Course');
            $recs = isset($student)?get_records('students_courses', 'Course_Id', 'Student_Id = ' . $student['Id']):null;
            $courses = array();
            if(isset($recs)) foreach ($recs as $row) { array_push($courses, $row['Course_Id']); }

            foreach ($course_list as $row) { ?>
              <option value="<?=$row['Id']?>" <?=in_array($row['Id'], $courses)?"selected":""?>><?=$row['Course']?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <input type="submit" class="btn btn-success" value="Save" />
          <input type="reset" class="btn btn-info" value="Reset" />
          <a href="students.php" class="btn">Back to list</a>
        </div>
      </div>
    </fieldset>
  </form>
</div>
<script type="text/javascript">$(".select2").select2()</script>
<!-- Course from ends -->
<?php 
include 'footer-adm.php'; 
?>