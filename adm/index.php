<?php 
$page_nav="Dashboard";
$page_title="Dashboard";
include 'header-adm.php'; 
$num = 5;
?>
<div class="row-fluid">
	<div class="span12">
	<h2>Assalamualaikum, Welcome to Al-Huda Flexible Schedule Courses
	Barakallahu Fikum.</h2>
	</div>
</div>
<div class="row-fluid">
	<div class="span6">
		<div class="panel hide">
			<div class="panel-header">Basic information</div>
				<div class="panel-body">
				</div>
		</div>
	
  <?php
		$schedules = schedule("", "", "0,".$num, "S");
		$count=mysql_num_rows($schedules);
		?>
	<div class="panel">
		<div class="panel-header">
				Upcomming Schedules <span class="pull-right badge badge-success"><?=$count . " out of " . get_count('students_schedules','`Schedule_On` > NOW()')?></span>
		</div>
		<div class="panel-body">
				
				<?php if($count > 0) { ?>
				<ul>
					<?php while($sql_row=mysql_fetch_array($schedules)){ //echo var_dump($sql_row)?>
					  <?php if($iDate!=$sql_row["Schedule_Date"]) { $iDate = $sql_row['Schedule_Date'] ?>
					  <li>&nbsp;</li>
						<li class="date"><?=$sql_row['Schedule_Date']?></li>
						<?php } ?>
						<li><?="[".$sql_row['Schedule_Time']."] ".$sql_row['Lesson']." of ".$sql_row['Course']?></li>
					<?php } ?>
				</ul>
				<div class="panel-inner-body">
					<a href="schedules.php">More</a>
				</div>
				<?php } else { ?>
				<div class="panel-inner-body">No schedules found</div>
				<?php } ?>
			</div>
	</div>
	</div>
	
	<div class="span6">
		<div class="panel">
			<div class="panel-header">Messages</div>
			<div class="panel-body">
				<ul>
					<li>&nbsp;</li>
				<?php $msgs = get_records('message', '', '(Sender_Type=\'A\') OR (Reciever_Type=\'A\')');
				$no_msgs = true;
				foreach ($msgs as $key => $value) { $no_msgs = false;?>
					<?php if($value['Sender_Type'] == 'A'){ ?>
					<li title="Out going"><i class="icon-arrow-up"></i>&nbsp;<?=$value['Title']?><span class="pull-right right-away"><?=dashboard_formate($value['Entry_Date'])?></span></li>
					<?php } ?>
					<?php if($value['Reciever_Type'] == 'A') { ?>
					<li title="In coming"><i class="icon-arrow-down"></i>&nbsp;<?=$value['Title']?><span class="pull-right right-away"><?=dashboard_formate($value['Entry_Date'])?></span></li>
					<?php } ?>
				<?php } ?>
				</ul>
				<?php if($no_msgs) { ?>
				<div class="panel-inner-body">
					No messages found<br/><br/>
				</div>
				<?php } else { ?>
				<div class="panel-inner-body">
					<a href="messages.php">More</a>
				</div>
				<?php } ?>
			</div>
		</div>
<?php
		$schedules = schedule('', '', "0,".$num, "H");
		$count=mysql_num_rows($schedules);
		?>
		<div class="panel">
			<div class="panel-header">
				History lessons <span class="pull-right badge badge-success"><?=$count . " out of " . get_count('students_schedules','`Schedule_On` < NOW() AND `deleted_by_admin`=0')?></span>
			</div>
			<div class="panel-body">
				
				<?php if($count > 0){ ?>
				<ul>
					<?php while($sql_row=mysql_fetch_array($schedules)){ //echo var_dump($sql_row)?>
					  <?php if($iDate!=$sql_row["Schedule_Date"]) { $iDate = $sql_row['Schedule_Date'] ?>
					  <li>&nbsp;</li>
						<li class="date"><?=($sql_row['course_mode'] == "A" ? "Audio lessons": $sql_row['Schedule_Date'])?></li>
						<?php } ?>
						<li><?=($sql_row['course_mode'] != "A"? "[".$sql_row['Schedule_Time']."] ":"").$sql_row['Lesson']." of ".$sql_row['Course']?></li>
					<?php } ?>
				</ul>
				<div class="panel-inner-body">
					<a href='history.php'">More</a>
				</div>
				<?php } else { ?>
				<div class="panel-inner-body">No history found</div>
				<?php } ?>
			</div>
		</div>
	</div>
		
</div>
<?php 
include 'footer-adm.php'; 
?>