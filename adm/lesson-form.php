<?php 
$page_nav="Lessons";
$page_title="Lesson - " . (isset($_REQUEST['lid']) ? "Edit": "Add");
include 'header-adm.php'; 
$cid = $_REQUEST['cid'];
$course = get_record('courses', 'Course, amount', 'Id='.$cid);
if($_SERVER['REQUEST_METHOD'] == "POST") {
  $myid=(isset($_REQUEST["lid"])&&intval($_REQUEST["lid"])>0)?intval($_REQUEST["lid"]):0;
  
  // create random file name
  $file_name = $cid."_".time()."_".random_string(8);

  // check whether we found file 
  if(isset($_FILES['lessonaudio'])) {

    $file = $_FILES['lessonaudio'];
    $ext = strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
    
    if($ext === 'mp3') {

      $file_name.=".".$ext;
      //save file on disk
      if (!move_uploaded_file($file["tmp_name"], '../lesson_audio/' . $file_name)) {
        if($myid > 0) {
          $list = get_records('lessons', 'audio', 'Id = '.$myid);
          $a_link = $list[0]['audio'];
          if(file_exists('../lesson_audio/'.$a_link)) unlink('../lesson_audio/'.$a_link);
        }
      } else {
        $file_name = "";
      }
    }
    else
    {
      $file_name = "";
      // echo "file type not supported";
    }
  } else $file_name = "";

  $sql_qry=($myid>0)?"UPDATE ":"INSERT INTO "; /// 
  $sql_qry.="lessons SET Course_Id=".$cid.", Lesson = '".escape_string($_REQUEST["lessontitle"])."',Brief='".escape_string($_REQUEST["lessonbrief"])."',Duration='".escape_string($_REQUEST["lessonduration"])."',amount='".$_REQUEST["lessonamount"]."',audio='".$file_name."',link='".$_REQUEST['lessonlink']."',order_no='".$_REQUEST['lessonorderno']."'";
  $sql_qry.=($myid>0)?",Last_Modified=NOW() ":", Entry_Date=NOW() ";
  $sql_qry.=($myid>0)?"WHERE Id=".$myid:"";
  $tempact=($myid>0)?"updated":"added";
  mysql_query($sql_qry) or die(session_err("Database error!", "Not " . $tempact . " (" . mysql_error() .")"));
  if(!isset($_SESSION['error'])) session_msg("Successful", "Lesson ".$tempact." successfully");
  header('location: lessons.php?cid='.$cid);
} else {
  $lesson = (isset($_REQUEST['lid']) ? get_record('lessons', '', 'Id = '.$_REQUEST['lid']): null);
}
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'courses.php'?>">Courses</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'lessons.php?cid='.$cid?>">Lessons of <strong><?=$course['Course']?></strong></a> <span class="divider">/</span></li>
      <li class="active"><?=(isset($_REQUEST['lid']) ? "Edit ": "Add ")?>Lesson <strong><?=isset($lesson) ? " [".$lesson['Lesson']."]":""?></strong></li>
    </ul>
</div>
<!-- Breadcrum ends -->
<!-- Course form starts -->
<div>
  <form method="POST" class="form-horizontal">
     <fieldset>
      <legend><?=$page_title?></legend>
      <div class="control-group">
        <label class="control-label" for="lessonorderno">Order No.</label>
        <div class="controls">
          <input name="lessonorderno" type="text" id="lessonorderno" placeholder="Enter lesson order number" value="<?=isset($lesson)?$lesson['order_no']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="lessontitle">Lesson title</label>
        <div class="controls">
          <input name="lessontitle" type="text" id="lessontitle" placeholder="Enter lesson title" value="<?=isset($lesson)?$lesson['Lesson']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="details">Details</label>
        <div class="controls">
          <textarea name="lessonbrief" id="details" placeholder="Enter lesson details"><?=isset($lesson)?$lesson['Brief']:''?></textarea>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="duration">Duration</label>
        <div class="controls">
          <input name="lessonduration" type="text" id="duration" placeholder="Duration (only numeric value)" value="<?=isset($lesson)?$lesson['Duration']:''?>" data-validation="number">
          <span class="help-inline"></span>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="cost">Cost per lesson</label>
        <div class="controls">
          <div class="input-prepend">
            <span class="add-on">$</span>
            <input class="span2" name="lessonamount" type="text" id="cost" placeholder="Enter cost" value="<?=isset($lesson)?$lesson['amount']:$course['amount']?>" data-validation="number">
          </div>
          <span class="help-inline"></span>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="lessonaudio">Audio file</label>
        <div class="controls">
          <input name="lessonaudio" type="file" id="lessonaudio">
          <span class="help-inline"><?=isset($lesson)?$lesson['audio']:''?></span>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="lessonlink">Lesson link</label>
        <div class="controls">
          <input name="lessonlink" type="text" id="lessonlink" placeholder="Enter lesson link" value="<?=isset($lesson)?$lesson['link']:''?>">
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <input type="submit" class="btn btn-success" value="Save" />
          <input type="reset" class="btn btn-info" value="Reset" />
          <a href="lessons.php?cid=<?=$cid?>" class="btn">Back</a>
        </div>
      </div>
    </fieldset>
  </form>
</div>
<!-- Course from ends -->
<script type="text/javascript" src="<?=JS.'/validate-form.js'?>"></script>
<?php 
include 'footer-adm.php'; 
?>