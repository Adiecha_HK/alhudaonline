<?php
$page_nav="Teacher";
$page_title="Techer Details";
include 'header-adm.php'; 
$tid = $_REQUEST['tid'];
$teacher = get_record('teachers', '', 'Id='.$tid);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'teachers.php'?>">Teachers</a> <span class="divider">/</span></li>
      <li class="active">Detials of <strong><?=$teacher['Salutation'].". ".$teacher['First_Name']." ".$teacher['Last_Name']?></strong></li>
    </ul>
</div>
<!-- Breadcrum ends -->
<h1><?=$teacher['Salutation'].". ".$teacher['First_Name']." ".$teacher['Last_Name']?> (Teacher)</h1>
<br/>
<ul class="nav nav-tabs" id="myTab">
  <li class="active"><a href="#basic" data-toggle="tab">Basic Details</a></li>
  <li><a href="#acc" data-toggle="tab">Account Details</a></li>
  <li><a href="#sch" data-toggle="tab">Schedules</a></li>
  <li><a href="#hstr" data-toggle="tab">History</a></li>
  <li><a href="#msg" data-toggle="tab">Messages</a></li>
  <li><a href="#pay" data-toggle="tab">Payment</a></li>
</ul>
 
<div class="tab-content">
  <div class="tab-pane fade in active" id="basic">
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Fullname</strong></div>
      <div class="span6"><?=$teacher['Salutation'].". ".$teacher['First_Name']." ".$teacher['Middle_Name']." ".$teacher['Last_Name']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>City</strong></div>
      <div class="span6"><?=$teacher['City']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Country</strong></div>
      <div class="span6"><?=$teacher['Country']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Email</strong></div>
      <div class="span6"><?=$teacher['Email']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Phone</strong></div>
      <div class="span6"><?=$teacher['Phone']?></div>
    </div>
  </div>
  <div class="tab-pane fade in" id="acc">
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Username</strong></div>
      <div class="span6"><?=$teacher['Username']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Password</strong></div>
      <div class="span6"><i>-- As it set --</i></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Last login</strong></div>
      <div class="span6"><?=$teacher['Last_Loggedin'].(trim($teacher['IP'])!=""?" with IP ":"").$teacher['IP']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Status</strong></div>
      <div class="span6"><?=$teacher['Status'] == 'A' ? "Active": "Blocked"?></div>
    </div>

  </div>
  <div class="tab-pane fade in" id="sch">
    <?php
    $sql_res=teacher_schedule($teacher['Id']);
    $sql_nos=mysql_num_rows($sql_res);
    ?>

    <table class="DataTable table">
      <tr class="DataTableHeading">
        <th width="8%" align="left">Date</th>
        <th width="7%" align="center">Time</th>
        <th width="21%" align="left">Name of student</th>

        <th width="16%" align="left">Course</th>
        <th width="16%" align="left">Lesson#</th>

        <th width="16%" align="center">Duration</th>
        <th width="16%" align="center">Remark</th>
      </tr> 
      <?php
      if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
        if($iDate!=$sql_row["Schedule_Date"])
        {
      ?>
      <tr class="DataTableRow">
        <td align="left"><?php echo $sql_row["Schedule_Date"]; ?></td>
        <td align="center"></td>
        <td align="left"></td>
        <td align="center"></td>
        <td align="center"></td>
        <td align="center"></td>
        <td align="center"></td>
        <td align="center"></td>
      </tr>
      <?php
          $iDate=$sql_row["Schedule_Date"];   
        }
      ?>
      <tr class="DataTableRow">
        <td width="8%" align="left"></td>
        <td width="7%" align="center"><?php echo $sql_row["Schedule_Time"]; ?></td>
        <td width="21%" align="left"><?php echo $sql_row["Student"]; ?></td>
        <td width="16%" align="left"><?php echo $sql_row["Course"]; ?></td>
        <td width="16%" align="left"></td>
        <td width="16%" align="center"><?php echo $sql_row["Duration"]; ?></td>
        <td width="16%" align="center"></td>

      </tr>
      <?php
      }}else{
      ?>
      <tr>
        <td colspan="8" align="center">There is no Scheduled lession found.</td>
      </tr>
    <?php 
    }
    ?>
    </table>

  </div>
  <div class="tab-pane fade in" id="hstr">
    <?php
    $sql_res=teacher_history($teacher['Id']);
    $sql_nos=mysql_num_rows($sql_res);
    ?>
    <table class="DataTable table">
      <tr class="DataTableHeading">
        <th width="6%" align="left">Date</th>
        <th width="5%" align="center">Time</th>
        <th width="32%" align="left">Details</th>
        <th width="31%" align="left">Student Name</th>
        <th width="11%" align="center">Duration</th>
        <th width="15%" align="center">Status</th>
        <!-- <th width="31%" align="center">More</th> -->
      </tr> 
      <?php
      $iDate="";
      if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
        if($iDate!=$sql_row["Schedule_Date"])
        {
      ?>
      <tr class="DataTableRow">
        <td align="left"><?php echo $sql_row["Schedule_Date"]; ?></td>
        <td align="center"></td>
        <td align="left"></td>
        <td align="center"></td>
        <td align="center"></td>
        <td align="center"></td>
      </tr>
      <?php
          $iDate=$sql_row["Schedule_Date"];   
        }
      ?>
      <tr class="DataTableRow">
        <td align="left"></td>
        <td align="center"><?php echo $sql_row["Schedule_Time"]; ?></td>
        <td align="left">Lesson for <?php echo $sql_row["Course"]; ?> to <?php echo $sql_row["Student"]; ?></td>
        <td><?php echo $sql_row["Student"]; ?></td>
        <td align="center"><?php echo $sql_row["Duration"]; ?></td>
        <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
      </tr>
      <?php
      }}else{
      ?>
      <tr>
        <td colspan="7" align="center">There is no Scheduled lession found.</td>
      </tr>
      <?php 
      }
      ?>
    </table>

  </div>
  <div class="tab-pane fade in" id="msg">Remaining to implement</div>
  <div class="tab-pane fade in" id="pay">Remaining to implement</div>
</div>

<a href="teachers.php" class="btn">Back</a>
<a href="teacher-form.php?tid=<?=$teacher['Id']?>" class="btn btn-info">Edit details</a>
<?php
include 'footer-adm.php'; 
?>