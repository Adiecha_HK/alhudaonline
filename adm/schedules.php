<?php
$page_nav="Schedules";
$page_title="Schedules";
include 'header-adm.php'; 
$sql_qry="SELECT 
  `ss`.`Id` as Id, 
  `c`.`Course` as Course,
  `ss`.`Course_Id` as C_id,
  CONCAT(`s`.`Salutation`,' ', `s`.`First_Name`,' ', `s`.`Last_Name`) as Student,
  CONCAT(`t`.`Salutation`,' ', `t`.`First_Name`,' ', `t`.`Last_Name`) as Teacher,
  `ss`.`Teacher_Id` as T_id,
  `ss`.`Teacher_Reason` as Reason,
  IF(`ss`.`Schedule_On` > NOW(), 'upcoming', 'gone') as Curr_status,
  IF(`ss`.`Status` = 0, 'Initial', IF(`ss`.`Status` = 1, 'Accepted', IF(`ss`.`Status` = 2, 'Rejected', 'Unexpected'))) as Status,
  `ss`.`Schedule_On` as Schedule,
  DATE_FORMAT(ss.Schedule_On, '%d/%m/%Y') AS Schedule_Date, 
  DATE_FORMAT(ss.Schedule_On, '%H:%i') AS Schedule_Time, 
  CONCAT(`ss`.`Duration`, ' mins') as Duration
FROM
  students_schedules as ss
LEFT JOIN
  courses as c
ON
  `c`.`Id` = `ss`.`Course_Id`
LEFT JOIN
  students as s
ON
  `s`.`Id` = `ss`.`Student_Id`
LEFT JOIN
  teachers as t
ON
  `t`.`Id` = `ss`.`Teacher_Id`
WHERE
  `ss`.`Schedule_On` > NOW()
ORDER BY 
  `ss`.`Schedule_On` ASC";

$sql_res=mysql_query($sql_qry) or die(error_mysql("Selecting Courses"));
$sql_nos=mysql_num_rows($sql_res);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Schedule</li>
    </ul>
</div>
<div class="container">
  <div class="row-fluid">
    <div class="span12">
      <h1>Scheduled lessons</h1>
    </div>
  </div><br/>
  <div class="row-fluid">
    <div class="span12">
      <table class="DataTable table">
        <tr class="DataTableHeading">
          <th width="5%" align="left">Date</th>
          <th width="5%" align="center">Time</th>
          <th width="15%" align="left">Course</th>
          <th width="15%" align="left">Lesson</th>
          <th width="15%" align="center">Duration</th>
          <th width="15%" align="center">Teacher</th>
          <th width="15%" align="center">Student</th>
          <th width="15%" align="center">Status</th>
        </tr> 
        <?php
        if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
          if($iDate!=$sql_row["Schedule_Date"])
          {
        ?>
        <tr class="DataTableRow">
          <td align="left"><?php echo $sql_row["Schedule_Date"]; ?></td>
          <td align="center"></td>
          <td align="left"></td>
          <td align="center"></td>
          <td align="center"></td>
          <td align="center"></td>
          <td align="center"></td>
          <td align="center"></td>
        </tr>
        <?php
            $iDate=$sql_row["Schedule_Date"];   
          }
        ?>
        <tr class="DataTableRow">
          <td align="left"></td>
          <td align="center"><?php echo $sql_row["Schedule_Time"]; ?></td>
          <td align="left"><?php echo $sql_row["Course"]; ?></td>
          <td align="left"></td>
          <td align="center"><?php echo $sql_row["Duration"]; ?></td>
          <td align="center"><?php echo $sql_row["Teacher"]; ?></td>
          <td align="center"><?php echo $sql_row["Student"]; ?></td>
          <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
        </tr>
        <?php
        }}else{
        ?>
        <tr>
          <td colspan="7" align="center">There is no Scheduled lesson found.</td>
        </tr>
        <?php 
        }
        ?>
      </table>
    </div>
  </div><br/>
</div>
<?php include 'footer-adm.php'; ?>