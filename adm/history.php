<?php
$page_nav="History";
$page_title="History";
include 'header-adm.php'; 
// $sql_qry="SELECT 
//   `ss`.`Id` as Id, 
//   `c`.`Course` as Course,
//   `ss`.`Course_Id` as C_id,
//   CONCAT(`s`.`Salutation`,' ', `s`.`First_Name`,' ', `s`.`Last_Name`) as Student,
//   CONCAT(`t`.`Salutation`,' ', `t`.`First_Name`,' ', `t`.`Last_Name`) as Teacher,
//   `ss`.`Teacher_Id` as T_id,
//   `ss`.`Teacher_Reason` as Reason,
//   IF(`ss`.`Schedule_On` > NOW(), 'upcoming', 'gone') as Curr_status,
//   IF(`ss`.`Status` = 0, 'Initial', IF(`ss`.`Status` = 1, 'Accepted', IF(`ss`.`Status` = 2, 'Rejected', 'Unexpected'))) as Status,
//   `ss`.`Schedule_On` as Schedule,
//   DATE_FORMAT(ss.Schedule_On, '%d/%m/%Y') AS Schedule_Date, 
//   DATE_FORMAT(ss.Schedule_On, '%H:%i') AS Schedule_Time, 
//   CONCAT(`ss`.`Duration`, ' mins') as Duration
// FROM
//   students_schedules as ss
// LEFT JOIN
//   courses as c
// ON
//   `c`.`Id` = `ss`.`Course_Id`
// LEFT JOIN
//   students as s
// ON
//   `s`.`Id` = `ss`.`Student_Id`
// LEFT JOIN
//   teachers as t
// ON
//   `t`.`Id` = `ss`.`Teacher_Id`
// WHERE
//   `ss`.`Schedule_On` < NOW() 
// ORDER BY
//   `ss`.`Schedule_On` DESC";

// mysql_query($sql_qry) or die(error_mysql("Selecting Courses"));


if($_SERVER['REQUEST_METHOD'] == "POST") {
  $action = $_REQUEST['action'];
  if($action == "delete") {
    $sql_qry = "UPDATE students_schedules SET deleted_by_admin='1' WHERE Id in ('".implode("','",$_REQUEST['history'])."')";
    mysql_query($sql_qry) or die(session_err("Database error!", "Unable to delete (" . mysql_error() . ")"));
  }
  header("location: ".$_SERVER['PHP_SELF']);
  echo '<script type="text/javascript"> window.location = "'.$_SERVER['PHP_SELF'].'"; </script>';
}

$sql_res=admin_history();

$sql_nos=mysql_num_rows($sql_res);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li class="active">History</li>
    </ul>
</div>

<div class="container">
  <div class="row-fluid">
    <div class="span12">
      <h1>Full History</h1>
    </div>
  </div><br/>
  <div class="row-fluid">
    <div class="span12">
      <form method="POST">
        <input type="hidden" name="action" value="delete" />  
        <!-- List Courses >>> -->
        <table class="DataTable table">
          <tr class="DataTableHeading">
            <th width="5%" align="left"><?php if($sql_nos>0) { ?><input type="checkbox" id="toggleAll"><?php } ?></th>
            <th width="6%" align="left">Date</th>
            <th width="5%" align="center">Time</th>
            <th width="30%" align="left">Teacher</th>
            <th width="29%" align="left">Student</th>
            <th width="15%" align="center">Lession</th>
            <th width="15%" align="center">Status</th>
          </tr> 
          <?php
          if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
            if($sql_row["course_mode"] == 'A') {
          ?>
              <tr class="DataTableRow">
                <td align="left"><input type="checkbox" class="historyRecord" name="history[]" value="<?=$sql_row['Id']?>" /></td>
                <td align="left"><?php echo "- N/A -"; ?></td>
                <td align="center"><?php echo "- N/A -"; ?></td>
                <td align="left"><?php echo $sql_row["Course"]; ?></td>
                <td align="left"></td>
                <td align="left"><?php echo $sql_row["Teacher"]; ?></td>
                <td align="center"><?php echo $sql_row["Duration"]; ?></td>
                <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
                <td align="center">
                  <?php if(isset($sql_row["Rating"]) && $sql_row["Rating"] > 0) { ?>
                    <?=$feedbacks[$sql_row['Rating'] - 1]?>
                  <?php } else { ?>
                    <a class="btn btn-info" href="#myModal" role="button" data-toggle="modal" data-id="<?=$sql_row['Id']?>">Feedback</a>&nbsp;
                  <?php } ?>
                </td>
              </tr>
          <?php
            } else {
              if($iDate!=$sql_row["Schedule_Date"])
              {
          ?>
          <tr class="DataTableRow">
            <td align="left"></td>
            <td align="left"><?php echo $sql_row["Schedule_Date"]; ?></td>
            <td align="center"></td>
            <td align="left"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
          </tr>
          <?php
              $iDate=$sql_row["Schedule_Date"];   
            }
          ?>
          <tr class="DataTableRow">
            <td align="left"><input type="checkbox" class="historyRecord" name="history[]" value="<?=$sql_row['Id']?>" /></td>
            <td align="left"></td>
            <td align="center"><?php echo $sql_row["Schedule_Time"]; ?></td>
            <td align="left"><?php echo $sql_row["Teacher"]; ?></td>
            <td align="left"><?php echo $sql_row["Student"]; ?></td>
            <td align="left"><?php echo $sql_row["Course"]; ?></td>
            <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
          </tr>
          <?php
          }}}else{
          ?>
          <tr>
            <td colspan="7" align="center">There is no Scheduled lession found.</td>
          </tr>
          <?php 
          }
          ?>
        </table>
        <?php if($sql_nos>0) { ?><input class="btn btn-danger" type="submit" value="Delete selected records" /><?php } ?>
      </form>
    </div>
  </div><br/>

<script type="text/javascript">
  $(document).ready(function() {
    $("#toggleAll").checkAll(".historyRecord");
  });
</script>


<!-- List Courses <<< -->
</div>
<?php include 'footer-adm.php'; ?>