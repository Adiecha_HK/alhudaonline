<?php 
$page_nav="Teachers";
$page_title="Teachers";
include 'header-adm.php'; 
if($_SERVER['REQUEST_METHOD'] == "POST") {
  $myids=(isset($_REQUEST["Teachers"]))?implode(", ", $_REQUEST["Teachers"]):"";
  if($myids!=""){
    del_records('courses_assignments', "Teacher_Id IN (".$myids.")");
    del_records('teachers', "Id IN (".$myids.")");
    if(!isset($_SESSION['error'])) session_msg("Successful", "Teacher(s) deleted successfully.");
  }
  header("location: " . $_SERVER['PHP_SELF']);
  exit;
}

$sql_qry="SELECT Id,CONCAT(Salutation,' ',First_Name,' ',Last_Name) AS Full_Name,Username,Email, Phone, City, Country, IF(Status='A','Active','Blocked') AS Status, (SELECT COUNT(Id) FROM courses_assignments WHERE Teacher_Id=teachers.Id) AS In_Use FROM teachers ORDER BY Id DESC";
$sql_res=mysql_query($sql_qry) or die(error_mysql("Selecting Teachers"));
$sql_nos=mysql_num_rows($sql_res);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Teachers</li>
    </ul>
</div>
<!-- List Teachers >>> -->
<form method="POST">
  <div class="row-fluid">
    <div class="span12">
      <table width="100%">  
        <tr>
          <td colspan="5" align="left" width="100%">
            <a href="teacher-form.php" class="AnchorButton btn">&nbsp;&nbsp;Add New Teacher&nbsp;&nbsp;</a>
            <?php if($sql_nos>0){ ?>
            <button type="submit" class="AnchorButton btn btn-danger">&nbsp;&nbsp;Delete Selected Teacher(s)&nbsp;&nbsp;</button>
            <?php } ?>
          </td>
        </tr>
      </table>
    </div>
  </div><br/>
  <div class="row-fluid">
    <div class="span12">
      <table width="100%" class="DataTable table" >
        <tr class="DataTableHeading">

          <th width="3%" align="center"><input type="checkbox" id="AllTecher"/></th>
          <th width="13%">Name</th>
          <th width="12%">Phone</th>
          <th width="13%">Email</th>
          <th width="13%">City</th>
          <th width="12%">Country</th>
          <th width="12%">Courses Assigned</th>
          <th width="5%" align="center">#</th>
          <th width="5%" align="center">#</th>

        </tr> 
      <?php
      if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
      ?>
        <tr class="DataTableRow">
          <td width="3%" align="center">
            <?php if($sql_row["In_Use"]>0) echo "InUse"; else{ ?> 
              <input type="checkbox" name="Teachers[]" value="<?php echo $sql_row["Id"]; ?>" class="teacherCheck"/>
            <?php } ?>
          </td>
          <td width="13%" align="left"><?php echo escape_string($sql_row["Full_Name"],"display"); ?></td>
          <td width="12%"><?php echo escape_string($sql_row["Phone"],"display"); ?></td>
          <td width="13%"><?php echo escape_string($sql_row["Email"],"display"); ?></td>
          <td width="13%"><?php echo escape_string($sql_row["City"],"display"); ?></td>
          <td width="12%"><?php echo escape_string($sql_row["Country"],"display"); ?></td>
          <td width="12%"><?php echo escape_string($sql_row["In_Use"],"display"); ?></td>




          <td width="5%" align="center"><a class="btn btn-info" href="teacher.php?tid=<?php echo $sql_row["Id"]; ?>" >View</a></td>
          <td width="5%" align="center"><a class="btn btn-info" href="teacher-form.php?tid=<?=$sql_row['Id']?>">Edit</a></td>
        </tr>
      <?php
      }}else{
      ?>
        <tr>
          <td colspan="8" align="center">There is no Teacher in database.</td>
        </tr>
      <?php 
      }
      ?>
      </table>
    </div>
  </div><br/>
  <div class="row-fluid">
    <div class="span12">
      <table width="100%">  
        <tr>
          <td colspan="5" align="left" width="100%">
            <a href="teacher-form.php" class="AnchorButton btn">&nbsp;&nbsp;Add New Teacher&nbsp;&nbsp;</a>
            <?php if($sql_nos>0){ ?>
            <button type="submit" class="AnchorButton btn btn-danger">&nbsp;&nbsp;Delete Selected Teacher(s)&nbsp;&nbsp;</button>
            <?php } ?>
          </td>
        </tr>
      </table>
    </div>
  </div>
</form>
<script type="text/javascript">
  $("#AllTecher").checkAll(".teacherCheck");
</script>
<?php 
include 'footer-adm.php'; 
?>