<?php
include '../inc/config.php';
include '../inc/functions.php';

if(isset($_SESSION["anybody_loggedin"]) && $_SESSION["anybody_loggedin"]=="admin"){ 
	header("Location: index.php");
	?><script type="text/javascript">window.location = "index.php"</script><?php
	exit;
}

if(isset($_POST["Login_Me"])&&$_POST["Login_Me"]=="Login"){
	$compulsory_postvars=array("Username"=>"Unm","Password"=>"Pwd");
	$errors=array();
	check_compulsory_postvars();
	$res=mysql_query("SELECT Id, Full_Name FROM adm_main WHERE Username='".mysql_escape_string($_POST["Unm"])."' AND Password='".mysql_escape_string($_POST["Pwd"])."'") or die(error_mysql("Administrator Login"));
	if($row=mysql_fetch_array($res)){
		session_destroy();
		session_start();
		$_SESSION["anybody_loggedin"]="admin";
		$_SESSION["anybody_fullname"]=escape_string($row["Full_Name"]);
		$_SESSION["admin_id"]=escape_string($row["Id"]);
		header("Location: index.php");
		?><script type="text/javascript">window.location = "index.php"</script><?php
		exit;
	}else{
		$errors=array();
		array_push($errors,"Invalid Username / Password.");
	}
  $_SESSION['error'] = $errors;
  header('location: /admin-login/');
  ?><script type="text/javascript">window.location = "/admin-login/"</script><?php
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>index.css" />
<link href='http://fonts.googleapis.com/css?family=Dosis:200,400' rel='stylesheet' type='text/css'>
<style type="text/css">
	body {
		background-image: url('<?=IMG."heroshot.jpg"?>');
	}
	.cg-form-container legend h3{
		color: white;
		margin-left: 20px;
	}

	.cg-form-container a.link {
		color: cyan;
	}
	.cg-form-container {
		background-color: rgba(0,0,0,0.9);
		border: 10px solid rgba(255,255,255,0.5);
		border-radius: 15px;
		margin-top: 20%;
	}

	.cg-form-container input {
		max-width: 90%;
	}

</style>
<script language="javascript" src="<?php echo JS; ?>html5.js"></script>
<script language="javascript" src="<?php echo JS; ?>jquery-1.8.0.min.js"></script>
<script language="javascript" src="<?php echo JS; ?>bootstrap.js"></script>
<script language="javascript" src="<?php echo JS; ?>index.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Login</title>
</head>

</head>

<body class="homepage">
<form name="frm_login" id="frm_login" method="post" style="display: none;">
<table width="100%">
	<tr><th>Administrator Login</th></tr>
	<tr><td align="center"><?php show_errors('adm'); ?></td></tr>
	<tr><td align="center">Username: <input type="text" name="Unm" id="Unm" maxlength="30" /></td></tr>
	<tr><td align="center">Password: <input type="Password" name="Pwd" id="Pwd" maxlength="30" /></td></tr>
	<tr><td align="center"><input type="submit" name="Login_Me" id="Login_Me" value="Login" /></td></tr>
	<tr><td align="center" colspan="2">
		<!-- <a href="forget-pswd.php">Forget password</a>&nbsp; -->
		<a href="#">Forget password</a>&nbsp;
	    <a href="<?php echo URL; ?>">back to home</a>
	</td></tr>
</table>
</form>


<div class="row-fluid">
	<div class="offset3 span6">
		<div class="cg-form-container">
			<form role="form" class="form-horizontal"  name="frm_login" id="frm_login" method="post">
				<legend><h3>Sign In (Admin)</h3></legend>
	
				<?php show_errors('adm'); ?>

				<div class="control-group">
					<label class="control-label" for="Unm">Username</label>
					<div class="controls">
						<input type="text" name="Unm" id="Unm" maxlength="30" placeholder="Username"/>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="Pwd">Password</label>
					<div class="controls">
						<input type="Password" name="Pwd" id="Pwd" maxlength="30" placeholder="Password"/>
						<div><a href="#" class="link">forget password?</a></div>
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<input type="submit" name="Login_Me" class="btn btn-success" id="Login_Me" value="Login" />
						<!-- <button type="submit" class="btn btn-success">Sign in</button> -->
						<a class="btn" href="<?php echo URL; ?>">Back</a>&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="register.php" class="link">new user?</a>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>



</body>
</html>
