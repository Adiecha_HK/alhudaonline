<?php
$page_nav="Student";
$page_title="Student Details";
include 'header-adm.php'; 
$sid = $_REQUEST['sid'];
$student = get_record('students', '', 'Id='.$sid);
// echo var_dump($student);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'students.php'?>">Students</a> <span class="divider">/</span></li>
      <li class="active">Detials of <strong><?=$student['Salutation'].". ".$student['First_Name']." ".$student['Last_Name']?></strong></li>
    </ul>
</div>
<!-- Breadcrum ends -->
<h1><?=$student['Salutation'].". ".$student['First_Name']." ".$student['Last_Name']?> (Student)</h1>
<br/>
<ul class="nav nav-tabs" id="myTab">
  <li class="active"><a href="#basic" data-toggle="tab">Basic Details</a></li>
  <li><a href="#acc" data-toggle="tab">Account Details</a></li>
  <li><a href="#sch" data-toggle="tab">Schedules</a></li>
  <li><a href="#hstr" data-toggle="tab">History</a></li>
  <li><a href="#msg" data-toggle="tab">Messages</a></li>
  <li><a href="#pay" data-toggle="tab">Payment</a></li>
</ul>
 
<div class="tab-content">
  <div class="tab-pane fade in active" id="basic">
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Fullname</strong></div>
      <div class="span6"><?=$student['Salutation'].". ".$student['First_Name']." ".$student['Middle_Name']." ".$student['Last_Name']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>City</strong></div>
      <div class="span6"><?=$student['City']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Country</strong></div>
      <div class="span6"><?=$student['Country']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Email</strong></div>
      <div class="span6"><?=$student['Email']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Phone</strong></div>
      <div class="span6"><?=$student['Phone']?></div>
    </div>
  </div>
  <div class="tab-pane fade in" id="acc">
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Username</strong></div>
      <div class="span6"><?=$student['Username']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Password</strong></div>
      <div class="span6"><?=$student['Password']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Last login</strong></div>
      <div class="span6"><?=$student['Last_Loggedin'].(trim($student['IP'])!=""?" with IP ":"").$student['IP']?></div>
    </div>
    <div class="row-fluid">
      <div class="offset2 span2"><strong>Status</strong></div>
      <div class="span6"><?=$student['Status'] == 'A' ? "Active": "Blocked"?></div>
    </div>

    <div class="row-fluid">
      <div class="offset2 span2"><strong>Credit</strong></div>
      <div class="span6"><?=$student['Credits']?> USD</div>
    </div>

  </div>
  <div class="tab-pane fade in" id="sch">
    <?php
    $sql_res = student_schedule($student['Id']);
    $sql_nos = mysql_num_rows($sql_res);
    ?>

    <table class="DataTable table" >
      <tr class="DataTableHeading">
        <th width="6%" align="left">Date</th>
        <th width="5%" align="center">Time</th>
        <th width="12%" align="left">Course</th>
        <th width="11%" align="left">Lesson#</th>
        <th width="12%" align="left">Teacher</th>
        <th width="11%" align="center">Duration</th>
        <th width="11%" align="center">Status</th>
      </tr> 
      <?php
      $iDate="";
      if($sql_nos > 0){ while($sql_row = mysql_fetch_array($sql_res)) {
        if($iDate!=$sql_row["Schedule_Date"])
        {
      ?>
      <tr class="DataTableRow">
        <td align="left"><?php echo $sql_row["Schedule_Date"]; ?></td>
        <td align="center"></td>
        <td align="left"></td>
        <td align="left"></td>
        <td align="left"></td>
        <td align="center"></td>
        <td align="center"></td>
      </tr>
      <?php
          $iDate=$sql_row["Schedule_Date"];   
        }
      ?>
      <tr class="DataTableRow">
        <td align="left"></td>
        <td align="center"><?php echo $sql_row["Schedule_Time"]; ?></td>
        <td align="left"><?php echo $sql_row["Course"]; ?></td>
        <td align="left"></td>
        <td align="left"><?php echo $sql_row["Teacher"]; ?></td>
        <td align="center"><?php echo $sql_row["Duration"]; ?></td>
        <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
      </tr>
      <?php
      }}else{
      ?>
      <tr>
        <td colspan="7" align="center">There is no Scheduled lession found.</td>
      </tr>
      <?php 
      }
      ?>
    </table>

  </div>
  <div class="tab-pane fade in" id="hstr">
    <?php
    $sql_res=student_history($student['Id']);
    $sql_nos=mysql_num_rows($sql_res);
    ?>

    <table class="DataTable table">
      <tr class="DataTableHeading">
        <th width="6%" align="left">Date</th>
        <th width="5%" align="center">Time</th>
        <th width="14%" align="left">Course</th>
        <th width="14%" align="left">Lesson</th>
        <th width="14%" align="left">Teacher</th>
        <th width="14%" align="center">Duration</th>
        <th width="14%" align="center">Status</th>
      </tr> 
      <?php
      $iDate="";
      if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
        if($iDate!=$sql_row["Schedule_Date"])
        {
      ?>
      <tr class="DataTableRow">
        <td align="left"><?php echo $sql_row["Schedule_Date"]; ?></td>
        <td align="center"></td>
        <td align="left"></td>
        <td align="left"></td>
        <td align="left"></td>
        <td align="center"></td>
        <td align="center"></td>
      </tr>
      <?php
          $iDate=$sql_row["Schedule_Date"];   
        }
      ?>
      <tr class="DataTableRow">
        <td align="left"></td>
        <td align="center"><?php echo $sql_row["Schedule_Time"]; ?></td>
        <td align="left"><?php echo $sql_row["Course"]; ?></td>
        <td align="left"></td>
        <td align="left"><?php echo $sql_row["Teacher"]; ?></td>
        <td align="center"><?php echo $sql_row["Duration"]; ?></td>
        <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
      </tr>
      <?php
      }}else{
      ?>
      <tr>
        <td colspan="7" align="center">There is no Scheduled lession found.</td>
      </tr>
      <?php 
      }
      ?>
    </table>
  </div>
  <div class="tab-pane fade in" id="msg">Not implemented yet</div>
  <div class="tab-pane fade in" id="pay">Not implemented yet</div>
</div>

<a href="students.php" class="btn">Back</a>
<a href="student-form.php?sid=<?=$student['Id']?>" class="btn btn-info">Edit details</a>
<?php
include 'footer-adm.php'; 
?>