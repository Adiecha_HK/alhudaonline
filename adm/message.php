<?php
$page_nav="Messages";
$page_title="Messages";
include 'header-adm.php'; 
$box = $_REQUEST['box'];
$msg = get_record('message','','Id='.$_REQUEST['id']);
$type = $box == 'I' ? 'Sender_' : 'Reciever_';
$tbl  = $msg[$type."Type"] == 'S' ? "students": "teachers";
$person = get_record($tbl, '', 'Id='.$msg[$type.'Id']);

if($box == 'I' && $msg['Status'] == 'U') {
  $sql_qry = "UPDATE message SET Status='R' WHERE Id='".$msg['Id']."';";
  // echo $sql_qry;
  mysql_query($sql_qry) or die (session_err('Database error!', "Unable to change message to status 'unread' to 'read'. (". mysql_error() . ")"));
}
// include '../inc/MSG.php'; 
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'messages.php'?>">Messages</a> <span class="divider">/</span></li>
      <li class="active"><?=$box == 'I' ? "Inbox": "Outbox"?></li>
    </ul>
</div>
<h2><?=$msg['Title']?></h2>
<?php if($_REQUEST['box'] == 'I') { ?>
<div class="row-fluid">Sent from <strong><?=$person['Salutation'].' '.$person['First_Name'].' '.$person['Last_Name']?></strong>&nbsp;(<?=$msg['Entry_Date']?>)</div>
<?php } ?>
<?php if($_REQUEST['box'] == 'O') { ?>
<div class="row-fluid">Sent to <strong><?=$person['Salutation'].' '.$person['First_Name'].' '.$person['Last_Name']?></strong>&nbsp;(<?=$msg['Entry_Date']?>)
<?=$msg['Status'] == 'U' ? "&nbsp;[<i class='icon-envelope'></i>&nbsp;Unread]" : "&nbsp;[<i class='icon-ok'></i>&nbsp;Read]" ?>
</div>
<?php } ?>
<br/>
<div class="row-fluid">
  Message:
  <pre><?=$msg['Message']?></pre>
</div>
<br/>
<?php if($box == 'O') { ?>
<p><em>Message is <?=$msg['Status'] == 'U' ? "Unread" : "Read" ?></em></p>
<?php } ?>
<br/>
<a href="messages.php" class="btn">Back to all messages</a>
<!-- Button to trigger modal -->
<?php if($box == 'I') { ?>
<a href="#myModal" role="button" class="btn btn-primary" data-toggle="modal">Reply</a>

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="myModalLabel">Reply to <?=$person['Salutation'].' '.$person['First_Name'].' '.$person['Last_Name']?></h3>
  </div>
  <form method="POST" action="messages.php" class="form-horizontal">
    <input type="hidden" name="type" value="<?=$msg['Type']?>">
    <select style="display: none;" name="to[]" multiple="multiple"><option value="<?=$msg['Sender_Type'].'_'.$msg['Sender_Id']?>" selected="selected">slfj</option></select>
    <div class="modal-body">
      <div class="control-group">
        <label class="control-label" for="title">Title:</label>
        <div class="controls">
          <input type="text" name="title" id="title" placeholder="Enter title" value="Re: <?=$msg['Title']?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="message">Message:</label>
        <div class="controls">
          <textarea name="message" id="message" placeholder="Write your message here" style="resize: none" rows="5"></textarea>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      <button class="btn btn-primary">Send Reply</button>
    </div>
  </form>
</div>
<?php } ?>

<?php include 'footer-adm.php'; ?>