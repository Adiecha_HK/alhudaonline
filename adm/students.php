<?php 
$page_nav="Student";
$page_title="Students";
include 'header-adm.php'; 
if($_SERVER['REQUEST_METHOD'] == "POST") {
  $myids=(isset($_REQUEST["students"]))?implode(", ", $_REQUEST["students"]):"";
  if($myids!=""){
    $sql_qry = "DELETE FROM courses_assignments WHERE Teacher_Id IN (".$myids.")";
    mysql_query($sql_qry) or die(session_err('Database Error!', "Unable to delete teacher(s)'s association with course' (".mysql_error().")"));
    $sql_qry = "DELETE FROM teachers WHERE Id IN (".$myids.")";
    mysql_query($sql_qry) or die(session_err('Database Error!', "Unable to delete teacher(s) (".mysql_error().")"));
    if(!isset($_SESSION['error'])) session_msg("Successful", "Teacher(s) deleted successfully.");
  }
  header("location: " . $_SERVER['PHP_SELF']);
  exit;
}

$sql_qry="SELECT Id,CONCAT(Salutation,' ',First_Name,' ',Last_Name) AS Full_Name,Username,Phone,City,Country,Email,IF(Last_Loggedin IS NULL,'Never',DATE_FORMAT(Last_Loggedin,'%d-%m-%Y %H:%i:%s')) AS Last_Loggedin, IF(Status='A','Active','Blocked') AS Status, (SELECT COUNT(Id) FROM students_courses WHERE Student_Id=students.Id) AS In_Use FROM students ORDER BY Id DESC";
$sql_res=mysql_query($sql_qry) or die(error_mysql("Selecting Teachers"));
$sql_nos=mysql_num_rows($sql_res);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Students</li>
    </ul>
</div>
<!-- List Teachers >>> -->
<form method="POST">
  <div class="row-fluid">
    <div class="span12">
      <table width="100%">  
        <tr>
          <td colspan="5" align="left" width="100%">
            <a href="student-form.php" class="AnchorButton btn">&nbsp;&nbsp;Add New Student&nbsp;&nbsp;</a>
            <?php if($sql_nos>0){ ?>
            <button type="submit" class="AnchorButton btn btn-danger">&nbsp;&nbsp;Delete Selected Student(s)&nbsp;&nbsp;</button>
            <?php } ?>
          </td>
        </tr>
      </table>
    </div>
  </div><br/>
  <div class="row-fluid">
    <div class="span12">
      <table class="DataTable table">
        <tr class="DataTableHeading">
          <th width="4%" align="center"><input type="checkbox" name="CheckAll_Students" onclick="check_uncheck(this.form,this);" /></th>


          <th width="16%" align="left">Name</th>
          <th width="10%" align="left">Registration#</th>
          <th width="10%" align="left">Phone#</th>

          <th width="10%" align="left">Email</th>
          <th width="10%" align="left">City</th>
          <th width="10%" align="left">Country</th>

          <th width="10%" align="left">Courses</th>
          <th width="10%" align="left">Status</th>



          <th width="5%" align="center">#</th>
          <th width="5%" align="center">#</th>
        </tr> 
        <?php
        if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
        ?>
        <tr class="DataTableRow">
          <td width="4%" align="center">
            <?php //if($sql_row["In_Use"]>0) echo "InUse"; else{ ?> 
            <input type="checkbox" name="Students[]" value="<?php echo $sql_row["Id"]; ?>" onclick="check_uncheck(this.form,this);" />
            <?php //} ?>
          </td>


          <td width="14%" align="left"><?php echo escape_string($sql_row["Full_Name"],"display"); ?></td>
          <td width="10%" align="left"><?=$sql_row['Id']?></td>
          <td width="10%" align="left"><?=$sql_row['Phone']?></td>

          <td width="10%" align="left"><?=$sql_row['Email']?></td>
          <td width="10%" align="left"><?=$sql_row['City']?></td>
          <td width="10%" align="left"><?=$sql_row['Country']?></td>

          <td width="10%" align="left"><?=$sql_row['In_Use']?></td>
          <td width="10%" align="left"><?=$sql_row['Status']?></td>


          <td width="5%" align="center"><a class="btn btn-info" href="student.php?sid=<?=$sql_row['Id']?>" >view</a></td>
          <td width="5%" align="center"><a class="btn btn-info" href="student-form.php?sid=<?=$sql_row['Id']?>">edit</a></td>
        </tr>
        <?php
        }}else{
        ?>
        <tr>
          <td colspan="8" align="center">There is no Student in database.</td>
        </tr>
        <?php 
        }
        ?>
      </table>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <table width="100%">  
        <tr>
          <td colspan="5" align="left" width="100%">
            <a href="student-form.php" class="AnchorButton btn">&nbsp;&nbsp;Add New Student&nbsp;&nbsp;</a>
            <?php if($sql_nos>0){ ?>
            <button type="submit" class="AnchorButton btn btn-danger">&nbsp;&nbsp;Delete Selected Student(s)&nbsp;&nbsp;</button>
            <?php } ?>
          </td>
        </tr>
      </table>
    </div>
  </div><br/>
</form>
<script type="text/javascript">
  $("#AllTecher").checkAll(".teacherCheck");
</script>
<?php 
include 'footer-adm.php'; 
?>