<?php
$page_nav="Lesson";
$page_title="Lesson Details";
include 'header-adm.php'; 
$lid = $_REQUEST['lid'];
$lesson = get_record('lessons', '', 'Id='.$lid);
$cid = $lesson['Course_Id'];
$course = get_record('courses', 'Course', 'Id='.$cid);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'courses.php'?>">Courses</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'lessons.php?cid='.$cid?>">Lessons of <strong><?=$course['Course']?></strong></a> <span class="divider">/</span></li>
      <li class="active">Detials of Lesson <strong><?=$lesson['Lesson']?></strong></li>
    </ul>
</div>

<div class="row-fluid">
  <div class="span2 offset3"><strong>Lesson Name:</strong></div>
  <div class="span4"><?=$lesson['Lesson']?></div>
</div>
<div class="row-fluid">
  <div class="span2 offset3"><strong>Details:</strong></div>
  <div class="span4"><?=$lesson['Brief']?></div>
</div>
<div class="row-fluid">
  <div class="span2 offset3"><strong>Duration:</strong></div>
  <div class="span4"><?=$lesson['Duration']." min"?></div>
</div>
<div class="row-fluid">
  <div class="span2 offset3"><strong>Link:</strong></div>
  <div class="span4"><?=$lesson['link']?></div>
</div>
<div class="row-fluid">
  <div class="span2 offset3"><strong>Cost:</strong></div>
  <div class="span4"><?="$ ".$lesson['amount']?></div>
</div><div class="row-fluid">
  <div class="offset5">
    <a href="lesson-form.php?cid=<?=$cid?>&lid=<?=$lid?>" class="btn btn-info">Edit</a>
    <a href="lessons.php?cid=<?=$cid?>" class="btn ">Back</a>
    </div>
</div>
<?php include 'footer-adm.php' ?>