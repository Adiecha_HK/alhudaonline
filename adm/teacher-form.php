<?php 
$page_nav="Teacher";
$page_title="Teacher - " . (isset($_REQUEST['tid']) ? "Edit": "Add");
include 'header-adm.php'; 
if($_SERVER['REQUEST_METHOD'] == "POST") {
  $myid=(isset($_REQUEST["tid"])&&intval($_REQUEST["tid"])>0)?intval($_REQUEST["tid"]):0;
  $sql_qry=($myid>0)?"UPDATE ":"INSERT INTO "; /// 
  $sql_qry.="teachers SET  Salutation = '".escape_string($_REQUEST["salutation"])."', First_Name = '".escape_string($_REQUEST["firstname"])."', Middle_Name = '".escape_string($_REQUEST["middlename"])."', Last_Name = '".escape_string($_REQUEST["lastname"])."', Username = '".escape_string($_REQUEST["username"])."', Email = '".escape_string($_REQUEST["email"])."', Phone = '".escape_string($_REQUEST["phone"])."', City = '".escape_string($_REQUEST["city"])."', Country = '".escape_string($_REQUEST["country"])."'" . (isset($_REQUEST["password"]) && $_REQUEST["password"] != "" ? ", `Password`='".$_REQUEST["password"]."'": "");
  $sql_qry.=($myid>0)?",Last_Modified=NOW() ":", Entry_Date=NOW() ";
  $sql_qry.=($myid>0)?"WHERE Id=".$myid:"";
  $tempact=($myid>0)?"updated":"added";
  mysql_query($sql_qry) or die(session_err("Database error!", "Teacher not " . $tempact . " (" . mysql_error() .")"));
  if($myid>0) {
    $myid = $myid;
  } else {
    $myid = mysql_insert_id();
    $_SESSION['final_redirect'] = "adm/teachers.php";
    $_SESSION['teacher_id'] = $myid;
  } 
  mysql_query("DELETE FROM courses_assignments WHERE Teacher_Id = " . $myid);
  foreach ($_REQUEST['courses'] as $cid) {
    # code...
    mysql_query("INSERT INTO courses_assignments SET Course_Id='" . $cid . "', Teacher_Id = '" . $myid . "'");
  }
  if(!isset($_SESSION['error'])) session_msg("Successful", "Teacher ".$tempact." successfully");
  $navigate = isset($_SESSION['teacher_id']) ? "../email-queues/add-teacher-q.php": "teachers.php";
  header('location: '.$navigate);
  echo '<script type="text/javascript">window.location = "'.$navigate.'"; </script>';
  exit;
} else {
  $teacher = (isset($_REQUEST['tid']) ? get_record('teachers', '', 'Id = '.$_REQUEST['tid']): null);
}
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=URL_ADM?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=URL_ADM.'teachers.php'?>">Teachers</a> <span class="divider">/</span></li>
      <li class="active"><?=(isset($_REQUEST['tid']) ? "Edit ": "Add ")?>Teacher</li>
    </ul>
</div>
<!-- Breadcrum ends -->
<!-- Course form starts -->
<div>
  <form method="POST" class="form-horizontal" id="teacher-form">
     <fieldset>
      <legend><?=$page_title?></legend>
      <div class="control-group">
        <label class="control-label" for="salutation">Salutation</label>
        <div class="controls">
          <select name="salutation" id="salutation">
            <option value="Mr" <?=isset($teacher) && $teacher['Salutation'] == 'Mr'?'selected':''?>>Mr.</option>
            <option value="Ms" <?=isset($teacher) && $teacher['Salutation'] == 'Ms'?'selected':''?>>Ms.</option>
          </select>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="firstname">First Name</label>
        <div class="controls">
          <input name="firstname" type="text" id="firstname" placeholder="Enter firstname" value="<?=isset($teacher)?$teacher['First_Name']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="middlename">Middle Name</label>
        <div class="controls">
          <input name="middlename" type="text" id="middlename" placeholder="Enter middlename" value="<?=isset($teacher)?$teacher['Middle_Name']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="lastname">Last Name</label>
        <div class="controls">
          <input name="lastname" type="text" id="lastname" placeholder="Enter lastname" value="<?=isset($teacher)?$teacher['Last_Name']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="username">Username</label>
        <div class="controls">
          <input name="username" type="text" id="username" placeholder="Enter username" value="<?=isset($teacher)?$teacher['Username']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="password">Password</label>
        <div class="controls">
          <input name="password" type="password" id="password" placeholder="Enter password" value="">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="email">Email</label>
        <div class="controls">
          <input name="email" type="text" id="email" placeholder="Enter email id" value="<?=isset($teacher)?$teacher['Email']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="phone">Phone</label>
        <div class="controls">
          <input name="phone" type="text" id="phone" placeholder="Enter phone number" value="<?=isset($teacher)?$teacher['Phone']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="city">City</label>
        <div class="controls">
          <input name="city" type="text" id="city" placeholder="Enter city" value="<?=isset($teacher)?$teacher['City']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="country">Country</label>
        <div class="controls">
          <input name="country" type="text" id="country" placeholder="Enter country" value="<?=isset($teacher)?$teacher['Country']:''?>">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="country">Associated Courses</label>
        <div class="controls">
          <!-- <input name="country" type="text" id="country" placeholder="Enter country" value="<?=isset($teacher)?$teacher['Country']:''?>"> -->
          <select name="courses[]" class="select2" multiple="mutliple" style="width:220px">
            <?php $course_list = get_records('courses', 'Id, Course');
            $recs = isset($teacher)?get_records('courses_assignments', 'Course_Id', 'Teacher_Id = ' . $teacher['Id']):null;
            $courses = array();
            if(isset($recs)) foreach ($recs as $row) { array_push($courses, $row['Course_Id']); }

            foreach ($course_list as $row) { ?>
              <option value="<?=$row['Id']?>" <?=in_array($row['Id'], $courses)?"selected":""?>><?=$row['Course']?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <input type="submit" class="btn btn-success" value="Save" />
          <input type="reset" class="btn btn-info" value="Reset" />
          <a href="teachers.php" class="btn">Back to list</a>
        </div>
      </div>
    </fieldset>
  </form>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("#teacher-form").submit(function() {
      var len = $("#password").val().trim().length;
      return (len == 0 || (len > 0 && confirm("Teacher's password will be get replace, are you sure you want to procced?")));
    });
  });

</script>
<!-- Course from ends -->
<?php 
include 'footer-adm.php'; 
?>