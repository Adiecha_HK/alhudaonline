var url = "";

var getRespectiveJson = function(data, format) {
  var json = {};
  for(var key in format) {
    var val = format[key];
    json[key] = data[val];
  }
  return json;
}

var loadSelOpts = function(selecter, list, txt, json) {
  var ele = $(selecter);
  $(ele).empty().append($("<option/>", { value: 0, html: "-- Select "+txt+" --"}));
  if(list.length > 0) {
    $(ele).removeAttr('disabled');
    $(list).each(function(i, list_ele) {
      $(ele).append($("<option/>", getRespectiveJson(list_ele, json)));
    });
  }
}

var coursesChange = function() {
  var cid = parseInt($("#courses").val());
  if(cid > 0) {
    $.post(url, {'action': "course_load", 'course': cid}, function(data) {
      console.log(data);
      switch(data.course.mode) {
        case 'A':
          $(".tutor-fields").hide();
          break;
        case 'T':
          $(".tutor-fields").show();
          loadSelOpts("#teachers", data.teachers, "Teachers", {value: "Id", html: "Full_Name"});
          break;
        default:
          console.log("Unexpected part of execution, please show this message to site developer.");
          break;
      }
      loadSelOpts("#lessons", data.lessons, "Lesson", {value:"Id", html: "Lesson", amount: "amount"});
      amountChange();
    });
  } else {
    $("#teachers").attr("disabled", "disabled");
    $("#lessons").attr("disabled", "disabled");
  }
  $("#date").val("-- Select Date --");
}



var amountChange = function() {

  // get lesson charge
  var lesson_charge = $("option:selected", "#lessons").attr('amount');
  lesson_charge = isNaN(lesson_charge) ? 0: parseInt(lesson_charge);
  console.log("Lesson charge: " + lesson_charge);
  var discount = 0;
  if(window.org_credit > 0) {
    var crUse = $("#useCr").attr("checked") == undefined ? false: true;
    if(crUse) {
      discount = window.org_credit > lesson_charge ? lesson_charge: window.org_credit;
    }
  }
  var credit = window.org_credit - discount;
  var amount = lesson_charge - discount;
  /*
  $("#amount_txt").text(amount);
  var credit = parseInt($("#credit").val());
  var crUse = $("#useCr").attr("checked") == undefined ? false: true;
  var discount = parseInt($("#credit_used_txt").text());
  if(discount > 0) {
    amount -= discount;
  }
  if(crUse) {
    if(credit >= amount) {
      discount = amount;
    } else {
      discount = credit;
    }
    amount -= discount;
    credit -= discount;
  } else {
    credit += discount;
    amount += discount;
    discount = 0;
  }
  */
  $("#credit_used_txt").text(discount);
  $("#payble_amount").text(amount);
  $("#amount_txt").text(lesson_charge);
  $("#credit_txt strong").text(credit);
  $("#discount").val(discount);
  $("#amount").val(amount);
  $("#credit").val(credit);
}

var lessonsChange = function() {
  amountChange();
}

window.availableDates = ["9-5-2014","14-5-2014","15-5-2014"];

function available(date) {
  dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
  if ($.inArray(dmy, window.availableDates) != -1) {
    return [true, "","Available"];
  } else {
    return [false,"","unAvailable"];
  }
}

function teachersChange() {
  var tid = $("#teachers").val();
  $("#date").val("-- Select Date --");
  if(tid > 0) {
    $.post(url, {'action': "teacher_available_date", 'teacher': tid}, function(data) {
      window.availableDates = data.dates
      $("#date").removeAttr("disabled");
    });
  } else {
    $("#date").attr("disabled", "disabled");
    $("[name=hr]").attr('disabled',  "disabled");
    $("[name=min]").attr('disabled',  "disabled");
  }
}

function dateChange() {
  var dt = new Date($("#date").val());
  var tid = $("#teachers").val();

  $.post(url, {
    'action': "time_of_day",
    'teacher': tid,
    'date': dt.getFullYear() + "-" + (dt.getMonth()+1) + "-" + dt.getDate()
  }, function(data) {
    var hr = $("[name=hr]");
    $(hr).empty().append($("<option/>", {'html': "-- Hour --"}));
    $(hr).removeAttr("disabled");
    var min = $("[name=min]");
    $(min).removeAttr("disabled");
    $(min).empty().append($("<option/>", {'html': "-- Min --"}));
    for(var i in data.time) {
      var time = data.time[i];
      // console.log(time);
      var h = time['Hours'];
      var m = time['Minutes'];
      var id = time['Id'];
      var h_ele = $("option[value="+h+"]", hr);
      if($(h_ele).size() == 0) {
        h_ele = $("<option/>", {'value': h, 'html': h, 'mins': ""});
        $(hr).append(h_ele);
      }
      t_mins = $(h_ele).attr('mins');
      var minjson = {'minutes':[]};
      if(t_mins.length != 0) minjson = eval("("+t_mins+")");
      minjson.minutes.push({'min':m, 'id': id});
      t_mins = JSON.stringify(minjson);
      $(h_ele).attr('mins', t_mins);
    }
    console.log(data);
  });
}

function loadMins() {
  var hr = $("[name=hr] option:selected");
  var min = $("[name=min]");
  var mins = $(hr).attr("mins");
  $(min).empty().append($("<option/>", {html: "-- Min --"}));
  if(mins != undefined && mins.length > 0) {
    var json = eval("("+mins+")");
    for(var i in json.minutes) {
      var m = json.minutes[i];
      $(min).append($("<option/>", { value: m.min, html: m.min, aval_id:m.id}));
    }
  }
}

function minChange() {
  $("#aval_id").val($("[name=min] option:selected").attr("aval_id"));
}

function validate() {
  var validation = {
    'course': function(val) {
      return (isNaN(parseInt(val)) || parseInt(val) <= 0 ? "Invalid course": "");
    },
    'lesson': function(val) {
      return (isNaN(parseInt(val)) || parseInt(val) <= 0 ? "Invalid lesson": "");
    },
    'teacher': function(val) {
      return (isNaN(parseInt(val)) || parseInt(val) <= 0 ? "Invalid teacher": "");
    },
    'date': function(val) {
      if(val.trim().length == 0) {
        return "Date is mendatory";
      } 
      var dt = new Date(val);
      var cur = new Date();
      return (cur < dt? "": "Invalid date");
    },
    'hr': function(val) {
      return (isNaN(parseInt(val)) || parseInt(val) < 0 ? "Invalid hours": "");
    },
    'min': function(val) {
      return (isNaN(parseInt(val)) || parseInt(val) < 0 ? "Invalid minutes": "");
    }
  }
  var err = ($(":input", "#booking-form")).toArray().reduce(function(total, input, index) {
    var funct = $(input).attr('name');
    var str = "";
    if(validation.hasOwnProperty(funct) && $(input).parents('td').css('display') != "none") {
      str = validation[funct]($(input).val());
    }
    return total += (total == "" || str == ""? str: "\n" + str);
  }, "");
  if(err.length > 0) {
    alert(err.trim());
    return false;
  } else return true;

}

$(document).ready(function() {
  url = $("#ajax-url").attr('url');

  window.org_credit = parseInt($("#credit").val());

  $("#courses").on('change', coursesChange);
  $("#lessons").on('change', lessonsChange);
  if(window.org_credit > 0) {
    $("#useCr").on('change', amountChange);
  }
  $("#teachers").on('change', teachersChange);
  $('#date').on('change', dateChange);
  $('#date').datepicker({ beforeShowDay: available, dateFormat: 'yy-mm-dd'});  
  $("[name=hr]").on('change', loadMins);
  $("[name=min]").on('change', minChange);


  $("#booking-form").submit(validate);

  // accMgnt(parseInt($("#credit").val()));
  // $('#date').datepicker().datepicker( "show" );  
});