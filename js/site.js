function Trim(myval){
	return myval.split(" ").join(" ");
}

function CheckUncheckAll(frm,chk){
	if(chk.name.substr(0,8)!="CheckAll"){
		CheckName = chk.name;
		MyChecked = true;

		for(i=0;i<frm.elements.length;i++){
			if(frm.elements[i].name==CheckName && frm.elements[i].checked==false)			{
				MyChecked=false;
				break;
			}
		}

		CheckAll = eval("frm.CheckAll_"+CheckName.substr(0,CheckName.length-2));
		CheckAll.checked=MyChecked;
	}else{
		CheckName = chk.name.substr(9,chk.name.length)+"[]";
		
		if(chk.checked==true)
			AllChecked = true;
		else
			AllChecked = false;

		for(i=0;i<frm.elements.length;i++){
			if(frm.elements[i].name==CheckName)
				frm.elements[i].checked=AllChecked;		
		}
	}
}

function validateEmail(fld){
	var my=fld.value;
	var attherate=my.indexOf("@");
	var lastattherate = my.lastIndexOf("@")
	var dotpos=my.lastIndexOf(".");
	var posspace = my.indexOf(" ");
	var totallen = my.length;
	
	if (attherate<=0 || dotpos<=0 || attherate > dotpos || (dotpos-attherate)<=1 || (dotpos == totallen-1) || posspace > -1 || attherate!=lastattherate)
		return false;
	else
		return true;
}

function gotoPage(a,l,frm){
	frm = eval("document."+frm);
	frm.action=l;
	frm.cpage.value=a;
	frm.submit();
}

function gotoPageNo(pageNo){
	document.frm1.cpage.value = pageNo;
	document.frm1.submit();
}

var tooltip=function(){
	var id = 'tt';
	var top = 3;
	var left = 10;
	var maxw = 300;
	var speed = 70;
	var timer = 20;
	var endalpha = 80; //transparent background
	var alpha = 0;
	var tt,t,c,b,h;
	var ie = document.all ? true : false;
	return{
		show:function(v,w){
			if(tt == null){
				tt = document.createElement('div');
				tt.setAttribute('id',id);
				t = document.createElement('div');
				t.setAttribute('id',id + 'top');
				c = document.createElement('div');
				c.setAttribute('id',id + 'cont');
				b = document.createElement('div');
				b.setAttribute('id',id + 'bot');
				tt.appendChild(t);
				tt.appendChild(c);
				tt.appendChild(b);
				document.body.appendChild(tt);
				tt.style.opacity = 0;
				tt.style.filter = 'alpha(opacity=70)';
				document.onmousemove = this.pos;
			}
			tt.style.display = 'block';
			c.innerHTML = v;
			tt.style.width = w ? w + 'px' : 'auto';
			if(!w && ie){
				t.style.display = 'none';
				b.style.display = 'none';
				tt.style.width = tt.offsetWidth;
				t.style.display = 'block';
				b.style.display = 'block';
			}
			if(tt.offsetWidth > maxw){tt.style.width = maxw + 'px'}
			h = parseInt(tt.offsetHeight) + top;
			clearInterval(tt.timer);
			tt.timer = setInterval(function(){tooltip.fade(1)},timer);
		},
		pos:function(e){
			var u = ie ? event.clientY + document.documentElement.scrollTop : e.pageY;
			var l = ie ? event.clientX + document.documentElement.scrollLeft : e.pageX;
			tt.style.top = (u - h) + 'px';
			tt.style.left = (l + left) + 'px';
		},
		fade:function(d){
			var a = alpha;
			if((a != endalpha && d == 1) || (a != 0 && d == -1)){
				var i = speed;
				if(endalpha - a < speed && d == 1){
					i = endalpha - a;
				}else if(alpha < speed && d == -1){
					i = a;
				}
				alpha = a + (i * d);
				tt.style.opacity = alpha * .01;
				tt.style.filter = 'alpha(opacity=' + alpha + ')';
			}else{
				clearInterval(tt.timer);
				if(d == -1){tt.style.display = 'none'}
			}
		},
		hide:function(){
			clearInterval(tt.timer);
			tt.timer = setInterval(function(){tooltip.fade(-1)},timer);
		}
	};
}();

function validateImageFile(myimg){
	if(myimg.value!=""){
		var str=new String();
		str=myimg.value;
		var len1;
		len1=str.length;
		var f=str.substr(len1-3,len1);
		var f2=str.substr(len1-4,len1);
		if(!(f=="jpg" || f=="JPG" || f2=="JPEG" || f2=="jpeg" || f=="GIF" || f=="gif" || f=="PNG" || f=="png" )){
			alert("You must have to select only jpg, jpeg, gif and png files.")
			myimg.value = "";
			return false;
		}else{
			return true;
		}		
	}
}

function clearMe(obj, objvalue){
	if(obj.value == objvalue)
		obj.value = "";	
}

function setMe(obj, objvalue){
	if(Trim(obj.value) == "")
		obj.value = objvalue;
}

function GetXmlHttpObject(){
	 var xmlHttp=null;
	
	 try{
	 // Firefox, Opera 8.0+, Safari
		 xmlHttp=new XMLHttpRequest();
	 } 
	 catch (e){
	 // Internet Explorer
		 try{
			  xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		 }
		 catch (e){
			  xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		 }
	 }
	 return xmlHttp;
}

/* LightBox Popup Start */
Actele = "";
function showpopopdiv(ele){
	Actele = ele;
	$('#'+ele).center(ele);
	document.getElementById('BlankOut').style.display = "block";
	document.getElementById(ele).style.display = "block";
}

function hidepopopdiv(ele){
	if(ele == "TeacherPopup") isTeacherPopupOn = false;
	if(ele == "StudentPopup") isStudentPopupOn = false;
	document.getElementById('BlankOut').style.display = "none";
	document.getElementById(Actele).style.display = "none";
}

function blankoutsize(element){ 
	if(window.innerWidth){
		var width = window.innerWidth;
		var height = window.innerHeight;
	}else{
		var width = document.documentElement.clientWidth;
		var height = document.documentElement.clientHeight;
	}
	document.getElementById('BlankOut').style.width = width;
	document.getElementById('BlankOut').style.height = height;
}

/* this makes the resize script fire on page-scrolling */
if(document.attachEvent){
	window.attachEvent('onscroll',function(e){blankoutsize();});
}else if(document.addEventListener){
	window.addEventListener('scroll',function(e){blankoutsize();},false);
}else{
	window.onscroll = function(e){blankoutsize();};
}

/* this makes the resize script fire on resizing the page */
if(document.attachEvent){
	window.attachEvent('onresize',function(e){blankoutsize();});
}else if(document.addEventListener){
	window.addEventListener('resize',function(e){blankoutsize();},false);
}else{
	window.onresize = function(e){blankoutsize();};
}
	
jQuery.fn.center = function (ele) {
	this.css("position","absolute");
	this.css("top", ( $(window).height() - this.height() ) / 2+$(window).scrollTop() + 40 +"px");
	this.css("left", ( $(window).width() - this.width() ) / 2+$(window).scrollLeft() + "px");
	return this;
}

window.onresize = function(){  if(Actele) $('#'+Actele).center(); }
window.load = function(){ blankoutsize(); }
$(window).scroll(function() { if(Actele) $('#'+Actele).center(); });
/* LightBox Popup End */

function checkuserstatus(cntuser){
	tURL = "ajax/checkstatus.php?uid="+cntuser+"&random="+Math.random()*5;
	var xmlHttpcs; try{xmlHttpcs=new XMLHttpRequest();}catch(e){try{xmlHttpcs=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttpcs=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttpcs.onreadystatechange=function(){
	if(xmlHttpcs.readyState==4){
	if(xmlHttpcs.status == 200){
	}}}
	xmlHttpcs.open("GET",tURL,true);	
	xmlHttpcs.send(null);
}

function addtofav(cut_Rec,Member_Id,Teacher_Id){
	tURL = "ajax/ajax.php?act=addtofav&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&random="+Math.random()*5;
	var xmlHttp2; try{xmlHttp2=new XMLHttpRequest();}catch(e){try{xmlHttp2=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttp2=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttp2.onreadystatechange=function(){
	if(xmlHttp2.readyState==4){
	if(xmlHttp2.status == 200){
		if(Trim(xmlHttp2.responseText) == 'ADD'){ 
		  $("#hart_"+cut_Rec+" a").addClass("Active");
		}else{
		  $("#hart_"+cut_Rec+" a").removeClass("Active");
		}
	}}}
	xmlHttp2.open("GET",tURL,true);	
	xmlHttp2.send(null);
}

<!-- Teacher Class Functions Start -->

function selecttheclass(cut_Rec,Member_Id,Teacher_Id){
	tURL = "ajax/ajax.php?act=selecttheclass&cut_Rec="+cut_Rec+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&random="+Math.random()*5;
	var xmlHttpsc1; try{xmlHttpsc1=new XMLHttpRequest();}catch(e){try{xmlHttpsc1=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttpsc1=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttpsc1.onreadystatechange=function(){
	if(xmlHttpsc1.readyState==4){
	if(xmlHttpsc1.status == 200){
		if(Trim(xmlHttpsc1.responseText) != ""){ 
			$('#StudentSkillPopupResult').html(xmlHttpsc1.responseText);
			showpopopdiv('ClassSkillPopup');
		}
	}}}
	xmlHttpsc1.open("GET",tURL,true);	
	xmlHttpsc1.send(null);
}



function startaclass(cut_Rec,Member_Id,Teacher_Id){
	Subject_Id = $("input[name='Subject_Id']:checked").val();	
	if(Subject_Id == null){
		alert("Please select class.");
		document.getElementById("Subject_Id").focus();
		return false;
	}
	tURL = "ajax/ajax.php?act=startaclass&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&Subject_Id="+Subject_Id+"&random="+Math.random()*5;
	var xmlHttpsc; try{xmlHttpsc=new XMLHttpRequest();}catch(e){try{xmlHttpsc=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttpsc=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttpsc.onreadystatechange=function(){
	if(xmlHttpsc.readyState==4){
	if(xmlHttpsc.status == 200){
		if(Trim(xmlHttpsc.responseText) == 'ADD'){
		  hidepopopdiv('ClassSkillPopup');	
		  $("#ReqClassOn_"+cut_Rec).hide();
		  $("#ReqClassOff_"+cut_Rec).show();
		}
	}}}
	xmlHttpsc.open("GET",tURL,true);	
	xmlHttpsc.send(null);
}

isTeacherPopupOn = false
function teacherpopups(cntuser){
	if(!isTeacherPopupOn){
		tURL = "ajax/ajax.php?act=checkclassrequest&Teacher_Id="+cntuser+"&random="+Math.random()*5;
		var xmlHttptc1; try{xmlHttptc1=new XMLHttpRequest();}catch(e){try{xmlHttptc1=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc1=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
		xmlHttptc1.onreadystatechange=function(){
		if(xmlHttptc1.readyState==4){
		if(xmlHttptc1.status == 200){
			if(Trim(xmlHttptc1.responseText) != ''){
				$('#TeacherPopupResult').html(xmlHttptc1.responseText);
				showpopopdiv('TeacherPopup');
				isTeacherPopupOn = true;
			}
		}}}
		xmlHttptc1.open("GET",tURL,true);	
		xmlHttptc1.send(null);
	}
}

function scheduleback(cntuser){
	tURL = "ajax/ajax.php?act=checkclassrequest&Teacher_Id="+cntuser+"&random="+Math.random()*5;
	var xmlHttptc1; try{xmlHttptc1=new XMLHttpRequest();}catch(e){try{xmlHttptc1=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc1=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttptc1.onreadystatechange=function(){
	if(xmlHttptc1.readyState==4){
	if(xmlHttptc1.status == 200){
		if(Trim(xmlHttptc1.responseText) != ''){
			$('#TeacherPopupResult').html(xmlHttptc1.responseText);
			showpopopdiv('TeacherPopup');
			isTeacherPopupOn = true;
		}
	}}}
	xmlHttptc1.open("GET",tURL,true);	
	xmlHttptc1.send(null);
}


function acceptstudentrequest(Class_Id,Member_Id,Teacher_Id){
	if(isTeacherPopupOn){
		tURL = "ajax/ajax.php?act=acceptstudentrequest&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&random="+Math.random()*5;
		var xmlHttptc2; try{xmlHttptc2=new XMLHttpRequest();}catch(e){try{xmlHttptc2=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc2=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
		xmlHttptc2.onreadystatechange=function(){
		if(xmlHttptc2.readyState==4){
		if(xmlHttptc2.status == 200){
			if(Trim(xmlHttptc2.responseText) != ''){
				$('#TeacherPopupResult').html(xmlHttptc2.responseText);
			}
		}}}
		xmlHttptc2.open("GET",tURL,true);	
		xmlHttptc2.send(null);
	}
}

function startyourclass(Class_Id,Member_Id,Teacher_Id){
	nowminute = $("input[name='nowminute']:checked").val();	
	if(nowminute == null){
		alert("Please select class start option.");
		document.getElementById("nowminute").focus();
		return false;
	}
	if(isTeacherPopupOn){
		tURL = "ajax/ajax.php?act=startyourclass&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&nowminute="+nowminute+"&random="+Math.random()*5;
		var xmlHttptc3; try{xmlHttptc3=new XMLHttpRequest();}catch(e){try{xmlHttptc3=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc3=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
		xmlHttptc3.onreadystatechange=function(){
		if(xmlHttptc3.readyState==4){
		if(xmlHttptc3.status == 200){
			if(Trim(xmlHttptc3.responseText) != ''){
				$('#TeacherPopupResult').html(xmlHttptc3.responseText);
			}
		}}}
		xmlHttptc3.open("GET",tURL,true);	
		xmlHttptc3.send(null);
	}
}


function teacherschedule(Class_Id,Member_Id,Teacher_Id){
	if(isTeacherPopupOn){
		tURL = "ajax/ajax.php?act=teacherschedule&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&random="+Math.random()*5;
		var xmlHttptc4; try{xmlHttptc4=new XMLHttpRequest();}catch(e){try{xmlHttptc4=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc4=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
		xmlHttptc4.onreadystatechange=function(){
		if(xmlHttptc4.readyState==4){
		if(xmlHttptc4.status == 200){
			if(Trim(xmlHttptc4.responseText) != ''){
				$('#TeacherPopupResult').html(xmlHttptc4.responseText);
			}
		}}}
		xmlHttptc4.open("GET",tURL,true);	
		xmlHttptc4.send(null);
	}
}

function showMiniCalendar(Class_Id,Member_Id,Teacher_Id,date,action){
	tURL = "ajax/ajax.php?act=minicalendar&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&date="+date+"&action="+action+"&random="+Math.random()*5;
	var xmlHttptc5; try{xmlHttptc5=new XMLHttpRequest();}catch(e){try{xmlHttptc5=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc5=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttptc5.onreadystatechange=function(){
	if(xmlHttptc5.readyState==4){
	if(xmlHttptc5.status == 200){
		if(Trim(xmlHttptc5.responseText) != ''){
			$('#CurCalendar').html(xmlHttptc5.responseText);
		}
	}}}
	xmlHttptc5.open("GET",tURL,true);	
	xmlHttptc5.send(null);
}

function addscheduledate(Class_Id,Member_Id,Teacher_Id,date){
	tURL = "ajax/ajax.php?act=addscheduledate&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&date="+date+"&random="+Math.random()*5;
	var xmlHttptc6; try{xmlHttptc6=new XMLHttpRequest();}catch(e){try{xmlHttptc6=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc6=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttptc6.onreadystatechange=function(){
	if(xmlHttptc6.readyState==4){
	if(xmlHttptc6.status == 200){
		if(Trim(xmlHttptc6.responseText) != ''){
			$('#ScheduleDate').html(xmlHttptc6.responseText);
			showMiniCalendar(Class_Id,Member_Id,Teacher_Id,date,'current')
		}
	}}}
	xmlHttptc6.open("GET",tURL,true);	
	xmlHttptc6.send(null);
}

function deleteschedulerclass(Class_Id,Member_Id,Teacher_Id,date,Schedule_Id){
	tURL = "ajax/ajax.php?act=deleteschedulerclass&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&date="+date+"&Schedule_Id="+Schedule_Id+"&random="+Math.random()*5;
	var xmlHttptc7; try{xmlHttptc7=new XMLHttpRequest();}catch(e){try{xmlHttptc7=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc7=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttptc7.onreadystatechange=function(){
	if(xmlHttptc7.readyState==4){
	if(xmlHttptc7.status == 200){
		if(Trim(xmlHttptc7.responseText) != ''){
			$('#ScheduleDate').html(xmlHttptc7.responseText);
			showMiniCalendar(Class_Id,Member_Id,Teacher_Id,date,'current')
		}
	}}}
	xmlHttptc7.open("GET",tURL,true);	
	xmlHttptc7.send(null);
}

function schedulerclasstime(Class_Id,Member_Id,Teacher_Id,date){
	tURL = "ajax/ajax.php?act=schedulerclasstime&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&date="+date+"&random="+Math.random()*5;
	var xmlHttptc8; try{xmlHttptc8=new XMLHttpRequest();}catch(e){try{xmlHttptc8=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc8=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttptc8.onreadystatechange=function(){
	if(xmlHttptc8.readyState==4){
	if(xmlHttptc8.status == 200){
		if(Trim(xmlHttptc8.responseText) != ''){
			$('#TeacherPopupResult').html(xmlHttptc8.responseText);
		}
	}}}
	xmlHttptc8.open("GET",tURL,true);	
	xmlHttptc8.send(null);
}

function deleteschedulerclassdetail(Class_Id,Member_Id,Teacher_Id,date,Schedule_Id){
	tURL = "ajax/ajax.php?act=deleteschedulerclassdetail&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&date="+date+"&Schedule_Id="+Schedule_Id+"&random="+Math.random()*5;
	var xmlHttptc9; try{xmlHttptc9=new XMLHttpRequest();}catch(e){try{xmlHttptc9=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc9=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttptc9.onreadystatechange=function(){
	if(xmlHttptc9.readyState==4){
	if(xmlHttptc9.status == 200){
		if(Trim(xmlHttptc9.responseText) == ''){
			schedulerclasstime(Class_Id,Member_Id,Teacher_Id,date);
		}
	}}}
	xmlHttptc9.open("GET",tURL,true);	
	xmlHttptc9.send(null);
}

function schedulerclasstimeadd(Class_Id,Member_Id,Teacher_Id){
	totalschedule = parseInt($('#totalschedule').val());
	if(totalschedule > 0){
		for(i=1;i<=totalschedule;i++){
			if(document.getElementById('F'+i).options[0].selected == true || document.getElementById('S'+i).options[0].selected == true || document.getElementById('T'+i).options[0].selected == true){
				alert("Please select time proposals for each day.");
				return false;
			}
		}
		tURL = "ajax/ajax.php?act=schedulerclasstimeadd&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id;
		tURL = tURL + "&totalschedule="+totalschedule;
		for(i=1;i<=totalschedule;i++){
			tURL = tURL + "&Schedule_Id"+i+"="+$('#Schedule_Id'+i).val();
			tURL = tURL + "&F"+i+"="+$('#F'+i).val();
			tURL = tURL + "&S"+i+"="+$('#S'+i).val();
			tURL = tURL + "&T"+i+"="+$('#T'+i).val();
		}
		tURL = tURL + "&random="+Math.random()*5;
		var xmlHttptc10; try{xmlHttptc10=new XMLHttpRequest();}catch(e){try{xmlHttptc10=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc10=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
		xmlHttptc10.onreadystatechange=function(){
		if(xmlHttptc10.readyState==4){
		if(xmlHttptc10.status == 200){
			if(Trim(xmlHttptc10.responseText) != ''){
				$('#TeacherPopupResult').html(xmlHttptc10.responseText);
			}
		}}}
		xmlHttptc10.open("GET",tURL,true);	
		xmlHttptc10.send(null);
	}
}

function rejectstudentrequest(Class_Id,Member_Id,Teacher_Id){
	tURL = "ajax/ajax.php?act=rejectstudentrequest&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&random="+Math.random()*5;
	var xmlHttptc11; try{xmlHttptc11=new XMLHttpRequest();}catch(e){try{xmlHttptc11=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttptc11=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttptc11.onreadystatechange=function(){
	if(xmlHttptc11.readyState==4){
	if(xmlHttptc11.status == 200){
		if(Trim(xmlHttptc11.responseText) == ''){
			hidepopopdiv('TeacherPopup');
		}
	}}}
	xmlHttptc11.open("GET",tURL,true);	
	xmlHttptc11.send(null);
}


<!-- Teacher Class Functions End -->

<!-- Student Class Functions Start -->

isStudentPopupOn = false;
function studentpopups(cntuser){
	if(!isStudentPopupOn){
		tURL = "ajax/ajax.php?act=checkclassresponse&Member_Id="+cntuser+"&random="+Math.random()*5;
		var xmlHttpsc1; try{xmlHttpsc1=new XMLHttpRequest();}catch(e){try{xmlHttpsc1=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttpsc1=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
		xmlHttpsc1.onreadystatechange=function(){
		if(xmlHttpsc1.readyState==4){
		if(xmlHttpsc1.status == 200){
			if(Trim(xmlHttpsc1.responseText) != ''){
				$('#StudentPopupResult').html(xmlHttpsc1.responseText);
				showpopopdiv('StudentPopup');
				isStudentPopupOn = true;
			}
		}}}
		xmlHttpsc1.open("GET",tURL,true);	
		xmlHttpsc1.send(null);
	}
}

function studentacceptedclass(Class_Id,Member_Id,Teacher_Id,accepttyle){
	if(isStudentPopupOn){
		tURL = "ajax/ajax.php?act=studentacceptedclass&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id;
		if(accepttyle == "withschedule"){
			if($("input[name=shtime]:checked").val() == ""){
				alert("Please select date and time convenient for you.");
				return false;
			}
		}
		tURL = tURL + "&shtime="+$("input[name=shtime]:checked").val();
		tURL = tURL + "&scomment="+$("#scomment").val();
		tURL = tURL + "&random="+Math.random()*5;
		var xmlHttpsc2; try{xmlHttpsc2=new XMLHttpRequest();}catch(e){try{xmlHttpsc2=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttpsc2=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
		xmlHttpsc2.onreadystatechange=function(){
		if(xmlHttpsc2.readyState==4){
		if(xmlHttpsc2.status == 200){
			if(Trim(xmlHttpsc2.responseText) == ''){
				hidepopopdiv('StudentPopup');
				window.location = "myaccount_class_feedback.php?cid="+Class_Id;
			}
		}}}
		xmlHttpsc2.open("GET",tURL,true);	
		xmlHttpsc2.send(null);
	}
}

function studentrejectedclass(Class_Id,Member_Id,Teacher_Id){
	tURL = "ajax/ajax.php?act=studentrejectedclass&Class_Id="+Class_Id+"&Member_Id="+Member_Id+"&Teacher_Id="+Teacher_Id+"&random="+Math.random()*5;
	var xmlHttpsc3; try{xmlHttpsc3=new XMLHttpRequest();}catch(e){try{xmlHttpsc3=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlHttpsc3=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){alert("Your browser does not support AJAX!");return false;}}}
	xmlHttpsc3.onreadystatechange=function(){
	if(xmlHttpsc3.readyState==4){
	if(xmlHttpsc3.status == 200){
		if(Trim(xmlHttpsc3.responseText) == ''){
			hidepopopdiv('StudentPopup');
		}
	}}}
	xmlHttpsc3.open("GET",tURL,true);	
	xmlHttpsc3.send(null);
}


<!-- Student Class Functions End -->