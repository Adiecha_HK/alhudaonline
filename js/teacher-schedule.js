$(document).ready(function() {
	$("[rejectId]").on('click', function(e) {
		e.preventDefault();
		var id = $(this).attr('rejectId');
		var reason = prompt("Reason for rejection:");
		var form = $("#rejection-form");
		$("[name=id]", form).val(id);
		$("[name=message]", form).val(reason);
		$(form).submit();
	});
});