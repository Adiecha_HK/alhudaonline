$.fn.markAsInvalid = function(message) {
  var control = $(this).parents('.control-group');
  $(control).addClass('error');
  $("span.help-inline", $(control)).html(message);
}

$.fn.markAsValid = function() {
  var control = $(this).parents('.control-group');
  $(control).removeClass('error');
  $("span.help-inline", $(control)).empty();
}

$(document).ready(function() {
  $(":input[data-validation]").each(function(index, input) {
    $(input).on('change', function() {
      var validation  = $(this).data('validation');
      switch(validation) {
        case 'number':
          if(isNaN($(this).val())) {
            $(this).markAsInvalid("Only numbers will allowed.");
          } else {
            $(this).markAsValid();
          }
          break;
      }
    });
  });

  $("form").submit(function() {
    if($(".error", $(this)).size() == 0) return true;
    alert("Please resolve mistaken inputs");
    return false;
  })
});