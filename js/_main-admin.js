var glb_frm_flds=new Array();
var glb_db_flds=new Array();
function check_uncheck(frm,chk){
	if(chk.name.substr(0,8)!="CheckAll"){
		CheckName=chk.name;
		MyChecked=true;
		for(i=0;i<frm.elements.length;i++){
			if(frm.elements[i].name==CheckName&&frm.elements[i].checked==false)			{
				MyChecked=false;
				break;
			}
		}
		CheckAll=eval("frm.CheckAll_"+CheckName.substr(0,CheckName.length-2));
		CheckAll.checked=MyChecked;
	}else{
		CheckName=chk.name.substr(9,chk.name.length)+"[]";
		if(chk.checked==true) AllChecked = true;
		else AllChecked = false;
		for(i=0;i<frm.elements.length;i++){
			if(frm.elements[i].name==CheckName)
				frm.elements[i].checked=AllChecked;		
		}
	}
}
function validate_email(fld){
	var my=fld.value;
	var attherate=my.indexOf("@");
	var lastattherate=my.lastIndexOf("@")
	var dotpos=my.lastIndexOf(".");
	var posspace=my.indexOf(" ");
	var totallen=my.length;
	if (attherate<=0||dotpos<=0||attherate>dotpos||(dotpos-attherate)<=1||(dotpos==totallen-1)||posspace>-1||attherate!=lastattherate) return false;
	else return true;
}
var xml_http=get_xml();
function get_xml(){
	 var xmlHttp=null;
	 try{
		 // Firefox, Opera 8.0+, Safari
		 xmlHttp=new XMLHttpRequest();
	 }catch(e){
	 	// Internet Explorer
		try{ xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); }
		catch(e){ xmlHttp=new ActiveXObject("Microsoft.XMLHTTP"); }
	 }
	 return xmlHttp;
}
function onready_state_change(){
	if(xml_http.readyState==4){
		if(xml_http.status==200){//OK
			var result=xml_http.responseText.split("||");
			if(result.length>1){ 
				$('#action_message').css('display','block'); 
				$('#action_message').html(result[1]); 
			}
			else alert(xml_http.responseText);
			if(result[0]=="success"){
				setTimeout(function(){window.location.reload();},2000);
			}
		}
	}
}
function clear_form(frm){
	if(typeof frm!=="object") frm=eval(frm);
	frm.reset();
}
function view_single_record(type,act,flg,id){ 
	if(act=="hide"){
		hideshow_addedit_form('hide');	
		$('#a_'+type).hide(1000);	
		$('#listing_'+type).show(1000);
		$('#action_message').html('');
		$('#action_message').css('display','none');
	}else{
		if(flg=="add") clear_form('document.frm_'+type);
		else if(flg=="edit"){ var myfld=eval('document.frm_'+type+'.myid'); myfld.value=id; }
		$('#listing_'+type).hide(1000);	
		$('#a_'+type).show(1000);	
	}
}
function save_record(type,frm,url){
	$('#addedit_formerror #error_message').html('');
	frm.act.value="save";	
	var compulsory_fldnames=new Array();
	var compulsory_fldvals=new Array();
	var errors="";
	if(type=="course"){
		var compulsory_fldnames=new Array('coursename','coursebrief','courselevels','courseduration');
		var compulsory_fldvals=new Array('Course Name','Brief','No. of Level(s)','Duration');
		url=url+"courses-ajax.php";
	}else if(type=="level"){
		var compulsory_fldnames=new Array('leveltitle','levelbrief','levelduration');
		var compulsory_fldvals=new Array('Level Title','Brief','Duration');
		url=url+"levels-ajax.php";
	}else if(type=="teacher"){
		var compulsory_fldnames=new Array('firstname','lastname','username','password','email');
		var compulsory_fldvals=new Array('First Name','Last Name','Username','Password','Email');
		if(frm.email.value!=""&&!validate_email(frm.email)) errors=errors+"Invalid Email<br>";
		url=url+"teachers-ajax.php";
	}else if(type=="student"){
		var compulsory_fldnames=new Array('firstname','lastname','username','password','email');
		var compulsory_fldvals=new Array('First Name','Last Name','Username','Password','Email');
		if(frm.email.value!=""&&!validate_email(frm.email)) errors=errors+"Invalid Email<br>";
		url=url+"students-ajax.php";
	}else if(type=="setting"){
		var compulsory_fldnames=new Array('username','password','fullname','email');
		var compulsory_fldvals=new Array('Username','Password','Full Name','Email');
		if(frm.email.value!=""&&!validate_email(frm.email)) errors=errors+"Invalid Email<br>";
		url=url+"settings-ajax.php";
	}
	if(compulsory_fldnames.length>0){
		for(var i=0;i<compulsory_fldnames.length;i++){
			frm_fld=eval('document.'+frm.name+'.'+compulsory_fldnames[i]);
			if(frm_fld.value=="") errors+=compulsory_fldvals[i]+" cannot be blank<br>";
		}
	}
	if(errors!=""){ 
		$('#addedit_formerror').show(500);
		$('#addedit_formerror #error_message').html(errors); 
		return; 
	}
	var params=new FormData(frm)
	xml_http.onreadystatechange=onready_state_change;
	xml_http.open('POST',url,true);
	xml_http.send(params);
}
function view_record(type,act,id,url){
	if(type=="course"){
		url=url+"courses-ajax.php?act=view&myid="+id;	
		glb_frm_flds=new Array('coursename','coursebrief','courselevels','courseduration');
		glb_db_flds=new Array('Course','Brief','No_Of_Levels','Duration');
	}
	else if(type=="level"){
		url=url+"levels-ajax.php?act=view&myid="+id;	
		glb_frm_flds=new Array('leveltitle','levelbrief','levelduration');
		glb_db_flds=new Array('Level','Brief','Duration');
	}else if(type=="teacher"){
		url=url+"teachers-ajax.php?act=view&myid="+id;	
		glb_frm_flds=new Array('salutation','firstname','middlename','lastname','username','password','email');
		glb_db_flds=new Array('Salutation','First_Name','Middle_Name','Last_Name','Username','Password','Email');
	}else if(type=="student"){
		url=url+"students-ajax.php?act=view&myid="+id;	
		glb_frm_flds=new Array('salutation','firstname','middlename','lastname','username','password','email');
		glb_db_flds=new Array('Salutation','First_Name','Middle_Name','Last_Name','Username','Password','Email');
	}else if(type=="setting"){
		url=url+"settings-ajax.php?act=view&myid=1";	
		glb_frm_flds=new Array('username','password','fullname','email');
		glb_db_flds=new Array('Username','Password','Full_Name','Email');
	}
	xml_http.onreadystatechange=function(){
			if(xml_http.readyState==4){
				if(xml_http.status==200){//OK	
					var result=xml_http.responseText.split("||");
					if(result.length>1&&result[0]=="success"){ 
						var data=result[1].split("##");
						data=$.parseJSON(data[1]);
						var frm=eval('document.frm_'+type);	
						for(var i=0;i<glb_frm_flds.length;i++){
							myfld=eval('document.frm_'+type+'.'+glb_frm_flds[i]);
							myfld.value=data[glb_db_flds[i]];
						}
						if(type=="course"){
							if(data.Duration_Type=="D") frm.coursedurationtype[0].checked=true;
							else if(data.Duration_Type=="W") frm.coursedurationtype[1].checked=true;
							else if(data.Duration_Type=="M") frm.coursedurationtype[2].checked=true;
							else frm.coursedurationtype[3].checked=true;
						}else if(type=="level"){
							if(data.Duration_Type=="D") frm.leveldurationtype[0].checked=true;
							else if(data.Duration_Type=="W") frm.leveldurationtype[1].checked=true;
							else if(data.Duration_Type=="M") frm.leveldurationtype[2].checked=true;
							else frm.leveldurationtype[3].checked=true;
						}else if(type=="teacher"){
							if(data.Status=="A") frm.teacherstatus[0].checked=true;
							else frm.teacherstatus[1].checked=true;
							var data_a=result[2].split("##");
							if(data_a[1]!="null"){
								data_a=$.parseJSON(data_a[1]);
								assigns=new Array();
								$.each(data_a,function(index,row_a){
									assigns[index]=row_a.Course_Id;
								});
								for(var i=0;i<frm.elements.length;i++){
									if(frm.elements[i].name=="teachercourses[]"){
										for(var j=0;j<frm.elements[i].length;j++){
											if(assigns.indexOf(frm.elements[i][j].value)!=-1)
												frm.elements[i][j].selected=true;
										}
									}
								}
							}
						}else if(type=="student"){
							if(data.Status=="A") frm.studentstatus[0].checked=true;
							else frm.studentstatus[1].checked=true;
							var data_a=result[2].split("##");
							if(data_a[1]!="null"){
								data_a=$.parseJSON(data_a[1]);
								assigns=new Array();
								$.each(data_a,function(index,row_a){
									assigns[index]=row_a.Course_Id;
								});
								for(var i=0;i<frm.elements.length;i++){
									if(frm.elements[i].name=="studentcourses[]"){
										for(var j=0;j<frm.elements[i].length;j++){
											if(assigns.indexOf(frm.elements[i][j].value)!=-1)
												frm.elements[i][j].selected=true;
										}
									}
								}
							}
						}
						/*$.each(data,function(fld,val){
							myfld=eval('document.frm_'+type+'.'+fld);	
							myfld.value=val;
						});*/
						setTimeout(function(){view_single_record(type,'show','edit',id);},100);
					}
					else alert(xml_http.responseText);
				}
			}	
		};
	xml_http.open('POST',url,true);
	xml_http.send(null);
}
function delete_records(type,frm,url,chkbox){
	var chkbox_count=0; var chkbox_value="";
	frm.act.value="del";
	for(var i=0;i<frm.elements.length;i++){ 
		if(frm.elements[i].name==chkbox&&frm.elements[i].checked==true){ 
			frm.myids.value=frm.myids.value+frm.elements[i].value+","; 
			chkbox_count++; 
		}
	} 
	if(chkbox_count==0){ alert("Kindly check record(s) to delete."); return; }
	if(!confirm("Are you sure to delete selected record(s)?")){ return; }
	if(type=="course"){
		url=url+"courses-ajax.php";	
	}else if(type=="level"){
		url=url+"levels-ajax.php";	
	}else if(type=="teacher"){
		url=url+"teachers-ajax.php";	
	}else if(type=="student"){
		url=url+"students-ajax.php";	
	}else if(type=="setting"){
		url=url+"settings-ajax.php";	
	}
	var params=new FormData(frm)
	xml_http.onreadystatechange=onready_state_change;
	xml_http.open('POST',url,true);
	xml_http.send(params);
}
function hideshow_addedit_form(act){
	if(act=="hide"){ $('#addedit_formerror').hide(500); }
	else{ $('#addedit_formerror').show(500); }
}
function popup_box(act,postop,posright){
	if(act=="hide"){ $('#popup_box').hide(500); }
	else{ 
		$('#popup_box').css('top',postop);
		$('#popup_box').css('right',posright);
		$('#popup_box').show(500); 
	}
}
function popup_box_fill(type,url){
	var postop=posright=0;
	if(type=="student"){
		url=url+"popup-box-fill-ajax.php";
		postop=-210;
		posright=-200;
	}
	if(type=="teacher"){
		url=url+"popup-box-fill-ajax.php";
		postop=-190;
		posright=-190;
	}
	url=url+"?type="+type;
	xml_http.onreadystatechange=function(){
			if(xml_http.readyState==4){
				if(xml_http.status==200){//OK	
					var result=xml_http.responseText.split("||");
					if(result.length>1&&result[0]=="success"){
						$('#popup_box_content').html(result[1]);
						popup_box('show',postop,posright);
					}
					else alert(xml_http.responseText);
				}
			}	
		};
	xml_http.open('GET',url,true);
	xml_http.send(null);
}
function popup_box_select(type,id,nm,url){
	var frmfld=eval("document.frm_schedule."+type+"id");
	frmfld.value=id;
	$("#inputtext_"+type).html(nm);
	$("#clear_"+type).css("display","block");
	if(type=="student") get_student_courses(id,url);
	else popup_box('hide');
}
function clear_div(div){
	$('#'+div).html('');
}
function clear_frmfld(obj){
	if(typeof obj!=="object") obj=eval(obj);
	obj.value="";
}
function hide_div(div){
	$('#'+div).css("display","none");
}
function get_student_courses(id,url){
	url=url+"get-student-course-ajax.php?myid="+id;
	xml_http.onreadystatechange=function(){
			if(xml_http.readyState==4){
				if(xml_http.status==200){//OK	
					var result=xml_http.responseText.split("||");
					if(result.length>1&&result[0]=="success"){
						$('#schedule_course').html(result[1]);
						popup_box('hide');
					}
					else alert(xml_http.responseText);
				}
			}	
		};
	xml_http.open('GET',url,true);
	xml_http.send(null);
}