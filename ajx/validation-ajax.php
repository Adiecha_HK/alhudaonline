<?php 
include '../inc/config.php';
include '../inc/functions.php';

$action = $_REQUEST['action'];

$responce = array();

header('Content-type:text/json;charset=UTF-8');

if ($action == "validate_username") {
  $data = $_REQUEST['data'];
  $responce = array('status' => !(get_count('students','Username=\''.($data).'\' AND Reg_Status != \'P\'')));
}

if ($action == "validate_email") {
  $data = $_REQUEST['data'];
  $responce = array('status' => !(get_count('students','Email=\''.($data).'\' AND Reg_Status != \'P\'')));
}

echo json_encode($responce);
?>