<?php 
include '../inc/config.php';
include '../inc/functions.php';

header('Content-type:text/json;charset=UTF-8');

$myact=(isset($_POST["action"])&&trim($_POST["action"])!="")?trim($_POST["action"]):"";
if($myact=="course_load"){
	$return=array();
	$cid = $_POST['course'];
	$sql_qry = "SELECT Id, CONCAT(Salutation,' ',First_Name,' ',Last_Name) AS Full_Name FROM teachers WHERE Id IN (SELECT Teacher_Id FROM courses_assignments WHERE Course_Id='".$cid."');";

	$sql_res=mysql_query($sql_qry) or die(session_err("Database error!", mysql_error()));
	if(mysql_num_rows($sql_res)>0) while($sql_row=mysql_fetch_assoc($sql_res)) array_push($return, $sql_row);

	$res = array(
		'course'  => get_record('courses', '', 'Id='.$cid),
		'lessons' => get_records('lessons', 'Id, Lesson, amount', 'Course_Id='.$cid),
		'teachers'=> $return);
	echo json_encode($res);
}
else if($myact == "teacher_available_date") {
	$return=array();
	$tid = $_POST['teacher'];
	$sql_qry = "SELECT  DATE_FORMAT(DATE_ADD(CONCAT(`Date`, ' ', `Hours`, ':', `Minutes`, ':00'), INTERVAL ".get_timezone()." MINUTE), '%e-%c-%Y') AS Dt FROM teachers_availabilities WHERE Teacher_Id='".$tid."' AND `Date` > NOW() AND Is_Scheduled = 'N' GROUP BY Date;";

	$sql_res=mysql_query($sql_qry) or die(session_err("Database error!", mysql_error()));
	if(mysql_num_rows($sql_res)>0) while($sql_row=mysql_fetch_assoc($sql_res)) array_push($return, $sql_row['Dt']);

	$res = array(
		'dates'  => $return,
		'query'  => $sql_qry
		);
	echo json_encode($res);
}
else if($myact=="time_of_day") {
	$return=array();
	$tid = $_POST['teacher'];
	$dt = $_POST['date'];
	#select DATE_FORMAT(DATE_ADD(CONCAT(`Date`,' ',`Hours`, ':', `Minutes`, ':00'), INTERVAL 330 Minute), "%k") As Hr from teachers_availabilities
	$sql_qry = "SELECT  Id,
		DATE_FORMAT(DATE_ADD(CONCAT(`Date`,' ',`Hours`, ':', `Minutes`, ':00'), INTERVAL ".get_timezone()." MINUTE), '%k') AS Hours, 
		DATE_FORMAT(DATE_ADD(CONCAT(`Date`,' ',`Hours`, ':', `Minutes`, ':00'), INTERVAL ".get_timezone()." MINUTE), '%i') AS Minutes
		FROM teachers_availabilities WHERE Teacher_Id='".$tid."' AND DATE_FORMAT(DATE_ADD(CONCAT(`Date`, ' ', `Hours`, ':', `Minutes`, ':00'), INTERVAL ".get_timezone()." MINUTE), '%Y-%c-%e')=DATE_FORMAT('".$dt."', '%Y-%c-%e') AND `Is_Scheduled`='N';";

#		echo $sql_qry;

	$sql_res=mysql_query($sql_qry) or die(session_err("Database error!", mysql_error()));
	if(mysql_num_rows($sql_res)>0) while($sql_row=mysql_fetch_assoc($sql_res)) array_push($return, $sql_row);

	$res = array(
		'time'  => $return,
		'query'  => $sql_qry
		);
	echo json_encode($res);
}

// SELECT DATE_FORMAT( DATE_ADD( CONCAT(  `Date` ,  ' ',  `Hours` ,  ':',  `Minutes` ,  ':00' ) , INTERVAL 330 
// MINUTE ) ,  '%k' ) AS Hours, DATE_FORMAT( DATE_ADD( CONCAT(  `Date` ,  ' ',  `Hours` ,  ':',  `Minutes` ,  ':00' ) , INTERVAL 330 
// MINUTE ) ,  '%i' ) AS Minutes
// FROM teachers_availabilities
// WHERE Teacher_Id =  '17'
// AND DATE_FORMAT( DATE_ADD( CONCAT(  `Date` ,  ' ',  `Hours` ,  ':',  `Minutes` ,  ':00' ) , INTERVAL 330 
// MINUTE ) ,  '%Y-%c-%e' ) =  '2014-12-28'
// LIMIT 0 , 30
?>