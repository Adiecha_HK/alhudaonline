<?php
include './inc/config.php';
include './inc/functions.php';
if(isset($_SESSION["anybody_loggedin"]) && $_SESSION["anybody_loggedin"]=="student"){ 
	header("Location: ".URL."dashboard.php"); 
	?><script type="text/javascript">window.location = "dashboard.php"</script><?php
	exit;

}

if(isset($_POST["Login_Me"])&&$_POST["Login_Me"]=="Login"){
	$compulsory_postvars=array("Username"=>"Unm","Password"=>"Pwd");
	$errors=array();
	check_compulsory_postvars();
	$sql_qry = "SELECT Id,CONCAT(Salutation,' ',First_Name,' ',Last_Name) AS Full_Name, Reg_Status FROM students WHERE Username='".mysql_escape_string($_POST["Unm"])."' AND Password='".mysql_escape_string($_POST["Pwd"])."'";
	$res=mysql_query($sql_qry) or die(error_mysql("Student Login"));


	if($row=mysql_fetch_array($res)){
		if($row['Reg_Status'] != 'S') {
			$errors=array();
			array_push($errors,"Sorry, you can't login, please contact administrator.");

		} else {
			session_destroy();
			session_start();

			$_SESSION["anybody_loggedin"]="student";
			$_SESSION["student_uniqueid"]=$row["Id"];
	    $_SESSION["timezone"]=$row["TimeZone"];
			$_SESSION["student_fullname"]=escape_string($row["Full_Name"]);
			header("Location: dashboard.php"); exit;
			?><script type="text/javascript">window.location = "dashboard.php"</script><?php
			exit;
		}
	}else{
		$errors=array();
		array_push($errors,"Invalid Username / Password.");
	}
	$_SESSION['error'] = $errors;
	header('location: /login/');
	?><script type="text/javascript">window.location = "/login/"</script><?php
	
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap-responsive.css" />
<link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>index.css" />
<link href='http://fonts.googleapis.com/css?family=Dosis:200,400' rel='stylesheet' type='text/css'>
<style type="text/css">
	body {
		background-image: url('<?=IMG."heroshot.jpg"?>');
	}
	.cg-form-container legend h3{
		color: white;
		margin-left: 20px;
	}

	.cg-form-container a.link {
		color: cyan;
	}
	.cg-form-container {
		background-color: rgba(0,0,0,0.9);
		border: 10px solid rgba(255,255,255,0.5);
		border-radius: 15px;
		margin-top: 20%;
	}

	.cg-form-container input {
		max-width: 90%;
	}

</style>
<script language="javascript" src="<?php echo JS; ?>html5.js"></script>
<script language="javascript" src="<?php echo JS; ?>jquery-1.8.0.min.js"></script>
<script language="javascript" src="<?php echo JS; ?>bootstrap.js"></script>
<script language="javascript" src="<?php echo JS; ?>index.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Student Login</title>
</head>

<body class="homepage">
<form name="frm_login" id="frm_login" method="post" style="display:none">
<table width="100%" style="display:none">
	<tr><th>Student Login</th></tr>
	<tr><td align="center"><?php show_errors('adm'); ?></td></tr>
	<tr><td align="center">Username: <input type="text" name="Unm" id="Unm" maxlength="30" /></td></tr>
	<tr><td align="center">Password: <input type="Password" name="Pwd" id="Pwd" maxlength="30" /></td></tr>
	<tr><td align="center"><input type="submit" name="Login_Me" id="Login_Me" value="Login" /></td></tr>
	<tr><td align="center">
		<a href="#">forget password?</a>&nbsp;
		<!-- <a href="forget-password.php">forget password?</a>&nbsp; -->
		<a href="register.php">new user?</a>&nbsp;
		<a href="<?php echo URL; ?>">back to home</a>
	</td></tr>
</table>
</form>



<div class="row-fluid">
	<div class="offset3 span6">
		<div class="cg-form-container">
			<form role="form" class="form-horizontal" name="frm_login" id="frm_login" method="POST">
				<legend><h3>Sign In (Student)</h3></legend>
	
				<?php show_errors('adm'); ?>

				<div class="control-group">
					<label class="control-label" for="Unm">Username</label>
					<div class="controls">
						<input type="text" name="Unm" id="Unm" maxlength="30" placeholder="Username"/>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="Pwd">Password</label>
					<div class="controls">
						<input type="Password" name="Pwd" id="Pwd" maxlength="30" placeholder="Password"/>
						<div><a href="#" class="link">forget password?</a></div>
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<input type="submit" name="Login_Me" class="btn btn-success" id="Login_Me" value="Login" />
						<!-- <button type="submit" class="btn btn-success">Sign in</button> -->
						<a class="btn" href="<?php echo URL; ?>">Back</a>&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="register.php" class="link">new user?</a>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>


</body>
</html>
