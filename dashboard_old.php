<?php 
$page_nav="Dashboard";
$page_title="Dashboard";
include 'header.php'; 
?>
<h2>Assalamualaikum, Welcome to AlHuda Flexible Schedule

Courses.
<br/>
May Allah swt make this learning easy and beneficial for 

you. Ameen</h2>
<table width="100%" cellpadding="2" cellspacing="3">
	<tr>
		<td width="75%" valign="top">
			<table width="100%" cellpadding="2" cellspacing="3">
				<tr>
					<td colspan="3" align="left"><strong>Upcoming Activities</strong></td>
				</tr>
				<tr>
					<td colspan="3" align="left"><strong>24/09/2014</strong></td>
				</tr>
				<tr>
					<td width="3%" align="left">#1</td>
					<td width="7%">10:00</td>
					<td width="90%" align="left">Lession of <strong>Urdu</strong> by Mr. Saif for 2 hours</td>
				</tr>
				<tr>
					<td width="3%" align="left">#2</td>
					<td width="7%">14:00</td>
					<td width="90%" align="left">Lesson of <strong>English</strong> by Mr. Girish for 1 hour</td>
				</tr>
			</table>
		</td>
		<td width="25%" valign="top">
			<table width="100%" cellpadding="2" cellspacing="3">
				<tr>
					<td width="100%" colspan="2" align="left"><strong>Summary</strong></td>
				</tr>
				<tr>
					<td width="80%" align="left">New Lessons</td>
					<td width="20%" align="right">12</td>
				</tr>
				<tr>
					<td align="left">Re-scheduled </td>
					<td align="right">1</td>
				</tr>
				<tr>
					<td align="left">Up comming Lessons in this week</td>
					<td align="right">3</td>
				</tr>
			</table>
		</td>
	</tr>
</table>	

<?php 
include 'footer.php'; 
?>