<?php 
$page_nav="My Schedules";
$page_title="My Schedules";
include 'header-tchr.php'; 

if($_SERVER['REQUEST_METHOD'] == "POST") {
  $action = $_REQUEST['action'];
  if($action == "delete") {
    $sql_qry = "UPDATE students_schedules SET deleted_by_teacher='1' WHERE Id in ('".implode("','",$_REQUEST['history'])."')";
    mysql_query($sql_qry) or die(session_err("Database error!", "Unable to delete (" . mysql_error() . ")"));
  }
  header("location: ".$_SERVER['PHP_SELF']);
  echo '<script type="text/javascript"> window.location = "'.$_SERVER['PHP_SELF'].'"; </script>';
}

$sql_res=teacher_history($_SESSION['anybody_id']);
$sql_nos=mysql_num_rows($sql_res);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=TEACHER?>">Home</a> <span class="divider">/</span></li>
      <li class="active">History</li>
    </ul>
</div>
<!-- List Courses >>> -->
<form method="POST">
<input type="hidden" name="action" value="delete">
<table class="DataTable table">
  <tr class="DataTableHeading">
    <th width="5%" align="left"><?php if($sql_nos>0) { ?><input type="checkbox" id="toggleAll"><?php } ?></th>
    <th width="6%" align="left">Date</th>
    <th width="5%" align="center">Time</th>
    <th width="30%" align="left">Details</th>
    <th width="28%" align="left">Name of the Student</th>
    <th width="11%" align="center">Duration</th>
    <th width="15%" align="center">Status</th>
    <!-- <th width="31%" align="center">More</th> -->
  </tr> 
<?php
$iDate="";
if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
  if($sql_row["course_mode"] == 'A') {
?>
    <tr class="DataTableRow">
      <td align="left"><input type="checkbox" class="historyRecord" name="history[]" value="<?=$sql_row['Id']?>" /></td>
      <td align="left"><?php echo "- N/A -"; ?></td>
      <td align="center"><?php echo "- N/A -"; ?></td>
      <td align="left"><?php echo $sql_row["Course"]; ?></td>
      <td align="left"></td>
      <td align="left"><?php echo $sql_row["Teacher"]; ?></td>
      <td align="center"><?php echo $sql_row["Duration"]; ?></td>
      <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
      <td align="center">
        <?php if(isset($sql_row["Rating"]) && $sql_row["Rating"] > 0) { ?>
          <?=$feedbacks[$sql_row['Rating'] - 1]?>
        <?php } else { ?>
          <a class="btn btn-info" href="#myModal" role="button" data-toggle="modal" data-id="<?=$sql_row['Id']?>">Feedback</a>&nbsp;
        <?php } ?>
      </td>
    </tr>
<?php
  } else {
	if($iDate!=$sql_row["Schedule_Date"])
	{
?>
  <tr class="DataTableRow">
    <td align="left"></td>
    <td align="left"><?php echo $sql_row["Schedule_Date"]; ?></td>
    <td align="center"></td>
    <td align="left"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
  </tr>
<?php
		$iDate=$sql_row["Schedule_Date"];		
	}
?>
  <tr class="DataTableRow">
    <td align="left"><input type="checkbox" class="historyRecord" name="history[]" value="<?=$sql_row['Id']?>" /></td>
    <td align="left"></td>
    <td align="center"><?php echo $sql_row["Schedule_Time"]; ?></td>
    <td align="left">Lesson for <?php echo $sql_row["Course"]; ?> to <?php echo $sql_row["Student"]; ?></td>
    <td><?php echo $sql_row["Student"]; ?></td>
    <td align="center"><?php echo $sql_row["Duration"]; ?></td>
    <td align="center"><?php echo "<span title='".escape_string($sql_row["Reason"],"display")."'>".escape_string($sql_row["Status"],"display")."</span>"; ?></td>
<!--     <td align="center">
        <a href="#" class="btn btn-info">Feedback</a>&nbsp;
    </td>
 -->  </tr>
<?php
}}}else{
?>
  <tr>
    <td colspan="7" align="center">There is no Scheduled lession found.</td>
  </tr>
<?php 
}
?>
</table>

<?php if($sql_nos>0) { ?><input class="btn btn-danger" type="submit" value="Delete selected records" /><?php } ?>
</form>
<script type="text/javascript">
  $(document).ready(function() {
    $("#toggleAll").checkAll(".historyRecord");
  });
</script>
<!-- List Courses <<< -->
<?php 
include 'footer-tchr.php'; 
?>















