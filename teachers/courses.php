<?php 
$page_nav="Courses";
$page_title="Course";
include 'header-tchr.php'; 
$sql_qry="SELECT c.Id AS Id, c.Course AS Course, c.Brief AS Brief, l.No_Of_Levels AS No_Of_Levels,(CONCAT(c.Duration,' ',IF(c.Duration_Type='D','Day(s)',IF(c.Duration_Type='W','Week(s)',IF(c.Duration_Type='M','Month(s)','Year(s)'))))) AS Duration FROM courses AS c LEFT JOIN (SELECT Course_Id AS cId, COUNT(Id) AS No_Of_Levels FROM `lessons` GROUP BY Course_Id) AS l ON l.cId=c.Id WHERE Id IN (SELECT Course_id From courses_assignments WHERE Teacher_id='".$_SESSION["anybody_id"]."') ORDER BY Id DESC";
// $sql_qry="SELECT c.Id as Id, c.Course as Course, c.Brief as Brief, l.No_Of_Levels as No_Of_Levels,(CONCAT(c.Duration,' ',IF(c.Duration_Type='D','Day(s)',IF(c.Duration_Type='W','Week(s)',IF(c.Duration_Type='M','Month(s)','Year(s)'))))) AS Duration, s.Students AS Students FROM courses AS c LEFT JOIN (SELECT Course_Id AS cId, COUNT(Id) AS No_Of_Levels FROM `lessons` GROUP BY Course_Id) AS l ON l.cId=c.Id LEFT JOIN (SELECT Course_Id AS cId, COUNT( Student_Id ) AS Students FROM  `students_courses` GROUP BY Course_Id) AS s ON s.cId=c.Id ORDER BY Id DESC";
/*
echo $sql_qry;
*/
$sql_res=mysql_query($sql_qry) or die(error_mysql("Selecting Courses"));
$sql_nos=mysql_num_rows($sql_res);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=TEACHER?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Courses</li>
    </ul>
</div>
<!-- List Courses >>> -->
<table  class="DataTable table">
  <tr class="DataTableHeading">
    <th width="27%" align="left">Course Name</th>
    <th width="44%" align="left">Details</th>
    <th width="11%" align="center">No. of Lessons</th>
    <th width="11%" align="center">Duration</th>
    <th width="7%" align="center">#</th>
  </tr> 
<?php
if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
?>
  <tr class="DataTableRow">
    <td width="27%" align="left"><?php echo escape_string($sql_row["Course"],"display"); ?></td>
    <td width="44%" align="left"><?php echo escape_string($sql_row["Brief"],"display"); ?></td>
    <td width="11%" align="center"><?php echo escape_string($sql_row["No_Of_Levels"],"display"); ?></td>
    <td width="11%" align="center"><?php echo escape_string($sql_row["Duration"],"display"); ?></td>
    <td width="7%" align="center"><a class="btn btn-info" href="lessons.php?cid=<?php echo $sql_row["Id"]; ?>" >View</a></td>
  </tr>
<?php
}}else{
?>
  <tr>
    <td colspan="7" align="center">There is no Course in database.</td>
  </tr>
<?php 
}
?>
</table>
<!-- List Courses <<< -->
<?php 
include 'footer-tchr.php'; 
?>
