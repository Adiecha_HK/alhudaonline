<?php 
$page_nav="Courses";
$page_title="Course";
include 'header-tchr.php'; 
$cid=isset($_REQUEST["cid"])?intval($_REQUEST["cid"]):0;

$sql_qry="SELECT Id,Lesson,Brief,Duration FROM lessons WHERE Course_Id = " . $cid . " ORDER BY Id DESC";
$sql_res=mysql_query($sql_qry) or die(error_mysql("Selecting Lesson"));
$sql_nos=mysql_num_rows($sql_res);
$course = get_record('courses', 'Course', 'Id='.$cid);
?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=TEACHER?>">Home</a> <span class="divider">/</span></li>
      <li><a href="<?=TEACHER.'courses.php'?>">Courses</a> <span class="divider">/</span></li>
      <li class="active">Lessons of <strong><?=$course['Course']?></strong></li>
    </ul>
</div>
<!-- Breadcrum ends -->

<!-- List Courses >>> -->
<div id="listing_lesson" style="display:block;">
  <div class="row-fluid">
    <div class="span12">
      <table width="100%" class="DataTable table" >
        <tr class="DataTableHeading">
          <th width="5%" align="center">#</th>
          <th width="20%" align="left">Lesson</th>
          <th width="40%" align="left">Details</th>
          <th width="15%" align="center">Duration</th>
          <th width="10%" align="center">#</th>
        </tr> 
      <?php
      if($sql_nos>0){ $i = 0; while($sql_row=mysql_fetch_array($sql_res)){
      ?>
        <tr class="DataTableRow">
          <td width="5%" align="center"><?=$i++?></td>
          <td width="20%" align="left"><?php echo escape_string($sql_row["Lesson"],"display"); ?></td>
          <td width="40%" align="left"><?php echo escape_string($sql_row["Brief"],"display"); ?></td>
          <td width="15%" align="center"><?php echo escape_string($sql_row["Duration"],"display"); ?></td>
          <td width="10%" align="center"><a class="btn btn-info" href="lesson.php?cid=<?=$cid?>&lid=<?=$sql_row["Id"]?>" >View</a></td>
        </tr>
      <?php
      }}else{
      ?>
        <tr>
          <td colspan="7" align="center">There is no lessons in database.</td>
        </tr>
      <?php 
      }
      ?>
      </table>
    </div>
  </div><br/>
</div>
<?php
include 'footer-tchr.php'; 
?>