<?php 
$page_nav="Dashboard";
$page_title="Dashboard";
include 'header-tchr.php'; 
$num = 5;
$tid = $_SESSION['anybody_id'];
?>
<!-- <h1><?=$_SESSION['anybody_fullname']?> - Dashboard</h1> -->
<div class="row-fluid">
	<div class="span12">
		<h2>Assalamualaikum, Welcome to AlHuda Flexible Schedule Courses. Barakallahu Fikum.</h2>
	</div>
</div>
<div class="row-fluid">
	<?php $user = get_record('teachers', '', 'Id='.$tid); ?>
	<div class="span6">
		<div class="panel">
			<div class="panel-header">Basic information</div>
			<div class="panel-body">
				<div class="panel-inner-body">
					<table class="table">
						<tr>
							<th>Full Name:</th>
							<td><?=$user['First_Name'] . "  " . $user['Last_Name']?></td>
						</tr>
						<tr>
							<th>Username:</th>
							<td><?=$user['Username']?></td>
						</tr>
						<tr>
							<th>Email:</th>
							<td><?=$user['Email']?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>




		<?php
		$schedules = schedule("T", $tid, "0,".$num, "S");
		$count=mysql_num_rows($schedules);
		?>
		<div class="panel">
			<div class="panel-header">
				Upcomming Schedules <span class="pull-right badge badge-success"><?=$count . " out of " . get_count('students_schedules','Teacher_Id='.$tid.' AND `Schedule_On` > NOW()')?></span>
			</div>
			<div class="panel-body">
				
				<?php if($count > 0) { ?>
				<ul>
					<?php while($sql_row=mysql_fetch_array($schedules)){ //echo var_dump($sql_row)?>
					  <?php if($iDate!=$sql_row["Schedule_Date"]) { $iDate = $sql_row['Schedule_Date'] ?>
					  <li>&nbsp;</li>
						<li class="date"><?=$sql_row['Schedule_Date']?></li>
						<?php } ?>
						<li><?="[".$sql_row['Schedule_Time']."] ".$sql_row['Lesson']." of ".$sql_row['Course']?></li>
					<?php } ?>
				</ul>
				<div class="panel-inner-body">
					<a href="schedule.php">More</a>
				</div>
				<?php } else { ?>
				<div class="panel-inner-body">No schedules found</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="span6">
		<div class="panel">
			<div class="panel-header">Messages</div>
			<div class="panel-body">
				<ul>
					<li>&nbsp;</li>
				<?php $msgs = get_records('message', '', '(Sender_Type=\'T\' AND Sender_Id='.$tid.') OR (Reciever_Type=\'T\' AND Reciever_Id='.$tid.')');
				$no_msgs = true;
				foreach ($msgs as $key => $value) { $no_msgs = false;?>
					<?php if($value['Sender_Type'] == 'T' && $value['Sender_Id'] == $tid) { ?>
					<li title="Out going"><i class="icon-arrow-up"></i>&nbsp;<?=$value['Title']?><span class="pull-right right-away"><?=dashboard_formate($value['Entry_Date'])?></span></li>
					<?php } ?>
					<?php if($value['Reciever_Type'] == 'T' && $value['Reciever_Id'] == $tid) { ?>
					<li title="In coming"><i class="icon-arrow-down"></i>&nbsp;<?=$value['Title']?><span class="pull-right right-away"><?=dashboard_formate($value['Entry_Date'])?></span></li>
					<?php } ?>
				<?php } ?>
				</ul>
				<?php if($no_msgs) { ?>
				<div class="panel-inner-body">
					No messages found<br/><br/>
				</div>
				<?php } else { ?>
				<div class="panel-inner-body">
					<a href="messages.php">More</a>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php
		$schedules = schedule("T", $tid, "0,".$num, "H");
		$count=mysql_num_rows($schedules);
		?>
		<div class="panel">
			<div class="panel-header">
				History lessons <span class="pull-right badge badge-success"><?=$count . " out of " . get_count('students_schedules','Teacher_Id='.$tid.' AND `Schedule_On` < NOW() AND `deleted_by_teacher`=0')?></span>
			</div>
			<div class="panel-body">
				
				<?php if($count > 0){ ?>
				<ul>
					<?php while($sql_row=mysql_fetch_array($schedules)){ //echo var_dump($sql_row)?>
					  <?php if($iDate!=$sql_row["Schedule_Date"]) { $iDate = $sql_row['Schedule_Date'] ?>
					  <li>&nbsp;</li>
						<li class="date"><?=($sql_row['course_mode'] == "A" ? "Audio lessons": $sql_row['Schedule_Date'])?></li>
						<?php } ?>
						<li><?=($sql_row['course_mode'] != "A"? "[".$sql_row['Schedule_Time']."] ":"").$sql_row['Lesson']." of ".$sql_row['Course']?></li>
					<?php } ?>
				</ul>
				<div class="panel-inner-body">
					<a href="history.php">More</a>
				</div>
				<?php } else { ?>
				<div class="panel-inner-body">No history found</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php 
include 'footer-tchr.php'; 
?>