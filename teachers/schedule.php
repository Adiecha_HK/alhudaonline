<?php 
$page_nav="Courses";
$page_title="Course";
$page_script=array('teacher-schedule.js');
include 'header-tchr.php'; 

$act = $_REQUEST['act'];
if($act == "reject") {
  cancel_schedule($_REQUEST['id'], 1);
  header("location: ".$_SERVER['PHP_SELF']);
}

/*
echo $sql_qry;
*/
$sql_res=teacher_schedule($_SESSION['anybody_id']);
$sql_nos=mysql_num_rows($sql_res);

?>
<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=TEACHER?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Schedule</li>
    </ul>
</div>
<!-- List Courses >>> -->
<table class="DataTable table">
  <tr class="DataTableHeading">
    <th width="6%" align="left">Date</th>
    <th width="5%" align="center">Time</th>
    <th width="19%" align="left">Name of student</th>

    <th width="14%" align="left">Course</th>
    <th width="14%" align="left">Lesson#</th>

    <th width="14%" align="center">Duration</th>
    <th width="14%" align="center">Status</th>
    <th width="14%" align="center">#</th>
  </tr> 
  <?php
  if($sql_nos>0){ while($sql_row=mysql_fetch_array($sql_res)){
    if($iDate!=$sql_row["Schedule_Date"])
    {
  ?>
  <tr class="DataTableRow">
    <td align="left"><?php echo $sql_row["Schedule_Date"]; ?></td>
    <td align="center"></td>
    <td align="left"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
  </tr>
  <?php
      $iDate=$sql_row["Schedule_Date"];   
    }
  ?>
  <tr class="DataTableRow">
    <td width="6%" align="left"></td>
    <td width="5%" align="center"><?php echo $sql_row["Schedule_Time"]; ?></td>
    <td width="19%" align="left"><?php echo $sql_row["Student"]; ?></td>
    <td width="14%" align="left"><?php echo $sql_row["Course"]; ?></td>
    <td width="14%" align="left"><?php echo $sql_row["Lesson"]; ?></td>
    <td width="14%" align="center"><?php echo $sql_row["Duration"]; ?></td>
    <td width="14%" align="center"><?php echo $sql_row["Status"]; ?></td>
    <td width="14%" align="center">
      <?php if($sql_row['Status'] == "OK") { ?>
      <a href="?act=reject&id=<?=$sql_row['Id']?>" class="btn btn-info reject-btn">Reject</a>
      <?php } ?>
    </td>
  </tr>
  <?php
  }}else{
  ?>
  <tr>
    <td colspan="8" align="center">There is no Scheduled lession found.</td>
  </tr>
  <?php 
  }
  ?>
</table>
<script type="text/javascript">
  $(".reject-btn").on('click', function() {
    var cond = confirm("Sure, you want to reject schedule?");
    return cond;
  })
</script>
<!-- List Courses <<< -->
<?php 
include 'footer-tchr.php'; 
?>
