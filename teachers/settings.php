<?php
$page_nav="Settings";
$page_title="Settings";
include 'header-tchr.php'; 

if($_SERVER['REQUEST_METHOD'] == "POST") {
  $tid = $_SESSION['anybody_id'];
  $updation = "";
  foreach ($_POST as $key => $value) {
    # code...
    $updation .= ($updation != ""?", ":"")."`".$key."`='".escape_string($value,'','db')."' ";
  }
  $sql_qry = "UPDATE `teachers` SET " . $updation . " WHERE Id=" . $tid;
  $_SESSION['timezone'] = $_POST['TimeZone'];
  mysql_query($sql_qry) or die(session_err('Database error!', "Unable to update your personal information. (" . mysql_error() . ")"));
  header("location: " . $_SERVER['PHP_SELF']);
}


?>

<!-- Breadcrum starts -->
<div>
    <ul class="breadcrumb">
      <li><a href="<?=TEACHER?>">Home</a> <span class="divider">/</span></li>
      <li class="active">Settings</li>
    </ul>
</div>
<?php $teacher = get_record('teachers', 'Username,First_Name,Middle_Name,Last_Name,Email,Phone,City,Country,TimeZone', 'Id='.$_SESSION['anybody_id']); ?>
<div id="info">
  <form class="form-horizontal" method="POST">
    <legend>Personal information</legend>
    <?php foreach ($teacher as $key => $value) { if(!is_numeric($key)) { ?>
      <div class="control-group">
        <label class="control-label" for="<?=$key?>"><?=implode(" ", explode("_", $key))?></label>
        <div class="controls">
          <?php if($key == "Credits") { ?>
            <input type="text" disabled="disabled" id="<?=$key?>" placeholder="<?='Enter '.implode(" ", explode("_", $key))?>" value="<?=$value?>" />
          <?php } else if($key == "TimeZone") { $i = -12 * 60?>
            <select name="TimeZone">
              <?php for ($i=0; $i < 24 * 60; $i+=30) { 
                # code...
                $t = $i - (12*60);
                $val = abs($t);
                echo "<option value='" . ($t) . "' ".($t == $value?'selected':'')." >" . ($t < 0?"-":"+").floor($val/60).":".(($val%60) < 10 ? "0":"").($val%60) . "</option>";
              } ?>
            </select>
          <?php } else { ?>
            <input type="text" name="<?=$key?>" id="<?=$key?>" placeholder="<?='Enter '.implode(" ", explode("_", $key))?>" value="<?=$value?>" />
          <?php } ?>
        </div>
      </div>
        <!-- <tr><td><?=$key?></td><td><input name="<?=$key?>" value="<?=$value?>" /></td></tr> -->
    <?php } } ?>
    <div class="control-group">
      <div class="controls">
        <button type="submit" class="btn">Save</button>
      </div>
    </div>

  </form>
</div>


<?php include 'footer-tchr.php'; ?>
