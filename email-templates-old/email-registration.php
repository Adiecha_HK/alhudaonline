<?php 
include 'email-top.php'; 

$_SESSION['payment_id'] = 19;

$StudentName = "";
$StudentEmail = "";
$StudentCourses = array();

$query = "SELECT p.Sid, s.First_Name, s.Last_Name, s.Email, c.Course 
	FROM payment p 
		LEFT JOIN students s ON s.Id = p.Sid
		LEFT JOIN students_courses sc ON sc.Student_Id = s.Id 
		LEFT JOIN courses c ON c.Id = sc.Course_Id 
	WHERE p.Id = ".$_SESSION['payment_id'];
$result = mysql_query($query) or print("Error :: Cannot select student details.<hr>".mysql_error());
while($arow = mysql_fetch_assoc($result)) {
	if($StudentName == "") {
		$StudentName = $arow["First_Name"]." ".$arow["Last_Name"];
		$StudentEmail = $arow["Email"];
	}
	array_push($StudentCourses, $arow["Course"]);
}
?>
Dear <?php echo $StudentName; ?>, <br /><br />
Thank you for taking interest on the below course(s).<br />
<?php echo implode("<br />", $StudentCourses); ?><br /><br />
<a href="<?php echo URL; ?>" target="_blank">Click here</a> to verify your email address.<br /><br />
<?php include 'email-bottom.php'; ?>