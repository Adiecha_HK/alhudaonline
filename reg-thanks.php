<?php
include './inc/config.php';
include './inc/functions.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo CSS; ?>index.css" />
    <link href='http://fonts.googleapis.com/css?family=Dosis:200,400' rel='stylesheet' type='text/css'>
    <!-- <META http-equiv="refresh" content="5;URL=<?=URL?>">  -->
    <meta http-equiv="refresh" content="5;URL='http://www.alhudaflexible.com/'" />

    <style type="text/css">
      body {
        background-image: url('<?=IMG."heroshot.jpg"?>');
      }
      .cg-form-container legend h3{
        color: white;
        margin-left: 20px;
      }

      .cg-form-container a.link {
        color: cyan;
      }
      .cg-form-container {
        background-color: rgba(0,0,0,0.9);
        border: 10px solid rgba(255,255,255,0.5);
        border-radius: 15px;
        margin-top: 20%;
      }

      .cg-form-container input {
        max-width: 90%;
      }

    </style>
    <script language="javascript" src="<?php echo JS; ?>html5.js"></script>
    <script language="javascript" src="<?php echo JS; ?>jquery-1.8.0.min.js"></script>
    <script language="javascript" src="<?php echo JS; ?>bootstrap.js"></script>
    <script language="javascript" src="<?php echo JS; ?>index.js"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Registration</title>
  </head>

  <body class="homepage">


    <div class="container">
      <div class="reg-form-holder">
        <div class="row-fluid">
          <div class="span10 offset1">
            <br/>
            <br/>
            <?php if(isset($_SESSION['error'])) { ?>
              <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4><?=(isset($_SESSION['error']['title'])?$_SESSION['error']['title']:"Error!")?></h4>
                <?=$_SESSION['error']['text']?>
              </div>
            <?php unset($_SESSION['error']); } ?>
            <?php 
            if(isset($_SESSION['payment']['Payment_Through']) && $_SESSION['payment']['Payment_Through'] == 'A') {

              // $responce = explode($_SESSION['payment']['delimiter'], $_SESSION['payment']['responce']);
              if($_SESSION['payment']['status'] == '1' || $_SESSION['payment']['status'] == '2') { 
              ?>
              <h1>Success!</h1>
              <p><?=$_SESSION['payment']['message']?></p>
              <a href="login.php" class="btn">Go to Login</a>
              <?php } else { ?>
              <h1>Error!</h1>
              <p><?=$_SESSION['payment']['message']?></p>
              <a href="register-payment.php" class="btn">Re-fill Details</a>
              <?php }
              // unset($_SESSION['payment']);
            } else {
              if($_REQUEST['act'] == 'P') { ?>
                <h1>Pending!</h1>
                <p>It's still pending</p><?php
              } else if($_REQUEST['act'] == 'S') { ?>
                <h1>Success!</h1>
                <p>Payment done successfully.</p><?php
              } else if($_REQUEST['act'] == 'F') { ?>
                <h1><?=isset($_SESSION['payment']['title']) ? $_SESSION['payment']['title']: "Error"?></h1>
                <p><?=isset($_SESSION['payment']['message']) ? $_SESSION['payment']['message']: "Please check with site admin, payment not done due to some technical error"?></p><?php
              }
            } ?>
            <br/>
            <br/>
            <a href="<?=URL?>">click here</a> to go to home page.
          </div>
        </div>
      </div>
    </div>
  </body>
</html>